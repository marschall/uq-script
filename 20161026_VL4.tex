\subsection{Gaussian random variables (RVs)}
  Let $K$ a Hilbert space and $X$ a $K$-valued Gaussian RV in $(\Omega,\mathcal{F},\mathbb{P})$.
  For $p\geq 1$, $L^p(\Omega, \mathcal{F}, \mathbb{P}; K)$ denotes the space of $p$-integrable RVs $X\colon \Omega\to K$, i.e.  
  \begin{equation}
    \int_\Omega \vert X(\omega)\vert^p\mathbb{P}(d\omega) < \infty \text{ .}
  \end{equation}
  The space $L^p(H, \mathcal{B}(H), \mu; K)$ embedded with the norm 
  \begin{equation}
    \| X\|_{L^p(\Omega,\mathcal{F},\mathbb{P}; K)} = \left(\int_\Omega \vert X(\omega)\vert^p \mu(d\omega)\right)^{\frac{1}{p}}
  \end{equation}
  is a Banach space.
  Now, we want to show that, under certain assumptions, for every $K$-valued random variable there exists a Gaussian measure on $K$.
  \begin{theorem}
    \label{Thm: RV-Gaussian exists}
    Let $X\in L^2(\Omega,\mathcal{F},\mathbb{P}; K)$. 
    Then, there exists unique $m_X\in K$ and $Q\in L^+(K)$, s.t. for $h, k\in K$ we have
    \begin{equation}
      \langle m_x, h\rangle = \int_K\langle y, h\rangle \mathbb{P}_X(dy) = \int_\Omega\langle X(\omega), h\rangle \mathbb{P}(d \omega)\text{ ,}
    \end{equation}
    \begin{equation}
      \langle Qh,k\rangle = \dots = \int_\Omega\langle X(\omega) - m_X, h\rangle\langle X(\omega) - m_X,k\rangle\mathbb{P}(d\omega)
    \end{equation}
    for given Fourier transform
    \begin{equation}
      \hat{\mu_X}(k) = \int_K e^{i\langle y, k\rangle}\mathbb{P}_X(dy) = \int_\Omega e^{i\langle X(\omega), k\rangle}\mathbb{P}(d\omega)\text{.}
    \end{equation}
  \end{theorem}
  \begin{proof}
    Use \ref{2} and Theorem \ref{Thm:Exist-mu}.
  \end{proof}
  \begin{example}
    Assume $X\in L^2(\Omega, \mathcal{F}, \mathbb{P}; \mathbb{R})$ is a real valued Gaussian random variable with law $\mathcal{N}_\lambda$.
    Then, for $m\in\mathbb{N}$, 
    \begin{equation}
      \int_\Omega\vert X(\omega)\vert^{2m}\mathbb{P}(d\omega) = (2\pi\lambda)^{-\frac{1}{2}}\int_{-\infty}^\infty \xi^{2m} e^{-\frac{\xi^2}{2}} d\xi = \frac{(2m!)}{2^mm!}\lambda^m\text{ .}
     \end{equation}
      Hence , $X\in L^{2m}(\Omega,\mathcal{F},\mathbb{P}; \mathbb{R})$ for $m\in\mathbb{N}$.
  \end{example}
  \begin{definition}
    Let $X_1,\dots,X_n$ be real RVs in $H$ and consider $X$ as an $\mathbb{R}^n$-valued random variable
    \begin{equation}
      X(\omega) = (X_1(\omega), \dots, X_n(\omega))\text{ .}
    \end{equation}
    Then, $X_1, \dots, X_n$ are \emph{independent} if 
    \begin{equation}
      \mathbb{P}_X = \bigotimes_{i=1}^n \mathbb{P}_{X_i}\text{ .}
    \end{equation}
  \end{definition}
    \begin{proposition}
    The real valued random variables $X_1,\dots X_n$ are independent if and only if for any set of real positive Borel functions $\phi_1,\dots,\phi_n$ it holds
    \begin{equation}
      \label{3}
      \int_{\Omega} \prod_{k=1}^n \phi_k(X_k(\omega_k))\mathbb{P}(d(\omega_1,\dots,\omega_n)) = \prod_{k=1}^n\int_{\Omega_k}\phi_k(X_k(\omega))\mathbb{P}_k(d\omega_k)
    \end{equation}
   \end{proposition}
    \begin{example}
      Let $\Omega=H$, $\mu=\mathcal{N}_Q:=\mathcal{N}_{0, Q}$ and $(e_k)_{k=1}^\infty$ an orthonormal basis in $H$ and $(\lambda_k)_{k=1}^\infty$ positive numbers, s.t 
      \begin{equation}
        Qe_k = \lambda_ke_k \text{ ,} \quad k\in\mathbb{N} \text{ .}
      \end{equation}
      Moreover, define the RVs $X_i(x) = \langle x,e_i\rangle$, for $i=1,\dots,n$ and $x\in H$.
      Then, 
      \begin{equation}
        \mu_{X_i}= \mathcal{N}_{\langle Qe_i,e_i\rangle} = \mathcal{N}_{\lambda_i} := \mathcal{N}_{0,\lambda_i}
      \end{equation}
      and
      \begin{equation}
        \mu_X = \mathcal{N}_{Q_{i,j}} \text{ ,}
      \end{equation}
      where $Q_{i,j} := \langle Qe_i, e_j\rangle = \lambda_i\delta_{i,j}$, $i,j =1,\dots,n$.
      Hence, 
      \begin{equation}
        \mu_X = \bigotimes_{i=1}^n\mathcal{N}_{\lambda_i}=\bigotimes_{i=1}^n\mu_{X_i}
      \end{equation}
      and, the $X_1,\dots,X_n$ are independent.
    \end{example}
    \begin{example}
      Let $H=\mathbb{R}^n$, $Q=(Q_{i,j}) \in L^+(\mathbb{R}^n)$, s.t. det$(Q) > 0$ and $\mu = \mathcal{N}_Q$, s.t.
      \begin{equation}
        \mu(dx) = \frac{1}{\sqrt{(2\pi)^n\text{det}(Q)}}e^{-\frac{1}{2}\langle Q^{-1}x,x\rangle}dx , \quad x\in\mathbb{R}^n
      \end{equation}
      %Let RVs $X_1,\dots, X_n$ be given by $X_1(\omega) = x_1,\dots,X_n(\omega) = x_n$, $x=(x_1,\dots, x_n)\in\mathbb{R}^n$ and set $X= (X_1,\dots,X_n)$.
      Furthermore, let $\mu$ be the law of an $\mathbb{R}^n$ valued RV $X$. Set $X_i(\omega) = (X(\omega))_i$ for $i=1,\dots, n$.
      Then, it holds 
      \begin{equation}
        \mu_{X_j} = \mathcal{N}_{Q_{j,j}} , \quad j=1,\dots,n
      \end{equation}
      and
      \begin{equation}
        \mu_X = \mathcal{N}_{(Q_{i,j})}\text{ .}
      \end{equation}
      Hence,  the RVs $X_1,\dots, X_n$ are independent if and only if $Q$ is diagonal.
    \end{example}
    \begin{example}
      Let, for $Q\in L^+_1(H)$, $\mu=\mathcal{N}_Q$ be a Gaussian measure on $(H, \mathcal{B}(H))$ and $z_i \in H$ for $i=1, \dots, n$. 
      Set, for $x\in H$ and $i=1,\dots n$
      \begin{equation}
        X_{z_i}(x) = \langle x, z_i\rangle
      \end{equation}
      and $X=(X_{z_1},\dots X_{z_n})$. 
      Then, $X_{z_1}, \dots X_{z_n}$ are independent if and only if for $i, j=1,\dots, n$
      \begin{equation}
        \langle Qz_i,z_j \rangle = 0 \text{, } \quad \text{if } z_i\neq z_j\text{.}
      \end{equation}
    \end{example}
    
  \subsection{Cameron-Martin Space And White Noise Mapping}
  Assume a separable infinite dimensional Hilbert space $H$ and a nondegenerated Gaussian measure $\mu = \mathcal{N}_Q$. i.e. $Q\in L_1^+(H)$ and Ker $Q = \{0\}$. 
  Note that $Q^{-1} $ is not continuous, since for any CONS $(e_k)_{k=1}^\infty$ 
  \begin{equation}
    Q^{-1}e_k = \frac{1}{\lambda_k}e_k
  \end{equation}
  for $k\in\mathbb{N}$ and $\lambda_k\to\infty$ as $k\to\infty$.
  
  \begin{lemma}
    Assume $H$ is an infinite dimensional Hilbert space with CONS $(e_k)_{k=1}^\infty$. Then, $Q(H)$ is a dense subspace of $H$ and given by 
    \begin{equation}
      Q(H) = \left\{ x\in H \colon \sum_{k=1}^\infty \langle x, e_k\rangle^2 \lambda_k^{-2}  < \infty \right\}
    \end{equation}
  \end{lemma}
  \begin{proof}[Proof e.q. Da Prato: Functional Analytic Methods for Ev. Eq. Lemma 2.8]
	$ $ \newline
	The identity follows directly and $Q(H)$ beeing dense in $H$ inherits from the trivial kernel of $Q$.
  \end{proof}
  In the following, it will be usefull to define $Q^{\nicefrac{1}{2}}$.
  For any $x\in H$ we have
  \begin{equation}
    Q^{\nicefrac{1}{2}}x = \sum_{k=1}^\infty \sqrt{\lambda_k}\langle x, e_k\rangle e_k\text{ .}
  \end{equation}
  The space $Q^{\nicefrac{1}{2}}$ is called a \emph{Cameron-Martin} space.
  In the theory of stochastic partial differential equations, this space will also be called the \emph{reproducing kernel} of $\mu$.
  It is easy to see that, $Q^{\nicefrac{1}{2}}(H)$ is a proper subspace of $H$, since 
  \begin{equation}
    Q^{\nicefrac{1}{2}}(H) = \left\{ x\in H \colon \sum_{k=1}^\infty \langle x, e_k\rangle ^2 \lambda_k ^{-1} < \infty \right\} \text{ .}
  \end{equation}
  Given $z\in Q^{\nicefrac{1}{2}}(H)$ we consider the function $W_z\in L^2(H, \mu):=L^2(H, \mathcal{B}(H), \mu)$ defined for any $x\in H$ by 
  \begin{equation}
    W_z(x) := \langle Q^{-\nicefrac{1}{2}} z, x\rangle \text{ .}
  \end{equation}
  It will be important to define this for every $z\in H \supset Q^{\nicefrac{1}{2}}(H)$.
  Intuitively setting 
  \begin{equation}
    W_z(x) = \langle Q^{-\nicefrac{1}{2}}z, x\rangle 
  \end{equation}
  for $x \in H$ is missleading, since one can show that 
  \begin{equation}
    \mu(Q^{\nicefrac{1}{2}}(H)) = 0\text{ .}
  \end{equation}

  Alternatively, for $x \in H$, we consider the mapping 
  \begin{equation}
    W \colon Q^{\nicefrac{1}{2}}(H) \subset H \to L^2(H\mu)\text{, } z\mapsto W_z\text{, } W_z(x) = \langle x, Q^{-\nicefrac{1}{2}} z\rangle
  \end{equation}   
  $W$ is an isometry because, for all $z_1, z_2\in Q^{\nicefrac{1}{2}}(H)$, 
  \begin{equation}
    \int_H W_{z_1}(x)W_{z_2}(x)\mu(dx) = \langle QQ^{-\nicefrac{1}{2}}z_1, Q^{-\nicefrac{1}{2}}z_2\rangle = \langle z_1, z_2\rangle\text{ .}
  \end{equation}
  Since, $Q^{\frac{1}{2}}(H)$ is dense in $H$, the \emph{white noise mapping} $W$ can be uniquely extended to $H$.  
  
  \begin{proposition}  
    Let $z_1,\dots,z_n \in H$.
    Then, the law of the $\mathbb{R}^n$-valued RV $W = (W_{z_1}, \dots, W_{z_n})$ is given by
    \begin{equation}
      \label{4}
      \mu_W = \mathcal{N}_{(\langle z_i, z_j\rangle)_{i,j = 1,\dots, n}}
    \end{equation}
    Moreover, the RVs $W_{z_1},\dots, W_{z_n}$ are independent if and only if $(z_1, \dots z_n)$ are orthogonal, i.e.
    \begin{equation}
      \langle z_i, z_j\rangle = \delta_{i,j} \text{ ,} \quad i,j=1,\dots,n\text{ .}
    \end{equation}
  \end{proposition}
  \begin{exercise}
    For $z\in H$ we have 
    \begin{equation}
      \int_H e^{W_z}(x)\mu(dx) = e^{\frac{1}{2}\vert z\vert^2}
    \end{equation}
  \end{exercise}
  \begin{exercise}
    The function $H\to L^2(H,\mu) $, $f\mapsto e^{W_f}$ is continuous.
  \end{exercise}
  \begin{exercise}
    For $f, g \in H$ we have
	\begin{equation}
      \int_H W_f W_g d\mu = \langle f, g\rangle \text{ .}
	\end{equation}
  \end{exercise}
  
\section{$L^2$ Spaces With Respect To Gaussian Measures}
Denote by $L^2(H,\mathcal{B}(H))$ the Hilbert space of all equivalence classes of Borel square integrable real functions on $H$ embedded with the inner product
  \begin{equation}
    \langle \phi, \psi\rangle _{L^2(H, \mu)} = \int_H \phi\psi d\mu \text{ , }\quad \phi,\psi \in L^2(H, \mu)    
  \end{equation}
  and the norm
  \begin{equation}
    \|\phi\|_{L^2(H, \mu)} = \left( \int_H \vert \phi\vert ^2 d\mu\right)^\frac{1}{2}\text{ .}
  \end{equation}
  Denote by $L^2(H, \mu; H)$ the Hilbert space of all equivalence classes of Borel square integrable mappings from $H$ to $H$.
  This space will be called $L^2$ vector field.
  For $F\colon H\to H$ define 
  \begin{equation}
    \|F\|_{L^2(H,\mu; H)} := \left(\int_H\vert F(x)\vert ^2 \mu(dx)\right) ^\frac{1}{2} < \infty
  \end{equation}
  and for another $G\colon H\to H$
  \begin{equation}
    \langle F, G\rangle_{L^2(H, \mu; H)} := \int_H\langle F(x), G(x)\rangle \mu(dx)\text {.}
  \end{equation}
  \subsection{Orthonormal basis in $L^2(H,\mu)$}
  \subsubsection{One-dimensional case $H=\mathbb{R}, \mu=\mathcal{N}_1$}
  We define an orthonormal basis in terms of \emph{Hermite} polynomials.
  Therefore, consider for $t, \xi\in\mathbb{R}$ the analytic functions 
  \begin{equation}
    F(t, \xi) = e^{-\frac{t^2}{2} + t\xi}
  \end{equation}
  and the polynomials $(H_n)_{n\in\mathbb{N}_0}$,  given by
  \begin{equation}
    F(t, \xi) = \sum_{n=0}^\infty \frac{t^n}{\sqrt{n!}}H_n(\xi)
  \end{equation}
  where for $n\in\mathbb{N}_0$ and $\xi\in\mathbb{R}$ it holds
  \begin{equation}
    H_n(\xi) = \frac{(-1)^n}{\sqrt{n!}}e^{\frac{\xi^2}{2}}D^n_\xi(e^{-\frac{\xi^2}{2}})\text{ .}
  \end{equation}
  The Hermite polynomial $H_n$ has degree $n$ and a positive leading coefficient.
  \begin{example}
    The first Hermite polynomials are
    \begin{equation}
      H_0(\xi) = 1
    \end{equation}
    \begin{equation}
      H_1(\xi) = \xi
    \end{equation}
    \begin{equation}
      H_2(\xi) = \frac{1}{\sqrt{2}}(\xi^2 -1) \text{ .}
    \end{equation}
    Furthermore, it holds a recursive structure
    \begin{equation}
      \xi H_n(\xi) = \sqrt{n+1}H_{n+1}(\xi) + \sqrt{n}H_{n-1}(\xi)
    \end{equation}
    \begin{equation}
      D_\xi H_n(\xi) =  \sqrt{n} H_{n-1}(\xi)
    \end{equation}
    \begin{equation}
      D_\xi^2 H_n(\xi) = \xi D_\xi H_n(\xi) = -n H_n(\xi) \text{ .}
    \end{equation}
    The system $(H_n)_{n\in\mathbb	{N}_0}$ is orthonormal and complete in $L^2(\mathbb{R}, \mu)$.
  \end{example}