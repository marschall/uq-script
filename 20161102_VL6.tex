For all $m\in\mathbb{N}$ let $(\varphi_{m,i})_{i\in\mathbb{N}_0}$ be an orthonormal basis of the space $L^2(\Gamma_m, \Sigma_m, \mu_m)$, s.t. $\varphi_{m,0}\equiv 1$ (normalized, since $\mu_m$ is a probability measure).
For $\nu\in\mathcal{F}$ define the tensor product
\begin{equation}
  \varphi_\nu := \bigotimes_{m\in\mathbb{N}}\varphi_{m, \nu_m}
\end{equation}
and let $y=(y_m)_{m\in\mathbb{N}}\in\Gamma$.
Note that $y$ has to be interpreted as a realisation of the random variable $Y$ for some $\omega\in\Omega$.
With $F(\mathbb{N})$ define the set of all finite subsets of $\mathbb{N}$ and $I\in F(\mathbb{N})$.
Furthermore, define the finite product $\sigma$-algebra 
\begin{equation}
  \Sigma_I := \bigotimes_{m\in I} \Sigma_m \subset \Sigma \text{.}
\end{equation}
A function is $\Sigma_I$-measurable if it is $\Sigma$-measurable and only depends on $(y_m)_{m\in I}$.
Set 
\begin{equation}
  \mathcal{F}_I := \{\nu\in\mathcal{F} \colon \text{supp }\nu\subset I \}\text{.}
\end{equation}
\begin{lemma}
  For all $I\in F(\mathbb{N})$, the $(\varphi_\nu)_{\nu\in\mathcal{F}_I}$ form an orthonormal basis of $L^2(\Gamma, \Sigma_I,\mu)$.
\end{lemma}
\begin{proof}
  Use
  \begin{equation}
    L^2(\Gamma, \Sigma_I, \mu) \cong \bigotimes_{m\in I} L^2(\Gamma_m, \Sigma_m, \mu_m)
  \end{equation}
  and $I$ being finite.
\end{proof}
\begin{proposition}
  The set
  \begin{equation}
    \bigcup_{I\in F(\mathbb{N})} L^2(\Gamma, \Sigma_I, \mu)
  \end{equation}
  is dense in $L^2(\Gamma, \Sigma,\mu)$.
\end{proposition}
\begin{proof}
  Application of the monotone class theorem.
\end{proof}
Finally the main result.
\begin{theorem}
  $(\varphi_\nu)_{\in\mathcal{F}}$ is an orthonormal basis of $L^2(\Gamma, \Sigma, \mu)$.
\end{theorem}
\begin{proof}
  Exercise!
\end{proof}

\subsection{Orthogonal Polynomials}
Assume $\Gamma_m$ is a Borel subset of $\mathbb{R}$ and $\mu_m$ has finite moments 
\begin{equation}
  \label{7}
  M_n := \int_{\Gamma_m} \xi^n \mu_m(d\xi) \text{, } \quad n\in\mathbb{N}_0\text{.}
\end{equation}
Then, the orthogonal polynomials w.r.t $\mu_m$ can be constructed by a three-term recursion
\begin{equation}
  \label{8}
  \beta_{n+1} P_{n+1}(\xi) = (\xi-\alpha_n) P_n(\xi) - \beta_n P_{n-1}(\xi) \text{, }\quad n\in\mathbb{N}_0\text{,}
\end{equation}
with $P_{-1}(\xi) = 0$, $P_0(\xi) = 1$ and 
\begin{equation}
  \alpha_n := \int_{\Gamma_m} \xi P_n(\xi)\mu_m(d\xi)
\end{equation}
\begin{equation}
  \beta_n:= \frac{c_n-1}{c_n}
\end{equation}
and $\beta_0 = 1$, where $c_n$ is the leading coefficient of $P_n$.
\ref{8} can be divided by Gram-Schmidt orthogonalisation of monomials $(\xi^n)_{n\in\mathbb{N}_0}$.
For all $n\in\mathbb{N}_0$, $P_n$ is a polynomial of degree $n$ if $n<N:=\dim(L^2(\Gamma_m, \Sigma_m,\mu_m))$ and zero otherwise.
The sequence of $(P_n)_{n\in\mathbb{N}_0}$ (resp. $(P_n)_{n=0}^{N-1}$, if $N$ is finite) is orthonormal in $L^2(\Gamma_m, \Sigma_m, \mu_m)$.
For $N$ finite, it follows immediately that $(P_n)_{n=0}^N$ is an orthonormal basis of $L^2(\Gamma_m,\Sigma_m,\mu_m)$. 
With $N=\infty$, for this to hold, $\mu_m$ has to be \emph{determinate}, i.e. the measure $\mu_m$ is uniquely characterized by its moments $(M_n)_{n\in\mathbb{N}_0}\in\mathbb{R}$.
Note that for bounded $\Gamma_m\subset \mathbb{R}$, $\mu_m$ is determinate.
\begin{example}
  For $m\in\mathbb{N}_0$ let $(P_n^m)_{n\in\mathbb{N}_0}$ be orthonormal polynomials forming a basis of $L^2(\Gamma_m, \Sigma_m, \mu_m)$.
  Then, the tensor product polynomials 
  \begin{equation}
    P_\nu := \bigotimes_{m\in\mathbb{N}}P_{\nu_m}^m\text{, }\quad \nu\in\mathcal{F}
  \end{equation}  
  form an orthonormal basis of $L^2(\Gamma, \Sigma, \mu)$, which is called \emph{generalized polynomial chaos basis}.
  If $\mu_m=\mathcal N_1$ for all $m\in\mathbb{N}$, then $(P_n^m)_{n\in\mathbb{N}_0}$ are hermite polynomials, interpreted as a basis of $L^2(\mathbb{R}^\infty, \mathcal{B}(\mathbb{R}^\infty), \gamma)$.
\end{example}
\begin{example}
  Consider $\mu_m$ as uniform distribution on $\Gamma_m := [-1, 1]$ for $m\in\mathbb{N}$, $\mu(d\xi) = \frac{1}{2}d\xi$.
  The corresponding orthonormal polynomials defined by three-term recursion
  \begin{equation}
    \frac{n+1}{\sqrt{2n+3}\sqrt{2n+1}}L_{n+1}(\xi) = \xi L_n(\xi) - \frac{n}{\sqrt{2n+1}\sqrt{2n-1}}L_{n-1}(\xi) \text{,}
  \end{equation}
  where $L_{-1}(\xi) = 0$, $L_0(\xi) = 1$.
  These polynomials satisfy the \emph{Rodriguez formula} 
  \begin{equation}
    L_n(\xi) = \frac{\sqrt{2n+1}}{2^n n!}\frac{d^n}{d\xi}(\xi^2 -1) \text{, }\quad n\in\mathbb{N}_0\text{.}
  \end{equation}
  The first few are $L_0(\xi) = 1$, $L_1(\xi) = \sqrt{3}\xi$, $L_2(\xi) = \frac{\sqrt{5}}{2}(3\xi^2-1)$.
  The measure space $(\Gamma, \Sigma, \mu)$ is a countable product of identical factors
  \begin{equation}
    ([-1, 1], \mathcal{B}([-1, 1]), \frac{1}{2}d\xi) \text{,}
  \end{equation}
  $\Gamma=[-1, 1]^\infty$, $\Sigma=\mathcal{B}([-1,1])^\infty = \mathcal{B}([-1,1]^\infty)$, $\mu = \bigotimes_{m\in\mathbb{N}}\mu_m$ with $\mu_m(d\xi) = \frac{1}{2}d\xi$, for $m\in\mathbb{N}_0$.
  The Legendre chaos basis $(L_\nu)_{\nu\in\mathcal{F}}$ is orthonormal basis of $L^2([-1,1]^\infty , \mathcal{B}([-1,1]^\infty) ,\mu)$.
  
  Note that, even thought each $\mu_m$ is absolutely continuous w.r.t to the Lebesgue measure, the product density is zero. Moreover, there is no Lebesgue measure in infinite dimensions. Nevertheless, by Kolmogorovs extension theorem, we obtain well-posedness of the formulation above. 
\end{example}