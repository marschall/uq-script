\section{PDEs with uniform stochastic parameters}
\subsection{Parametric stochastic operators}
  Let $V$ be a separable Hilbert space with dual space $V^*$ and the $(V^*, V)$ duality paring $\langle \cdot , \cdot \rangle$.
  We consider a parametric operator equation of the form
  \begin{equation}
    \label{9}
    Au=f
  \end{equation}
  with $f\in V^*$ and $A\in\mathcal{L}(V,V^*)$ a linear, bounded operator from $V$ to $V^*$.
  \begin{theorem}
    If $A$ is boundedly invertible, then \eqref{9} has a unique solution $u=A^{-1}f$.
  \end{theorem}
  Let $\Gamma$ be a topological space, $A$ a parametric operator from $V$ to $V^*$ defined by the continuous map 
  \begin{equation}
    A\colon \Gamma\to \mathcal{L}(V, V^*)\text{.}
  \end{equation}
  We assume that $A(y)$ is boundedly invertible, for all $y\in\Gamma$ and consider the parametric operator equation
  \begin{equation}
    \label{10}
    A(y)u(y)=f(y) \text{,}\qquad \forall y\in\Gamma
  \end{equation}
  for a map $f\colon\Gamma\to V^*$.
  \begin{proposition}
    Eq \eqref{10} has a unique solution $u\colon\Gamma\to V$. It is continuous if and only if $f\colon\Gamma\to V^*$ is continuous.
  \end{proposition}
  \begin{proof}
    Existence follows directly and continuity of the map $y\mapsto A^{-1}(y)$ yields the claim.
  \end{proof}
  To derive the weak formulation of \eqref{10} in the parameter $y$, we additionally assume $A(y)$ is symmetric, positive definite for all $y\in\Gamma$ and there exists constants $\check{c}$ and $\hat{c}$, s.t. 
  \begin{equation}
    \label{11}
    \|A(y)\|_{V\rightarrow V^*} \leq \check{c}\text{,}\quad \|A^{-1}(y)\|_{V^*\rightarrow V} \leq \hat{c}\text{,}\quad \forall y\in\Gamma\text{,}
  \end{equation}
  i.e. the bilinear form $\langle A(y)\cdot, \cdot \rangle$ is a scalar product on $V$ that induces a norm equivalent to $\|\cdot\|_V$.
  In the future we will omit the operator norm index.
  The estimates \eqref{11} always hold for $\Gamma$ compact.
  Let $\mu$ be a probability measure on Borel-measurable set $(\Gamma, \mathcal{B}(\Gamma))$. Then, the operator $A(y)$ becomes stochastic since it depends on the parameter $y\in\Gamma$ in the probability space $(\Gamma,\mathcal{B}(\Gamma),\mu)$.
  Similarly, if $f$ is a random variable random on $(\Gamma, \mathcal{B}(\Gamma), \mu)$, with values in $V^*$. We assume in the following 
  \begin{equation}
    \label{12}
    f\in L^2(\Gamma,\mathcal{B}(\Gamma); V^*)\text{.}
  \end{equation}
  The linear variational problem will be obtained by multiplication of \eqref{10} by a test function $v\colon\Gamma\to V$ and integration over $\Gamma$, 
  \begin{equation}
    \label{13}
    \int_\Gamma \langle A(y)u(y), v(y)\rangle \mu(dy) = \int_{\Gamma}\langle f(y),v(y)\rangle\mu(dy)\text{.}
  \end{equation}
  \begin{theorem}
    With \eqref{11} and \eqref{12}, the solution $u$ of \eqref{10} is the unique element of $L^2(\Gamma,\mathcal{B}(\Gamma),\mu; V)$ satisfying \eqref{13} for all $v\in L^2(\Gamma,\mathcal{B}(\Gamma),\mu; V)$.
    Moreover,
    \begin{equation}
      \|u\|_{L^2(\Gamma,\mathcal{B}(\Gamma), \mu; V)} \leq \check{c}\| f\|_{L^2(\Gamma,\mathcal{B}(\Gamma), \mu; V^*)} \text{.}
    \end{equation}
  \end{theorem}
  \begin{remark}
    For separable Hilbert space $X$, the Lebesgue-Bochner space \newline $L^2(\Gamma, \mathcal{B}(\Gamma), \mu; X)$ is isometrically isomorphic to the Hilbert tensor product space $X\otimes L^2(\Gamma,\mathcal{B}(\Gamma),\mu)$. 
    In particular, the solution $u$ can be seen as an element of $V\otimes L^2(\Gamma,\mathcal{B}(\Gamma), \mu)$ and $f$ is an element of $V^*\otimes L^2(\Gamma,\mathcal{B}(\Gamma), \mu)$.
  \end{remark}
\subsection{Stationary diffusion with stochastic coefficient}
  Let $D$ be a bounded Lipschitz domain in $\mathbb{R}^d$ and a probability space $(\Omega, \Sigma, \mathbb{P})$.
  The model problem reads as follows:
  \begin{align}
    \label{MP}
    -\nabla\cdot (a(x,\omega)\nabla U(x,\omega)) &= f(x)\text{, } \quad &x\in D\text{, } \omega\in\Omega\text{, } \\
    U(x,\omega) &= 0\text{, } &x\in\partial D\text{, }\omega\in\Omega\text{.}
  \end{align}
  We assume uniform boundedness of the coefficient, i.e. for all $x\in D$ and $\omega \in \Omega$ there exists a $\check{a},\hat{a}\in\mathbb{R}_{>0}$, s.t.
  \begin{equation}
    \label{15}
    0 < \check{a} \leq a(x, \omega) \leq \hat{a} < \infty \text{, }
  \end{equation}
  and select some deterministic approximation $a_0\in L^\infty (D)$ to the stochastic coefficient $a(x, \omega)$, e.q. 
  \begin{equation}
    a_0(x) = \bar{a}(x) := \int_\Omega a(x, \omega) d\mathbb{P}(\omega)
  \end{equation}
  or
  \begin{equation}
    a_0 = \frac{1}{2}(\check{a} + \hat{a})\text{, or } a_0 = \sqrt{\check{a}\hat{a}}\text{.}
  \end{equation}
  We consider a series expansion of $a(x, \omega) - a_0(x)$ which in case of $a_0 = \bar{a}$ is the fluctuation around the mean value. 
  Let $(\varphi_m)_{m\in\mathbb{N}}$ be a biorthogonal basis of $L^2(D)$ with associated dual basis $(\tilde{\varphi}_m)_{m\in\mathbb{N}}\subset L^2(D)$, i.e. 
  \begin{equation}
    \langle \varphi_m,\tilde{\varphi}_n\rangle = \delta_{m,n}\text{, }
  \end{equation}
  and for $v\in L^2(D)$,  
  \begin{equation}
    \label{16}
    v=\sum_{m\geq 1}\langle v, \tilde{\varphi}_m\rangle \varphi_m
  \end{equation}
  with unconditional convergence.
  For a positive sequence $(\alpha_m)_{m\in\mathbb{N}}$, define RVs 
  \begin{equation}
    Y_m(\omega) := \frac{1}{\alpha_m}\int_D (a(x, \omega) - a_0(x)) \tilde{\varphi}_m(x)dx\text{, }\quad m\in\mathbb{N}\text{.}
  \end{equation}
  By \eqref{16}, for all $\omega\in\Omega$, 
  \begin{equation}
    a(x, \omega) = a_0(x) + \sum_{m=1}^\infty \alpha_m \phi_m(x) Y_m(\omega)
  \end{equation}
  with unconditional convergence in $L^2(D)$.
  \begin{lemma}
    \label{lemma 1}
    There is a positive sequence $(\alpha_m)_{m\in\mathbb{N}}$, s.t. $Y_m(\omega)\in[-1, 1]$, for all $\omega\in \Omega$.
  \end{lemma}
  \begin{proof}
    By H\"olders inequality, 
    \begin{equation}
      \vert \int_D (a(x, \omega) - a_0(x))\tilde{\varphi}_m dx \vert \leq \| a(\cdot, \omega) - a_0(\cdot) \|_{L^\infty(D)}\|\tilde{\varphi}_m\|_{L^1(D)}\text{.}
    \end{equation}
    Hence, 
    \begin{equation}
      \alpha_m := \sup_{\omega\in\Omega}\|a(\cdot, \omega) - a_0(\cdot)\|_{L^\infty(D)}\|\tilde{\varphi}_m\|_{L^1(D)}\text{.}
    \end{equation}
  \end{proof}
  Define as a parametric domain the compact and topological space 
  \begin{equation}
    \Gamma = [-1, 1]^\infty = \bigtimes_{m=1}^\infty [-1, 1]
  \end{equation}
  and let $(\alpha_m)_{m\in\mathbb{N}}$ be a sequence as in lemma \ref{lemma 1}.
  Assume that $\sum_{m=1}^\infty \alpha_m\vert \varphi_m(X)\vert $
  converges in $L^\infty(D)$, i.e.
  \begin{equation}
    \lim_{M\rightarrow\infty}\esssup_{x\in D}\sum_{m=M}^\infty \alpha_m\vert \varphi_m(x)\vert = 0
  \end{equation}