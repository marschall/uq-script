Then, 
\begin{equation}
  a_\varphi(x, y) := a_0(x) + \sum_{m=1}^\infty \alpha_m\varphi_m(x)y_m\text{,}\quad (y_m)_{m\in\mathbb{N}}\in \Gamma
\end{equation}
 converges unconditionally in $L^\infty(D)$ and the stochastic diffusion coefficient satfisfies
\begin{equation}
  \label{17}
  a(x, \omega) = a_\varphi(x, Y(\omega))\text{,}\quad x\in D\text{, } \omega\in\Omega\text{,}
\end{equation}
where $Y(\omega):=(Y_m(\omega))_{m\in\mathbb{N}}\in\Gamma$.
Define the operators (with dual paring index $_{H^{-1}}\langle \cdot,\cdot\rangle_{H_0^1}$ omitted)
\begin{equation}
  \langle A(y)v, w\rangle := \int_D a_\varphi(x, y)\nabla v(x)\cdot\nabla w(x) dx\text{, }\quad y\in\Gamma
\end{equation}
\begin{equation}
  \langle A_0v, w\rangle := \int_D a_0(x)\nabla v(x)\cdot\nabla w(x) dx\text{, }
\end{equation}
\begin{equation}
  \langle A_mv, w\rangle := \int_D \alpha_m\varphi_m(x)\nabla v(x)\cdot\nabla w(x) dx\text{, }\quad m\in\mathbb{N}  
\end{equation}
for $v,w\in H_0^1(D)$.
$A(y)$ is the operator associated to \eqref{MP} for all $\omega\in\Omega$, namely 
\begin{equation}
  \label{18}
  A(y) = A_0 + \sum_{m=1}^\infty A_my_m\text{, }\quad y\in\Gamma\text{.}
\end{equation}
Hence, $U(x,\omega) = u(x,Y(\omega))$, for $\omega\in\Omega$ and $x\in D$.
Note that \eqref{18} converges in $\mathcal{L}(H_0^1(D), H^{-1}(D))$ uniformly in $y\in\Gamma$ and $A(y)$ depends continuously on $y\in\Gamma$.
We assume that the bilinear form associated to the operator $A_0$ is \emph{coercive} on $H_0^1(D)$, i.e. there exists an $\check{a}_0$ ,s.t.
\begin{equation}
  \label{19}
  \essinf_{x\in D} a_0(x) \geq \check{a}_0 > 0\text{.}
\end{equation}
\begin{proposition}
  \label{prop2}
  If 
  \begin{equation}
    \label{20}
    \gamma := \frac{1}{\check{a}_0}\esssup_{x\in D} \sum_{m\geq 1}\vert \varphi_m(x)\vert < 1\text{, }
  \end{equation}
  then, 
  \begin{eqnarray}
    A(y)\colon H_0^1(D) \to H^{-1}(D)
  \end{eqnarray}
  is boundedly invertible for all $y\in\Gamma$ with 
  \begin{equation}
    \sup_{y\in\Gamma}\|A^{-1}(y)\| \leq \frac{1}{\check{a}_0(1-\gamma)}
  \end{equation}
 and $A(y)$ is bounded with 
 \begin{equation}
   \sup_{y\in\Gamma}\|A(y)\| \leq \|a_0\|_{L^\infty(D)}(1+\gamma)\text{.}
 \end{equation}
\end{proposition}
\begin{proof}
  $A_0\colon H_0^1(D)\to H^{-1}(D)$ is invertible due to \eqref{19} and the Lax-Milgram lamma. 
  The norm if $A_0^{-1}$ is denoted by $\check{a}_0^{-1}$.
  By \eqref{20} we have
  \begin{equation}
    \|A_0^{-1}(A_0 - A(y))\| \leq \check{a}_0^{-1}\esssup_{x\in D}\sum_{m=1}^\infty\alpha_m\vert \varphi_m(x)\vert = \gamma < 1\text{.}
  \end{equation}
  Therefore, 
  \begin{equation}
    I-A_0^{-1}(A_0-A(y)) = A_0^{-1}A(y)
  \end{equation}
  is invertible by a Neumann series and has norm less than $(1-\gamma)^{-1}$.
  Multiplication by $A_0$ yields the statement.
\end{proof}


\subsection{Discretization in Legendre polynomials}
For a seperable Hilbert space $V$, consider the parametric operator in $\mathcal{L}(V, V^*)$ of the form
\begin{equation}
  \label{21}
  A(y) = A_0 + \sum_{m\geq 1}A_my_m\text{,}\quad y\in [-1, 1]^\infty
\end{equation}
with $A_0, A_m\in\mathcal{L}(V, V^*)$ and uniformly convergent in $\mathcal{L}(V, V^*)$ with $A(y)$ positive boundedly invertible for all $y$ and continuous dependence on $y\in\Gamma=[-1,1]^\infty$, which holds by proposition \ref{prop2}.
Let $\mu$ on $(\Gamma, \mathcal{B}(\Gamma))$ be a countable product of uniform measures on $[-1, 1]$.
The tensor product Legendre polynomials $(L_\nu)_{\nu\in\mathcal{F}}$ form an orthonormal basis of $L^2(\Gamma, \mathcal{B}(\Gamma), \mu)$.
The operator $A(y)$ induces a boundedly invertible operator between the Hilbert tensor product spaces $V\otimes L^2(\Gamma,\mathcal{B}(\Gamma), \mu))$ and $V^*\otimes L^2(\Gamma, \mathcal{B}(\Gamma), \mu)$.
\begin{definition}
  We introduce the \emph{multiplication operator}, for $m\in\mathbb{N}$, 
  \begin{equation}
    \label{22}
    M_{y_m} \colon L^2(\Gamma, \mathcal{B}(\Gamma), \mu) \to L^2(\Gamma, \mathcal{B}(\Gamma), \mu) \text{, } g(y)\mapsto y_mg(y)\text{.}
  \end{equation}
\end{definition}
\begin{remark}
  From $y_m\in [-1,1]$ it follows that $M_{y_m}$ is self-adjoint and 
  \begin{equation}
    \label{23}
    \|M_{y_m}\| = 1\text{,}\quad \text{for all } m\in\mathbb{N}\text{.}
  \end{equation}
\end{remark}
\begin{proposition}
  \label{prop3}
  We assume that 
  \begin{equation}
    \sum_{m\geq 1}\| A_m\| < \infty\text{.}
  \end{equation}
  Then, the operator induced by $A(y)$ in \eqref{13} can be represented by
  \begin{equation}
    \label{24}
    \mathcal{A}=A_0\otimes I + \sum_{m=1}^\infty A_m\otimes M_{y_m}
  \end{equation}
  and $\mathcal{A}\in \mathcal{L}(V\otimes L^2(\Gamma,\mathcal{B}(\Gamma), \mu; V^*\otimes L^2(\Gamma,\mathcal{B}(\Gamma), \mu))$.
  \eqref{24} converges unconditionally.
\end{proposition}
\begin{proof}
  The operator $\mathcal{A}$ is well-defined by \eqref{24}, since by \eqref{23} 
  \begin{equation}
    \|\sum_{m=M}^\infty A_m\otimes M_{y_m}\| \leq \sum_{m=M}^\infty \|A_m\| \|M_{y_m}\| = \sum_{m=M}^\infty \|A_m\|\text{, }
  \end{equation}
  which can be made arbitrarily small for $M$ large.
  Let $g\in L^2(\Gamma, \mathcal{B}(\Gamma),\mu)$ and $v\in V$. 
  Then, with \eqref{24},
  \begin{equation}
    \mathcal{A}(v\otimes g)(y) = A_0 vg(y) + \sum_{m=1}^\infty A_m v y_mg(y) = A(y)(v g(y))\text{,}\quad y\in\Gamma\text{.}
  \end{equation}
  Hence, $\mathcal{A}$ is the operator induced by $A$.
\end{proof}
Since, $(L_\nu)_{\nu\in\mathcal{F}}$, are a CONS of $L^2(\Gamma,\mathcal{B}(\Gamma),\mu)$, 
\begin{equation}
  \label{25}
  T_L\colon l^2(\mathcal{F})\to L^2(\Gamma, \mathcal{B}(\Gamma),\mu) \text{, } (c_\nu)_{\nu\in\mathcal{F}}\mapsto\sum_{\nu\in\mathcal{F}}c_\nu L_\nu
\end{equation}
is an unitary isomorphism by Parseval's identity.
The tensorization with the dual identity $I_V\colon V\to V^*$ on $V$ yields the isometric isomorphism 
\begin{equation}
  \label{26}
  I_V\otimes T_L\colon V\otimes l^2(\mathcal{F})\to V^*\otimes L^2(\Gamma, \mathcal{B}(\Gamma), \mu)\text{,}
\end{equation}
with adjoint
\begin{equation}
  (I_V\otimes T_L)^* = I_{V^*}\otimes T_L^* \colon V^*\otimes L^2(\Gamma, \mathcal{B}(\Gamma),\mu) \to V\otimes l^2(\Gamma, \mathcal{B}(\Gamma),\mu)
\end{equation}
\begin{definition}
  The \emph{semidiscrete operator} is given by 
  \begin{equation}
    \label{27}
    \mathcal{A}_L := (I_V\otimes T_L)^*\mathcal{A}(I_V\otimes T_L)
  \end{equation}
  and interpreting $f\in L^2(\Gamma, \mathcal{B}(\Gamma, \mu; V^*)$ as an element of $V^*\otimes L^2(\Gamma, \mathcal{B}(\Gamma), \mu)$, 
  \begin{equation}
    F_L:= (I_V\otimes T_L)^* f= \int_\Gamma f(y)L_\nu(y)\mu(dy)\text{, }\quad \nu\in\mathcal{F}\text{.}
  \end{equation}
\end{definition}
With the semidiscrete operator we obtain the \emph{semidiscrete operator equation}
\begin{equation}
  \label{28}
  \mathcal{A}_Lu_L = f_L
\end{equation}
with \begin{equation}
 \mathcal{A}_L = A\otimes I + \sum_{m\geq 1} A_m\otimes K_m\text{, }
\end{equation}
\begin{equation}
  \label{29}
  K_m:= T_L^*M_{y_m} T_L 
\end{equation}
and 
\begin{equation}
  u = (I_V\otimes T_L) u_L\text{.}
\end{equation}
\begin{lemma}
  \label{lemma2}
  $K_m \colon l^2(\mathcal{F})\to l^2(\mathcal{F})$, $m\in\mathbb{N}$ has the form
  \begin{equation}
    \label{30}
    K_m(c_\nu)_{\nu\in\mathcal{F}} = (\beta_{\nu_m+1} c_{\nu + \epsilon_m} + \beta_{\nu_m}c_{\nu-\epsilon_m})_{\nu\in\mathcal{F}}
  \end{equation}
  with $(\epsilon_m)_{n} =\delta_{m,n} $ and $\beta_n = \frac{1}{\sqrt{4-n^{-2}}}\in \left( \frac{1}{2}, \frac{1}{\sqrt{3}}\right]$, $n\in\mathbb{N}$.
  $K_m$ is self-adjoint and $\|K_m\| = 1$, for every $m\in\mathbb{N}$.
\end{lemma}
\begin{proof}
  Since, $T_L^{-1} = T_L^*$, 
  \begin{equation}
   T_LK_m(c_\nu)_{\nu\in\mathcal{F}} = M_{y_m} T_L(L_\nu)_{\nu\in\mathcal{F}} = \sum_{\nu\in\mathcal{F}}c_\nu y_mL_\nu(y)\text{.}
  \end{equation}
  Therefore, \eqref{30} is equivalent to 
  \begin{equation}
    y_mL_\nu(y) = \beta_{\nu_m+1}L_{\nu+\epsilon_m}(y) + \beta_{\nu_m}L_{\nu - \epsilon_m}(y)\text{.}
  \end{equation}
  The claim follows from the three term recursion 
  \begin{equation}
    \xi L_n(\xi) = \beta_{n+1}L_{n+1}(\xi) + \beta_n L_{n-1}(\xi) \text{, }\quad \xi \in [-1, 1]\text{, } n\in\mathbb{N}\text{.}
  \end{equation}
\end{proof}
The solution $u$ of \eqref{10} is 
\begin{equation}
  u(y) = \sum_{\nu\in\mathcal{F}}u_\nu L_\nu(y)\in V\text{, }\quad y\in\Gamma\text{, }
\end{equation}
with convergence in $L^2(\Gamma, \mathcal{B}(\Gamma), \nu; V)$, where the coefficients $(u_\nu)_{\nu\in\mathcal{V}}\in V$ are determined by the equation
\begin{equation}
  %\label{31}
  A_0u_\nu + \sum_{m\geq 1}A_m(\beta_{\nu_m+1}u_{\nu_m+\epsilon_m}+ \beta_{\nu_m}u_{\nu_m - \epsilon_m}) = f_\nu \text{,} \quad \nu\in\mathcal{F}
\end{equation}
with $f_\nu := \int_\Gamma f(y) L_\nu(y)\mu(dy)\in V^*$.


\subsection{full discretization with finite elements}
Assume $\Lambda\subset\mathcal{F}$ with $\vert \Lambda\vert < \infty$ and let $V_{N,\nu}\subset V$ be finite dimensional spaces for $\nu\in\mathcal{F}$, with $V_{N, \nu}=\{0\}$ for $\nu\in\mathcal{F}\\ \Lambda$.
Define 
\begin{equation}
  %\label{32}
  V_N := \{v\in L^2(\Gamma, \mathcal{B}(\Gamma), \mu; V)\colon V_\nu\in V_{N, \nu}\text{, } \forall \nu\in\mathcal{F} \}\text{.}
\end{equation}
where $v_\nu\in V$ is the $\nu^{\text{th}}$ coefficient of the expansion of $v\in L^2(\Gamma, \mathcal{B}(\Gamma), \mu; V)$ w.r.t $(L_\nu)_{\nu\in\mathcal{F}})$.
This span can be interpreted as a subspace of $L^2(\Gamma; V)$ or as the span of sequences $(v_\nu)_{\nu\in\mathcal{F}}$ in V with $v_\nu\in V_{N, \mu}$ for $\nu\in\mathcal{F}$, which is a subspace of $l^2(\mathcal{F}; V)$.
By Parseval's identity, the norms induced by these two spaces coincide.