\subsection{Optimal convergence rates of stochastic Galerkin approximation}
The considered Galerkin approximation of the solution of the parametric deterministic formulation of a stochastic problem is well-defined and quasi-optimal in mean-square sense.
Our goals are  
\begin{enumerate}[(i)]
\item Identification of the best possible convergence rate for an $N$-term polynomial chaos approximation (based on regularity of the solution in terms of summability of the chaos coefficients.)
\item Construction of an optimal index sets $\Lambda\subset\mathcal{F}$ of active polynomials, such that $\vert \lambda\vert \leq N$.
\end{enumerate}

\begin{remark}
  $(i)$ can be approached similar to standard regularity theory of PDEs (bootstrapping arguments) or by (more modern) analytic continuation, which often yields sharper bounds.
\end{remark}

\subsubsection{Affine Case with Legendre Chaos}
We assume that $Y_m(\omega)\in [-1,1]$ for $\omega\in\Omega$, $m\in\mathbb{N}$, and denote the scaled coefficient functions of $a(x,\omega)$ by $\psi_j(x)=\alpha_j\varphi_j(x)$, for $j=1,2,\dots$.
Furthermore, assume the following additional conditions
\begin{assumption}
  \label{49}
  \begin{equation}
    \label{C1}  
    \psi_j\in L^\infty(D)\text{,}\quad j\in\mathbb{N}
  \end{equation}
  \begin{equation}
    \label{C2}
    y=(y_1,\dots) \in [-1,1]^\mathbb{N}
  \end{equation}
  \begin{equation}
    \label{C3}
    a(x,y) = a_0(x) + \sum_{j\geq 1}\psi_j(x)y_j \quad \text{ for all }x\in D\text{ and } y\in\Gamma
  \end{equation}
\end{assumption}
\begin{assumption}[UEA - uniform ellipticity assumption in $\mathbb{R}$]
  \label{assumption 1}
$ $ \\
  Assume there exists $ 0<r\leq R<\infty$ s.t. for all $x\in D$
  \begin{equation}
    0<r\leq a(x,y) \leq R < \infty\text{,} \quad \text{UEA(r,R)}\text{.}
  \end{equation}
\end{assumption}

\begin{remark}
  The uniform boundedness in assumption \ref{assumption 1} yields directly boundedness for the coefficient field mean by taking $y_j = 0$ for $j\in\mathbb{N}$
  \begin{equation}
    r\leq a_0(x) \leq R \quad \text{ for }x\in D\text{.}
  \end{equation}
  Moreover the equivalent formulations 
  \begin{equation}
    \label{50}
    \sum_{j\geq 1}\vert\psi_j(x)\vert \leq a_0(x) -r 
  \end{equation}
  and
  \begin{equation}
    \label{51}
    \sum_{j\geq 1} \vert \psi_j(x)\vert \leq R-a_0(x)\text{.}
  \end{equation}
\end{remark}

\begin{assumption}[UEA in $\mathbb{C}$ with complex parameters $y_j$]
$ $ \\
  Assume there exists $0< r\leq R< \infty$ s.t. for $x\in D$, $z\in U\subset \mathbb{C}^{\mathbb{N}}$,
  \begin{equation}
    0<r<\text{Re}  (a(x,z)) \leq \vert a(x,z)\vert \leq R < \infty\text{,}\quad\text{UEAC(r,R)}
  \end{equation}
   with complex variables $z=(z_j)_{j=1,\dots}$ and $\vert z_j\vert \leq 1$ for $j\in\mathbb{N}$. 
  Hence, the parameter vector $z$ belongs to the polydisc 
  \begin{equation}
    \label{52}
    U:= \prod_{j\geq 1}\{z_j\in\mathbb{C}\colon \vert z_j\vert \leq 1\}\supset \Gamma
  \end{equation}
\end{assumption}
\begin{remark}
  UEA($r$,$R$) implies UEAC($r$,$2R$) for real $a_0$ and $y_j$.
  Hence, solution $u(z)$ is well-definied in $V$ for $z\in U$ y complex Lax-Milgram.
\end{remark}
\begin{lemma}[Stechkin Lemma]
  \label{lemma 9}
  Let $(\gamma_n)_{n\geq 1}$ be a decreasing sequence of non-decreasing integers.
  Then, for any $0<p\leq q<\infty$ and $N\in\mathbb{N}$,
  \begin{equation}
    \label{53}
    \left( \sum_{n\geq N}\gamma_n^q\right)^\frac{1}{q}\leq N^{\frac{1}{q}-\frac{1}{p}}\left(\sum_{n\geq 1}\gamma_n^p\right)^{\frac{1}{p}}
  \end{equation}
\end{lemma}
\begin{proof}
  For $q<\infty$, combine the estimations 
  \begin{equation}
    \sum_{n\geq N} \gamma_n^q \leq \gamma_N^{q-p}\sum_{n\geq N}\gamma_n^p\leq \gamma_N^{q-p}\sum_{n\geq 1}\gamma_n^p 
  \end{equation}
  and
  \begin{equation}
    N\gamma_N^p \leq \sum_{n\leq N}\gamma_n^P\leq \sum_{n\geq 1}\gamma_n^p
  \end{equation}
\end{proof}
\ref{lemma 9} will be used repeatedly although there are recent (sometimes) sharper techniques.

\begin{definition}
  Let $\nu\in\mathcal{F}$ with support in $\{1,\dots, J\}$ and a complex sequence $\alpha=(\alpha_j)_{j\geq 1}$.
  Then, we define the partial derivative with respect to a multi-index 
  \begin{equation}
    \partial^\nu u = \frac{\partial^{\vert\nu\vert}u}{\partial^{\nu_1}y_1\cdots \partial^{\nu_J}y_J}
  \end{equation}
  and by convenience we set 
  \begin{equation}
    \nu ! := \prod_{j\geq 1} \nu_j!
  \end{equation}
  \begin{equation}
   0! = 1
  \end{equation}
  \begin{equation}
    \alpha^{\nu} := \prod_{j\geq 1} \alpha_j^{\nu_j}
  \end{equation}
\end{definition}
\begin{theorem}
  \label{theorem 5}
  If $a(x,y)$ fulfills UEAC($r$,$R$) for some $0 < r\leq R<\infty$ and if $\left(\|\psi_j\|_{L^\infty(D)}\right)_{j\geq 1}\in\ell^{p}(\mathbb{N})$ for some $0<p<1$, then $u(z)$ is analytic as a mapping from $U$ into $V$ and $u(z)$ can be expanded by a Taylor series 
  \begin{equation}
    \label{54}
    u(z) = \sum_{\nu\in\mathcal{F}}t_\nu z^\nu \quad\text{ in } V \text{,}
  \end{equation}
  where $t_\nu := \frac{1}{\nu!}\partial^\nu u(0)$ for $\nu\in\mathcal{F}$, and $t_\mu\in V$ and $\left(\|t_\nu\|_V\right)_{\nu\in\mathcal{F}}\in \ell^p(\mathcal{F})$. 
  The convergence of \ref{54} is unconditional: if $(\Lambda_N)_{N\geq 1}$ is an increasing sequence of finite sets which exhausts $\mathcal{F}$, i.e. there exists an $N_0\in\mathbb{N}$, s.t. any finite subset $\tilde{\Lambda}\subset \mathcal{F}$ is contained in all sets $\Lambda_N$ for all $N\geq N_0$.
  Then, the partial sums 
  \begin{equation}
    S_{\Lambda_N}u(z) = \sum_{\nu\in\Lambda_N}t_\nu z^\nu
  \end{equation}
  satisfy 
  \begin{equation}
    \lim_{N\to\infty}\sup_{z\in U}\|u(z) - S_{\lambda_N}u(z)\|_V =0 \text{.}
  \end{equation}
  
  If $\Lambda_N$ is a set of $\nu\in\mathcal{F}$ corresponding to the indicies of $N$ Taylor coefficients with largest norms $\|t_\nu\|_V$, it holds 
  \begin{equation}
    \label{56}
    \sup_{z\in U} \|u(z) -S_{\Lambda_N}u(z)\|_V\leq \left\|(\|t_\nu\|_V)_{\nu\in\mathcal{F}}\right\|_{L^p(\mathcal{F})} N^{-s}
  \end{equation}
  with $s=\frac{1}{p}-1$.
\end{theorem}

  Analyticity can e.q. be proved by a power series and bootstrapping argument, or by strong differentiability of $u$ w.r.t. complex coordinates $z_j$.
  If UEAC($r$,$R$) holds, then $z\mapsto u(z)$ is $V$-valued bounded analytic function in certain domains larger than $U$.
  To define $U$ we choose $0 < \delta \leq 2R <\infty$, and consider the sets 
  \begin{equation}
    \label{57}
    \mathcal{A}_\delta := \{z\in\mathbb{C}^\mathbb{N} \colon \delta \leq \text{Re}(a(x,z))\leq \vert a(x,z)\vert \leq 2R \text{ for } x\in D\}
  \end{equation}
  and $\mathcal{A}_\delta$ contains $U$ for $0<\delta<r$. 
  By Lax-Milgram, the unique solution $u(z)\in V$ for $z\in\mathcal{A}_\delta$ satisfies the a priori estimate
  \begin{equation}
    \label{58}
    \|u(z)\|_V\leq \frac{\|f\|_{V^*}}{\delta}
  \end{equation}
  Holomorphy of $u(z)$ is shown by the difference quotient argument which uses a pertubation lemma starting from a stability result. 
  \begin{lemma}
    \label{lemma 10}
    Let $u$ and $\tilde{u}$ be solutions of \ref{MP} with UEAC($r$,$R$) coefficients $\alpha$ and $\tilde{\alpha}$, then 
    \begin{equation}
      \label{59}
      \|u - \tilde{u}\|_V \leq \frac{\|f\|_{V^*}}{r^2}\|\alpha - \tilde{\alpha}\|_{L^\infty(D)}
    \end{equation}
  \end{lemma}
  Based on Lemma \ref{lemma 10} we are able to define holomorphy of the mapping $z\mapsto u(z)$.
  \begin{lemma}
    \label{lemma 11}
    For $z\in\mathcal{A}_\delta$, the function $z\mapsto u(z)$ admits a complex derivative $\partial_{z_j}u(z) \in V$, s.t., for all $v\in V$ and $z\in \mathcal{A}_\delta$,
    \begin{equation}
      \label{60}
      \int_D a(x,z)\nabla\partial_{z_j} u(x, u)\cdot\nabla v(x) dx = L_0(x) := -\int_D \psi_j(x)\nabla u(x,z)\cdot \nabla v(x) dx\text{.} 
    \end{equation}
  \end{lemma}
  \begin{proof}
    Fix $j\geq 1$ and $z\in\mathcal{A}_\delta$.
    For $h\in\mathbb{C}\backslash\{0\}$, set 
    \begin{equation}
      \label{64}
      w_h(z) := \frac{u(z+he_j) -u(z)}{h}\in V\text{.}
    \end{equation}
    Note that $w_h$ is the unique solution of the variational problem 
    \begin{equation}
      \int_D a(x,z)\nabla w_h(x,z)\cdot\nabla v(x) dx = -\int_D\psi_j\nabla u(x,z+he_j)-\nabla v(x)dx =: L_h(v)\text{.}
    \end{equation}