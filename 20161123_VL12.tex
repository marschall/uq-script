For $\vert h\vert\|\psi_j\|_{L^\infty(D)}\leq \frac{\delta}{2}$, $w_h$ is well-defined, since 
\begin{equation}
  \frac{\delta}{2}\leq \text{Re}(a(x, z+he_j))\leq \vert a(x, z+he_j)\vert \leq 2R+\frac{\delta}{2}\text{,}\quad x\in D
\end{equation}
by stability estimate ~\eqref{59}
\begin{equation}
  \|u(z+he_j) - u(z) \|_{V} = \|u(z +he_j) - \nabla u(z)\|_{L^2(D)} \leq \vert h\vert\|\psi_j\|_{L^\infty(D)}\frac{4\|f\|_V^*}{\delta}
\end{equation}
and $L_h$ converges towards $L_0$ in $V^*$ as $h\to\infty$.
This implies that $w_h$ converges in $V$ towards a limit $w_0\in V$ which is the solution of 
\begin{equation}
  \int_D a(x,z)\nabla w_=(x,z)\cdot\nabla v(x)dx = L_0(v)
\end{equation}
for $v\in V$.
Hence, $\partial_q u(z) = w_0$ exists in $V$ and is the unique solution of ~\eqref{60}.
\end{proof}

Note that the analyticity domain $\mathcal{A}_\gamma$ contains polydiscs, i.e. with a sequence $\rho=(\rho_j)_{j\geq 1}$ of positive radii, define the polydiscs 
\begin{equation}
  \label{62}
  \mathcal{U}_\rho:=\prod_{j\geq 1}\{z_j\in\mathbb{C} \colon \vert z_j\vert \leq \rho_j\} = \{z_j\in\mathbb{C} \colon z=(z_j)_{j\geq 1}\;\text{;}\; \vert z_j\vert \leq \rho_j\}
\end{equation}
\begin{definition}
  A sequence $\rho=(\rho_j)_{j\geq 1}$ is called \emph{admissible} if, for $x\in D$ 
  \begin{equation}
    \label{63} 
    \sum_{j\geq 1}\rho_j\vert \psi_j(x)\vert \leq \text{Re}(a_0(x))-\delta\text{,}
  \end{equation}
  and $U_\rho\subset\mathcal{A}_\delta$ for admissible $\rho$. 
  In this case, the linear UEAC is equivalent to 
  \begin{equation}
    \sum_{j\geq 1}\vert \psi_j(x)\vert \leq \text{Re}(a_0(x))-r \text{,}\quad x\in D\text{.}
  \end{equation}
\end{definition}
\begin{example}
  The sequence $\rho_j = 1$ is $\delta$-admissible for all $0<\delta\leq r$ and for $\delta< r$ there exists a $\delta$-admissible sequence, s.t. $\rho_j > 1$ for all $j\geq 1$, i.e. the polydiscs $\mathcal U_\rho$ is strictly larger than $\mathcal{U}$ in every variable.
\end{example}
\begin{lemma}[Estimate of Taylor coefficients]
  \label{lemma 12}
  If UEAC($r$,$R$) holds for some $0<r\leq R<\infty$ and if $\rho=(\rho_j)_{j\geq 1}$ is $\delta$-admissible for some $0<\delta <r$, then, for any $\nu\in\mathcal{F}$, it holds,
  \begin{equation}
    \label{65}
    \|t_\nu\|_V \leq \frac{\|f\|_{V^*}}{\delta}\prod_{j\geq 1}\rho_j^{-\nu_j} = \frac{\|f\|_{V^*}}{\delta}\rho^{-\nu}\text{,}
  \end{equation}
  with $t^{-0}=1$ for $t\geq 0$.
\end{lemma}
\begin{proof}[sketch of proof for Theorem \ref{theorem 5}]
  We assume UEAC($r$,$R$) and analyticity of the mapping $z\mapsto u(z)$, on the domain $\mathcal{A}_\delta$.
  The proof follows by
  \begin{enumerate}[(a)]
  \item a choice of a $\frac{\delta}{2}$-admissible sequence $\rho$
  \item establish $\ell^p(\mathcal{F})$-summability of the Taylor coefficients.
  \end{enumerate}
  By Lemma \ref{lemma 12} with $\delta=\frac{r}{2}$ follows for the Taylor coefficients 
  \begin{equation}
    \label{66}
    \|t_\nu\|_V\leq \frac{2\|f\|_{V^*}}{r}e^{-\rho}
  \end{equation}
  (one possible) construction of a $\delta$-admissible vector $\rho$: 
  \begin{itemize}
  \item select $J_0\in\mathbb{N}$, s.t. $\sum_{j > J_0}\|\psi_j\|_{L^\infty(D)}\leq \frac{r}{12}$
  \item partitioning $\mathbb{N}$ in $E:=\{1,\dots, J_0\}$ and $F:=\mathbb{N}\\ E$
  \item choose $\kappa > 1$ s.t. $(\kappa-1)\sum_{j\leq J_0}\|\psi_j\|_{L^\infty(D)}\leq \frac{r}{4}$
  \item 
  For each $\nu\in\mathcal{F}$, select $\rho=\rho(\nu)$ by
  \begin{equation}
    \rho_j := \begin{cases}
               \kappa & \quad \text{,}\; j\in E \\
               \max\{1, \frac{r\nu_j}{4\vert\nu_F\vert\|\psi_j\|_{L^\infty(D)}}\} & \quad\text{,}\; j\in F
    \end{cases}
  \end{equation}
  where $\nu_F$ denotes the restriction of $\nu$ to the set $F$ and $\vert\nu_F\vert := \sum_{j\geq J_0}\nu_j$.
  \item equation ~\eqref{65} takes the form 
  \begin{equation}
    \label{68}
    \|t_\nu\|_V\leq \frac{2\|f\|_{V^*}}{r}\left(\prod_{j\in E}\mu^{\nu_j}\right)\left(\prod_{j\in F}\left(\frac{\vert\nu_F\vert d_j}{\nu_j}\right)^{\nu_j}\right)\text{,}
  \end{equation}
  where $\mu:= \frac{1}{\kappa}< 1$ and $d_j:= \frac{4\|\psi_j\|_{L^\infty(D)}}{r}$
  \item $\ell^p(\mathcal{F})$-summability of $t_\nu$: ~\eqref{68} has the general form 
  \begin{equation}
    \label{69}
    \|t_\nu\|_V\leq C_r\alpha(\nu_E)\beta(\nu_F)    
  \end{equation}
  \item Let $\mathcal{F}_E$ be the collection of $\nu\in\mathcal{F}$ with support on $E$.
  For $0<p<\infty$, it holds
  \begin{equation}
    \label{70}
    \sum_{\nu\in\mathcal{F}}\|t_\nu\|^p_V\leq C_r^p\sum_{\nu\in\mathcal{F}}\alpha(\nu_E)^p\beta(\nu_F)^p = C_r^p A_EA_F \text{,}
  \end{equation}
  with 
  \begin{equation}
    A_E := \sum_{\nu\in\mathcal{F}_E}\alpha(\nu)^p \quad\text{and}\quad A_F:= \sum_{\nu\in\mathcal{F}_F}\beta(\nu)^p
  \end{equation}
  \item From $<\infty$ bounds for $A_E$ and $A_F$ follows $\ell^p(\mathcal{F})$-summability of $t_\nu$
  \item The best $N$-term convergence rate ~\eqref{56} in Theorem \ref{theorem 5} follows from Stechkin's Lemma.
  \end{itemize}
\end{proof}

\subsubsection{Convergence rates of Legendre expansion}
  Previously we have seen the rate of convergence for the best $N$-term truncation of the Taylor expansion in $y$.
  Analogously we can give a result on a Legendre expansion.
  Therefore, we consider two kinds of normalizations.
  One in $L^2$ and the other point-wise.
  \begin{equation}
    \label{L inf}
    \|P_n\|_{L^\infty([-1,1])} = P_n(1) = 1\text{,}
  \end{equation}
  \begin{equation}
    \label{L 2}
    \frac{1}{2}\int_{-1}^1\vert L_n(t)\vert^2 dt = 1
  \end{equation}
  with $L_n(t) = \sqrt{2n-1}P_n(t)$.
  \begin{remark}
    Note that since $u\in L^\infty(\Gamma, \mu;V)\subset L^2(\Gamma,\mu;V)$, it admits a unique expansion
    \begin{equation}
      \label{71}
      u(y) = \sum_{\nu\in\mathcal{F}}u_\nu P_\nu(y) = \sum_{\nu\in\mathcal{F}}v_\nu L_\nu(y)
    \end{equation}
    which converges in $L^2(\Gamma,\mu;V)$ with coefficients $u_\nu, v_\nu\in V$,
    \begin{equation}
      \label{72}
      u_\nu ;= \int_\Gamma u(y)L_\nu(y)\mu(dy) \quad\text{and}\quad v_\nu:=\left(\prod_{j\geq 1}(1+2\nu_j)\right)^\frac{1}{2}u_\nu\text{.}
    \end{equation}
    Thus,
    \begin{equation}
    \|v_\nu\|_V\leq \|u\|_V
    \end{equation}
    since 
    \begin{equation}
      \|u_\nu\|_V = \left(\prod_{j\geq 1}(1+2\nu_j)\right)^\frac{1}{2}\|v_\nu\|_V\text{,}\quad \nu\in\mathcal{F}
    \end{equation}
    and therefore it is sufficient to show $\ell^p$-summability of $\left(\|u_\nu\|_V\right)_{\nu\in\mathcal{F}}$.
  \end{remark}
  \begin{lemma}
    \label{lemma 13}
    Assume UEAC($r$,$R$) for some $0<r\leq R<\infty$ and a $\delta$-admissible sequence $\rho=(\rho_j)_{j\geq 1}$, for some $0<\delta< r$, that satisfies $\rho_j >1$ for all $j\in\bbN$, s.t. $\nu_j\neq 0$.
    Then, for any $\nu\in\mathcal{F}$,
    \begin{equation}
      \label{73}
      \|u_\nu\|_V \leq \frac{\|f\|_{V^*}}{\delta}\prod_{j\geq 1,\nu_j\neq 0}\varphi(\rho_j)(2\nu_j +1)\rho_j^{-\nu_j}\text{,}
    \end{equation}
    where $\varphi(t) := \frac{\pi t}{2(t-1)}$ for $t > 1$.
  \end{lemma}
  As a consequence we can formulate
  \begin{theorem}
    \label{theorem 6}
    For $a(x,z)$ satisfying UEAC($r$,$R$) for some $0<r\leq R< 0$ and $\left(\psi_j\|_{L^\infty(D)}\right)_{j\geq 1}\in\ell^p(\bbN)$ for some $p<1$, the sequences $\left(\|u_\nu\|_V\right)_{\nu\in\mathcal{F}}$ and $\left(\|v_\nu\|_V\right)_{\nu\in\mathcal{F}}$ belong to $\ell^p(\mathcal{F})$ for some value of $p$.
    The Legendre expansion ~\eqref{71} converges in $L^\infty(\Gamma,\mu;V)$ in the following sense.
    
    If $(\Lambda_N)_{N\geq 1}$ is any sequence exhausting $\mathcal{F}$, then the partial sums 
    \begin{equation}
      S_{\Lambda_N}u(y) := \sum_{\nu\in\Lambda_N}u_\nu(x)P_\nu(y)=\sum_{\nu\in\Lambda_N}v_\nu(x)L_\nu(y)
    \end{equation}
    satisfy
    \begin{equation}
      \lim_{N\to\infty}\sup_{y\in\Gamma}\|u(y)-S_{\Lambda_N}u(y)\|_V = 0\text{.}
    \end{equation}
    If $\Lambda_N$ is a set of $\nu\in\mathcal{F}$ corresponding to indices of $N$ maximal $\|u_\nu\|_V$, 
    \begin{equation}
      \sup_{y\in\Gamma}\|u(y)-S_{\Lambda_N}u(y)\|_V\leq \left(\|u_\nu\|_V\right)_{\ell^p(\mathcal{F})}N^{-s}\text{, }\quad s:=\frac{1}{p}-1\text{.}
    \end{equation}
    If $\Lambda_N$ is a set of $\nu\in\mathcal{F}$ corresponding to indices of $N$ maximal $\|v_\nu\|_V$, 
    \begin{equation}
      \|u-S_{\Lambda_N}u\|_{L^2(\Gamma,\mu;V)}\leq \left(\|v_\nu\|_V\right)_{\ell^p(\mathcal{F})}N^{-s}\text{, }\quad s:=\frac{1}{p}-\frac{1}{2}\text{.}
    \end{equation}
  \end{theorem}
  