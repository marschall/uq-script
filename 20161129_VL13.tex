\subsection{Adaptive stochastic Galerkin FEM}

Our aim is to propose an algorithm to construct an index-set $\Lambda\subset\mathcal{F}$ and according finite element spaces, such that the Galerkin approximation of the parametric variational problem converges (almost) quasi-optimally.

\begin{remark}
  Using the three term recursion of orthogonal polynomials in ~\eqref{8} and the polynomial expansion of ~\eqref{71}, the coefficients $(u_\nu)_{\nu\in\mathcal{F}}$ are determined by 
  \begin{equation}
    A_0u_\nu + \sum_{m\geq 1}A_m(\beta_{\nu_m+1}^mu_{\nu+\epsilon_m}+\beta^m_{\nu_m}u_{\nu-\epsilon_m}) = f\delta_{0,\nu}\text{,}\quad \text{for all }\nu\in\mathcal{F}\text{.}  \end{equation}
\end{remark}
\begin{definition}
For $\Lambda\subset \mathcal{F}$ define $\text{supp}(\Lambda)\subset\bbN$ as set of \emph{active dimensions}
\begin{equation}
  \label{activeDimensions}
  \supp(\Lambda) := \bigcup_{\nu\in\mathcal{F}}\supp(\nu)\text{.}
\end{equation}
Define the \emph{boundary} of $\Lambda$ as the infinite set 
\begin{equation}
  \label{boundary}
  \partial\Lambda := \{\nu\in\mathcal{F}\backslash\Lambda \colon \exists m\in\bbN \text{ s.t } \nu-\epsilon_m\in\Lambda \text{ or } \nu+\epsilon_m\in\Lambda\}\text{.}
\end{equation}
Define the \emph{active boundary} of $\Lambda$
\begin{equation}
  \label{activeBoundary}
  \partial^\circ\Lambda := \{\nu\in\mathcal{F}\backslash\Lambda \colon \exists m\in\supp(\Lambda) \text{ s.t } \nu-\epsilon_m\in\Lambda \text{ or } \nu+\epsilon_m\in\Lambda\}\text{.}
\end{equation}
\end{definition}

The first part of the subspace construction is based on the chosen active dimension. 
For $\Lambda\subset\mathcal{F}$ the Galerkin projection of $u$ onto the space 
\begin{equation}
  \mathcal{V}(\Lambda) :=\{v_\Lambda(x,y) = \sum_{\nu\in\Lambda}v_{\Lambda,\nu}(x)P_\nu(y)\colon v_{\Lambda, \nu}\in V\}\subset L^2(\Gamma, \mu;V)
\end{equation}
is the unique $u_\Lambda\in \mathcal{V}(\Lambda)$ satisfying 
\begin{equation}
  \label{LambdaVariational}
  \int_\Gamma\langle A(y)u_\Lambda(y), v_\Lambda(y)\rangle d\mu(y) = \int_\Gamma\int_D f(x)v_\Lambda(y) dxd\mu\text{,}\quad\forall v_\Lambda\in\mathcal{V}(\Lambda)\text{.}
\end{equation}
  
 To further restrict the problem in ~\eqref{LambdaVariational} we assume a conform mesh triangulation $\mathcal{T}$ of $D$ and consider the space of continuous piecewise polynomials of some fixed degree $p$: $\mathcal{V}_p(\mathcal{T})$.
For $\Lambda\subset\mathcal{F}$ and $p\geq 0$ define the subspace of $\mathcal{V}(\Lambda)$ 
\begin{equation}
  \mathcal{V}_p(\Lambda,\mathcal{T}) :=\{v_N(x,y) = \sum_{\nu\in\Lambda}v_{N,\nu}(x)P_\nu(y)\colon v_{N, \nu}\in \mathcal{V}_p(\mathcal{T})\}
\end{equation}
\begin{remark}
  $\mathcal{V}_p(\mathcal{T})$ is a finite dimensional subspace of $L^2(\Gamma,\mu;V)$ and analogously to ~\eqref{LambdaVariational}, the Galerkin approximation of $u_N$ of $u$ is the solution of the variational problem 
  \begin{equation}
    \label{TVariational}
    \int_\Gamma\langle A(y)u_N(y), v_N(y)\rangle d\mu(y) = \int_\Gamma\int_D f(x)v_N(y) dxd\mu\text{,}\quad\forall v_N\in\mathcal{V}_p(\Lambda,\mathcal{T})\text{.}
  \end{equation}
\end{remark}

The object of main interest is the residual
\begin{definition}
  For any approximation $w_\Lambda\in\mathcal{V}(\Lambda)$ of $u$ the \emph{residual operator} $\mathcal{R}(w_\Lambda)\in L^2(\Gamma,\mu;V^*)$ is given by 
  \begin{equation}
    \label{residualOperator}
    \mathcal{R}(w_\Lambda):= f-\mathcal{A}w_\Lambda = \mathcal{A}(u-w_\Lambda)\text{.}
  \end{equation}
\end{definition}
Given the basis representation we have $\mathcal{R}(w_\Lambda) = \sum_{\nu\in\mathcal{F}}r_\nu(w_\Lambda)P_\nu$ with convergence in $L^2(\Gamma,\mu;V^*)$ and the coefficients are given by 
\begin{equation}
  r_\nu(w_\Lambda) = f\delta_{\nu, 0} - A_0w_{\Lambda,\nu} - \sum_{m\geq 1}A_m(\beta^m_{\nu_{m}+1}w_{\Lambda, \nu+\epsilon_m} + \beta^m_{\nu_m}w_{\Lambda, \nu-\epsilon_m})\text{,} \quad\nu\in\mathcal{F}\text{,} 
\end{equation}
i.e.
\begin{equation}
  \langle r_\nu(w_\Lambda), v\rangle = \int_D f\delta_{\nu,0}-\sigma_\nu(w_\Lambda)\cdot\nabla v dx \quad\forall\; v\in V
\end{equation}
for
\begin{equation}
  \sigma_\nu(w_\Lambda) := a_0\nabla w_{\Lambda,\nu} + \sum_{m\geq 1} a_m\nabla (\beta^m_{\nu_m+1}w_{\Lambda, \nu+\epsilon_m} + \beta^m_{\nu_m}w_{\Lambda,\nu-\epsilon_m})\text{,}\quad\nu\in\mathcal{F}\text{.}
\end{equation}
\begin{remark}
  Note that, $r_\nu(w_\Lambda)$ is nonzero only for $\nu\in\Lambda\cup\partial\Lambda$.
  Thus, we have the decomposition of the residual on the active domain and the boundary, i.e. $\mathcal{R}(w_\Lambda) = \mathcal{R}_\Lambda(w_\Lambda) + \mathcal{R}_{\partial\Lambda}(w_\Lambda)$ where, for a set $\Xi\subset\mathcal{F}$, we define 
  \begin{equation}
    R_\Xi(w_\Lambda) := \sum_{\nu\in\Xi}r_\nu(w_\Lambda) P_\nu\text{.}
  \end{equation}
  Consequently we obtain
  \begin{equation}
    \|\mathcal{R}(w_\Lambda)\|_{L^2(\Gamma,\mu,V^*)}^2 = \|\mathcal{R}_\Lambda(w_\Lambda)\|_{L^2(\Gamma,\mu,V^*)}^2 + \|\mathcal{R}_{\partial\Lambda}(w_\Lambda)\|_{L^2(\Gamma,\mu,V^*)}^2\text{.}
  \end{equation}
\end{remark}
\begin{lemma}
  \label{lemma 1.1}
  For any $w_\Lambda\in \mathcal{V}(\Lambda)$, 
  \begin{align}
   & \quad\frac{1}{1+\gamma}\left(\|\mathcal{R}_\Lambda(w_\Lambda)\|^2_{L^2(\Gamma, \mu;V)} +\|\mathcal{R}_{\partial\Lambda}(w_\Lambda)\|^2_{L^2(\Gamma,\mu;V)}\right)&
   \\ &\leq \|w_\Lambda-u\|_\mathcal{A} \\
   & \leq \frac{1}{1-\gamma	}\left(\|\mathcal{R}_\Lambda(w_\Lambda)\|^2_{L^2(\Gamma,\mu;V)} +\|\mathcal{R}_{\partial\Lambda}(w_\Lambda)\|^2_{L^2(\Gamma,\mu;V)}\right)
  \end{align}
\end{lemma}
\begin{proof}
  By Riesz representation Theorem in $L^2(\Gamma,\mu;V)$, 
  \begin{equation}
  \label{RiezHint}
    \|u-w_\Lambda\|^2_\mathcal{A} = \sup_{v\in L^2(\Gamma,\mu;V)} \frac{\vert\langle\mathcal{A}(u-w_\Lambda),v\rangle\vert^2}{\|v\|_\mathcal{A}^2} = \sup_{v\in L^2(\Gamma,\mu ;V)}\frac{\vert\langle\mathcal{R}_\Lambda(w_\Lambda),v\rangle\vert^2}{\|v\|^2_\mathcal{A}}\textbf{.}
  \end{equation}
  Then, by Cauchy-Schwarz and the equivalenz of the energy norm and the $L^2(\Gamma,\mu;V)$-norm, the assertion follows.
\end{proof}
\begin{lemma}
  For any $w_\Lambda\in \mathcal{V}(\Lambda)$, we have
  \begin{equation}
    \frac{1}{1-\gamma}\|\mathcal{R}(w_\Lambda)\|^2_{L^2(\Gamma,\mu ;V)} \leq \|w_\Lambda-u_\Lambda\|^2_\mathcal{A}\leq \frac{1}{1-\gamma}\|\mathcal{R}_\Lambda(w_\Lambda)\|^2_{L^2(\Gamma,\mu ;V)}
  \end{equation}
\end{lemma}
\begin{proof}
  For any $v_\Lambda\in \mathcal{V} (\Lambda)\subset L^2(\Gamma,\mu ;V)$
  \begin{equation}
    \langle\mathcal{A}(u_\Lambda-w_\Lambda),v_\Lambda\rangle = \langle \mathcal{A}(u-w_\Lambda),v_\Lambda\rangle = \langle\mathcal{R}(w_\Lambda),v_\Lambda\rangle\text{ .}
  \end{equation}
  Then, the proof follows by \ref{RiezHint} and the same argument as in Lemma \ref{lemma 1.1}.
\end{proof}
Note that by Galerkin orthogonality, 
\begin{equation}
  \label{eq:errorGalerkinOrthogonality}
  \|u-u_N\|^2_\mathcal{A} = \|u-u_\Lambda\|^2_\mathcal{A} + \|u_\Lambda-u_N\|^2_\mathcal{A}\text{.}
\end{equation}
In the following we will investigate each part of \eqref{eq:errorGalerkinOrthogonality} for itself.

\subsubsection{Tail estimator}
The adpative process will rely on the computation of suitable error estimators for the stochastic \emph{tail} and the physical part of the solution.
We first approach the stochastic approximation and define a suitable error indicator yielding a decision support to define the stochastic space approximation, i.e. taking greater polynomial degrees in consideration.
For any $w_\Lambda\in \mathcal{V}(\Lambda)$ and $v\in\partial\Lambda$, let
\begin{equation}
\label{ZetaSchaetzer}
\zeta_\nu(w_\Lambda):=\sum_{m=1}^\infty\left\|\frac{a_m}{a_0}\right\|_{L^\infty(D)}(\beta^m_{\nu_m+1}\|w_{\Lambda , \nu+\epsilon_m}\|_V + \beta^m_{\nu_m}\|w_{\Lambda , \nu-\epsilon_m}\|_V)\text{.}
\end{equation}
For $\Delta\subset\partial\Lambda$, let 
\begin{equation} 
  \zeta(w_\Lambda,\Delta) := \left(\sum_{\nu\in\Delta}\zeta_\nu(w_\Lambda)^2\right)^{\frac{1}{2}}  
\end{equation}
\begin{lemma}
  For any $w_\Lambda\in \mathcal{V}(\Lambda)$
  \begin{equation}
    \|\mathcal{R}_{\partial\Lambda}(w_\Lambda)\|_{L^2(\Gamma,\mu;V^*)}\leq \zeta(w_\Lambda,\partial\Lambda)\text{.}
  \end{equation}
\end{lemma}
\begin{proof}
 By Parseval's identity we have
 \begin{equation}
   \|\mathcal{R}_{\partial\Lambda}(w_\Lambda)\|^2_{L^2(\Gamma,\mu;V^*)} = \sum_{ \nu\in\partial\Lambda}\| r_\nu(w_\Lambda)\|^2_{V^*}\text{.}
 \end{equation}
 Since $\nu\neq 0\textbf{ } (0\in\Lambda)$, and (64*) and Cauchy Schwarz and triangle inequality leads to\todo{That sentence has to be reformulated}
 \begin{align}
   \|r_\Lambda(w_\Lambda)\|_{V^*} &=\sup\frac{\vert\langle r_\Lambda(w_\Lambda),v\rangle\vert}{\|v\|_V} \\ &= \sup\frac{1}{\|v\|_V}\left\vert\int_D a_0^{-1}\sigma_v(w_\Lambda)\nabla(a_0v) dx\right\vert \\ &\leq \|a_0^{-1}v(w_\Lambda)\|_V\leq\zeta_v(w_\Lambda)\text{.}
 \end{align}
\end{proof}
 Note that $\zeta(w_\Lambda,\partial\Lambda)$ is an infinite sum in (68) due to $\vert\partial\Lambda\vert = \infty$. 
 However, for $v\in\partial\Lambda \backslash\partial^\circ\Lambda$, i.e. we have $  v =\mu+\epsilon_n$ for $\mu\in\Lambda, m\in\mathbb{N} \backslash \supp(\Lambda)$
 \begin{equation}
   \label{zetaSchaetzer2}
   \zeta_v(w_\Lambda)=\left\|\frac{a_m}{a_0}\right\|_{L^\infty(D)}\beta^m_1\|w_{\Lambda,\mu}\|_V\text{.}
 \end{equation} 
 Summing over all inactive dimensions,
 \begin{align}
   \label{zetaSum}
   \zeta_\mu(w_\Lambda,\Lambda) &:= \left(\sum_{m\in\mathbb{N}\backslash\supp(\Lambda)}\zeta_{\mu+\epsilon_m}(w_\Lambda)^2\right)^\frac{1}{2} \\ &= \|w_{\Lambda,\mu}\|_V \left(\sum_{m\in\mathbb{N}\backslash\supp(\Lambda)}\left(\left\|\frac{a_m}{a_0}\right\|_{L^\infty(D)}\beta^m_1\right)^2\right)^\frac{1}{2}
 \end{align}
 for $\mu\in\Lambda$. 
 Hence,
 \begin{equation}
   \label{zetaSquare}
 \zeta(w_\Lambda,\partial\Lambda)^2=\sum_{v\in\partial^\circ\Lambda}\zeta_v(w_\Lambda)^2+\sum_{\mu\in \Lambda}\zeta_\mu(w_\Lambda)^2\text{.}
 \end{equation}
 
\subsubsection{Residual based estimation of spatial error}
 Assume a regular triangulation with a triangle $T\in\mathcal{T}$ of $D$ and edge $E\in\mathcal{E}$, where $\mathcal{E}$ denotes the set of edges for a triangluation, $h_T:=\diam(T)$, $h_E:=\diam(E)$ and patches $\widetilde{\omega}_T$, $\widetilde{\omega}_E$. 
 Then, there exists some interpolation operator $J\colon H^1_0\to \mathcal{V}_p(\mathcal{T})$, s.t.
 \begin{equation}
 \label{interpolationOperator}
   \|a_0^{-\frac{1}{2}}(v-Jv)\|_{L^2(T)}\leq c_\mathcal{T}h_T\vert v\vert_{V, \widetilde{\omega}_T}\text{,} \quad T\in\mathcal{T} 
 \end{equation}
 and 
 \begin{equation}
  \|a_0^{-\frac{1}{2}}(v-Jv)\|_{L^2(T)}\leq c_\mathcal{E}h^\frac{1}{2}_E\vert v\vert_{V,\tilde{\omega}_E}\text{,}\quad E\in\mathcal{E}\text{.}
 \end{equation}
 For any $T\in\mathcal{T}$ and $\omega_N\in \mathcal{V}_p(\Lambda,\mathcal{T})$ let, 
 \begin{equation}
   \eta_{\mu,T}(w_N):=h_T\|a_0^{-\frac{1}{2}}(f\delta_{\mu 0}+\nabla\cdot\sigma(w_N))\|_{L^2(T)}\text{,}\quad\mu\in\Lambda
 \end{equation}
 denote the energy in the interior and 
 \begin{equation}
   \eta_{\mu,E}(w_N):=h_E^\frac{1}{2}\|a_0^{-\frac{1}{2}}\llbracket\sigma_\mu(w_N)\rrbracket\|_{L^2(E)}\text{,} \quad \mu\in\Lambda
 \end{equation}
 the flow over the edge $E=T_1\cup T_2$, With the jump term
 \begin{equation}
   \llbracket\sigma\rrbracket:=\sigma\vert_{T_1}\times n_1+\sigma\vert_{T_2}\times n_2
 \end{equation}  
 defined by the corresponding normal directions $n_i$ of $T_i$.
 Then the physical error indicator is given by
 \begin{equation}
   \eta_\mu(w_N):=\left(\sum_{T\in\mathcal{T}}\eta_{\mu,T}(w_N)^2 + \sum_{E\in\mathcal{E}}\eta_{\mu,E}(\omega_N)^2\right)^\frac{1}{2}\text{.}
 \end{equation}
 \begin{theorem}
   %\label{26}
   For $w_N\in \mathcal{V}_p(\Lambda;\mathcal{T})$, $\nu\in\Lambda, v\in V$
   \begin{equation}
   \label{etaSchaetzerTheorem}
   \|\langle r_\nu(w_N),v-Jv\rangle\|\leq c_\eta \eta_\nu(w_N)\|v\|_V
   \end{equation}
   with $c_\eta > 0$ depending only on $a_0$ and $\mathcal{T}$.
 \end{theorem}
 \begin{proof}
   Set $z:=v-Jv$, $\sigma_\nu :=\sigma_\nu(w_N)$. 
   Then, it follows
   \begin{align}
     \langle r_\nu(w_N),z\rangle &= \sum_{T\in\mathcal{T}}\int_T f\delta_{\nu 0}z-\sigma_\mu\cdot\nabla z dx \\ &= \sum_{T\in\mathcal{T}}\int_T (f\delta_{\nu 0} + \nabla \sigma_\nu)z dx - \sum_{E\in\mathcal{E}}\int_E\llbracket\sigma_\nu\rrbracket z ds\text{.}
   \end{align}
   By Cauchy Schwarz we obtain
   \begin{equation}
     \vert \langle r_\nu(w_N),z\rangle\vert \leq \sum_{T\in\mathcal{T}}\|a_0^{-\frac{1}{2}}(f\delta_{\nu 0}+\nabla\sigma_\nu)\|_{L^2(T)} + \sum_{E\in\mathcal{E}}\|a_0^{-\frac{1}{2}}\llbracket\sigma_\nu\rrbracket\|_{L^2(E)}\|a_0^{-\frac{1}{2}}z\|_{L^2(T)}\text{.}
   \end{equation}
   By \eqref{interpolationOperator}
   \begin{align}
     \vert \langle r_\nu(w_N),z\rangle\vert &\leq c_T\sum_{T\in\mathcal{T}}h_T\|a_0^{-\frac{1}{2}}(f\delta_{0\nu}+\nabla\sigma_{\nu})\|_{L^2(T)}\vert v\vert_{V,\widetilde{w}_\mathcal{T}} \\ &\qquad\qquad + c_\mathcal{E}\sum_{E\in\mathcal{E}}g_E^{-\frac{1}{2}}\|a_0^{-\frac{1}{2}}\llbracket\sigma_\nu\rrbracket\|_{L^2(E)}\vert v\vert_{V,\widetilde{w}_E} \\ &\leq c_\eta\eta_\nu(w_N)\|v\|_V\text{.}	
    \end{align}    
 \end{proof}
 
 %Ab hier VL vom 28.1.16 %%%%% weiter machen mit Korrektur
 \subsubsection{Upper Bound of total error}
 Assume some
 \begin{equation}
 % \label{75}
Q\colon L^2(\Gamma, \mu;\mathcal{T})\to \mathcal{V}_p(\Lambda;\Gamma)\text{, } v\mapsto Qv:=\sum_{\nu\in\Lambda}(Iv_\nu)P_\nu 
 \end{equation}
 with
 \begin{equation}
   v=\sum_{\nu\in\mathcal{F}}v_\nu P_\nu\in L^2(\Gamma,\mu;V) \text{,}
 \end{equation}
 and define
 \begin{equation}
  c_Q := \|id-Q\|_\mathcal{A^*}\text{.}
 \end{equation}
 \begin{theorem}
   \label{theorem GalerkinError}
   For $w_N\in \mathcal V_p(\Gamma;\mathcal{T})$ and $u_N$ as the Galerkin projection of $u$ onto $\mathcal V_p(\Lambda;\mathcal{T})$, then 
   \begin{align}
     \| w_N-u\|^2_\mathcal{A} &\leq \left(\frac{1}{\sqrt{1-\gamma}}\sup_{v\in L^2_\pi(\Gamma;V)}\frac{\vert\langle \mathcal{R}(w_N),v-Qv\rangle\vert}{\|v\|_{L^2_\pi(\Gamma;V)}}  + c_Q\|w_N-u_N\|_\mathcal{A}\right)^2 \\ & \qquad\qquad + \|w_N-u_N\|^2_\mathcal{A}\text{.}
   \end{align}
 \end{theorem}
 \begin{proof}
   By orthogonality, we have 
   \begin{equation}
     \|w_N-u\|^2_\mathcal{A} = \|u_N-u\|^2_\mathcal{A}+\|w_N-u_N\|^2_\mathcal{A}
   \end{equation}
   and
   \begin{align}
     \|u_N-u\|_\mathcal{A} &= \sup_{v\in L^2(\Gamma,\mu;V)}\frac{\vert\langle \mathcal{R}(u_N),v\rangle\vert}{\|v\|_{\mathcal{A}}} \\ &= \sup_{v\in L^2(\Gamma,\mu;V)}\inf_{v_N\in \mathcal V_p(\Gamma;\mathcal{F})}\frac{\vert\langle \mathcal{R}(u_N),v-v_N\rangle\vert}{\|v\|_{\mathcal{A}}}\text{.}
   \end{align}
   By Cauchy Schwarz we get
   \begin{align}
     \vert \langle \mathcal{R}(u_N)-\mathcal{R}(w_N),v-v_N\rangle\vert &= \vert \langle\mathcal{A}(w_N-u_N),v-v_N\rangle\vert \\ &\leq \|w_N-u_N\|_\mathcal{A}\|v-v_N\|_\mathcal{A} \text{.}
   \end{align}
   The Claim follows with the norm equivalence and $v_N:= Qv$.
 \end{proof}
 \begin{theorem}
 \label{Thm28}
   For $w_N\in \mathcal V_p(\Lambda;\mathcal{T})$ we have, 
   \begin{align}
     %\label{76}
     \|w_N-u\|^2_\mathcal{A} &\leq \left\{\frac{c_\eta}{\sqrt{1-\gamma}}\left(\sum_{\nu\in\Lambda}	 \eta_\nu (w_N)^2\right)^\frac{1}{2} + \frac{c_Q}{\sqrt{1+\gamma}}\zeta(w_N,\partial\Lambda) + c_Q\|w_N-u_N\|^2_\mathcal{A}\right\}^2 \\ & \qquad\qquad+ \|w_N-u_N\|^2_\mathcal{A}\text{.}
   \end{align}
 \end{theorem}
 \begin{proof}
   \begin{align}
     \frac{\vert\langle \mathcal{R}(w_N),v-Qv\rangle\vert}{\|v\|_{L^2(\Gamma,\mu;V)}} &\leq \frac{1}{\|v\|_{L^2(\Gamma,\mu;V}} \underbrace{\|\mathcal{R}_{\partial\Lambda}(w_N)\|_{L^2(\Gamma,\mu;V^*)}}_{\text{Lem. \ref{lemma 1.1}: }\leq \zeta(w_N,\partial\Lambda}\underbrace{\|v-Qv\|_{L^2(\Gamma,\mu;V)}}_{\leq c_Q\|v\|_{L^2(\Gamma,\mu;V)}} \\ & \qquad\qquad + \frac{1}{\|v\|_{L^2(\Gamma,\mu;V)}} \sum_{\nu\in\Lambda}\vert\langle r_\nu (w_N), [v-Qv]_\nu\rangle\vert   
   \end{align}
   For the latter we have by Theorem \ref{theorem GalerkinError}
   \begin{align}
   \sum_{\nu\in\Lambda}\vert\langle r_\nu (w_N), [v-Qv]_\nu\rangle\vert &\leq c_\eta\sum_{\nu\in\Lambda}\eta_\nu (w_N)\|v_\nu\| \\ &\leq c_\eta (\sum_{\nu\in\Lambda}\eta_\nu (w_N)^2)^\frac{1}{2}(\sum_{\nu\in\Lambda}\|v_\nu\|^2_V)^\frac{1}{2}\text{,}
   \end{align}
   which concludes the statement.
 \end{proof}
 \subsubsection{refinement strategy (edge based FEM)}
 For $E\in\mathcal{E}$ and for $\nu\Lambda$ define
 \begin{equation}
 \widehat{\eta}_{\nu,E}(w_N):= \left(\eta_{\nu,E}(w_N)^2+\frac{1}{d+1}\sum_{T\colon E\in\mathcal{E}\cup\partial T}\eta_{\mu,T}(w_N)^2\right)^\frac{1}{2}
 \end{equation}
 In the physical domain we use the marking method of D\"orfler, i.e., for a parameter $0\leq\rho_\eta < 1$, we refine a set of edges $\widehat{\mathcal{E}}_\eta\subset\mathcal{E}$, if 
 \begin{equation}
 \sum_{\nu\in\Lambda}\sum_{R\in\widehat{\mathcal{E}}_\eta}\eta_{\nu,E}(w_N)^2 \geq \rho^2_\eta\sum_{\nu\in\Lambda}\eta_\nu(w_N)^2\text{.}
 \end{equation}
 Similar, in the stochastic domain, marking with $0 < \rho_\zeta <1$ w.r.t the estimator $\zeta$ or for all indices larger than a given threshold, we e.q. add indices to $\Lambda$, which are in the following set 
 \begin{equation}
   \left\{v\in\partial^0\Lambda \colon \zeta_v(w_N)\geq\rho_\zeta\left(\sum_{v\in\partial^0\Lambda}\zeta_(w_N)^2\right)^\frac{1}{2}\right\}
 \end{equation}
 and if some of these indices fulfills $v = \nu+\epsilon_m$ with $\nu\in\Lambda$ and $m=\max(\supp(\Lambda))$, then we add an index $v\prime = \nu +\epsilon_m$ with $m\prime=\min(\mathbb{N}\backslash \supp(\Lambda))$ and $\zeta_{v\prime}(w_N) = \left\|\frac{a_m}{a_0}\right\|_{L^\infty(D)}\|w_{N,\mu}\|_V$ is also considered for marking.
 
 The algorithm reads as follows:
 
% \begin{algorithm}
   \begin{enumerate}
     \item{compute approximation $w_N \approx u_N$ in $\mathcal V_p(\Gamma;\mathcal{T})$}
     \item{compute estimators $\eta_{\nu.T}$, $\eta_{\nu ,E}$ and $\zeta_\nu$}
     \item{consistence error $\|w_N-u_M\|$ can be estimated by residual}
     \item{decide whether to refine mesh $\mathcal{T}$ or increase active set $\Lambda$}
   \end{enumerate}    
% \end{algorithm}
 \subsection{Structure of discrete operator}
 Recall the variational problem representation using the parametrization and the recursive structure of the polynomials.
 Given a Triangulation $\mathcal{T}$ and an active index set $\Lambda$, for $v\in \mathcal{V}_p(\mathcal{T},\Lambda)$ we have
 \begin{equation}
   \langle A_0 u_{N,\nu},v\rangle + \sum_{m\geq 1}\left(\langle \beta^m_{\nu_m+a}A_m u_{N,\nu_m+\epsilon_m},v\rangle + \langle\beta^m_{\nu_m} A_m u_{N,\nu_m -\epsilon_m},v\rangle\right) = \langle f\delta_{\nu_0},v\rangle \text{.}
 \end{equation}
 Now, let $N:=\dim \mathcal{V}_p(\mathcal{T},\Lambda) < \infty$, $\tilde{N}:=\vert\Lambda\vert N$. 
 The operator equation $ \mathbb{A}\mathbf{u}=f$ can be written as
 \begin{equation}
  \begin{bmatrix}
   \ddots & & & & & & \ddots \\
   \dots & A_0 &\dots & C_m & \dots & 0 & \dots\\
   \dots & B_m &\dots & A_0 & \dots & C_m & \dots\\
   \dots & 0 &\dots & B_m & \dots & A_0 & \dots\\
  \end{bmatrix}
 \begin{bmatrix}
   \hdots \\ u_{\nu-\epsilon_m} \\ \hdots \\ u_\nu \\ \hdots \\ u_{\nu + \epsilon_m} \\ \hdots
 \end{bmatrix}
 = 
 \begin{bmatrix}
   \hdots \\ f_{\nu-\epsilon_m} \\ \hdots \\ f_\nu \\ \hdots \\ f_{\nu + \epsilon_m} \\ \hdots
 \end{bmatrix}
 \end{equation}
 with symmetric $\mathbb{A}\in\mathbb{R}^{\tilde{N}, \tilde{N}}$, $\mathbf{u}\in\mathbb{R}^{\tilde{N}}$ and for $\mathcal{V}_p(\mathcal{T},\Lambda) = \text{span}\{\varphi_i\}_{i=1,\dots N}$ we have 
 \begin{equation}
   [A_0]_{i,j} := \langle A_0\varphi_i,\varphi_j\rangle\text{.}
   [B_m]_{i,j} := \beta_{\nu_m}^m \langle A_m\varphi_i,\varphi_j\rangle
   [C_m]_{i,j} := \beta_{\nu_m+1}^m \langle A_m\varphi_i,\varphi_j\rangle
 \end{equation}
 $A_m,B_m,C_m\in\mathbb{R}^{N,N}$ and 
 \begin{equation}
   [B_m]_{i,j} := \beta_{\nu_m}^m \langle A_m\varphi_i,\varphi_j\rangle
   [C_m]_{i,j} := \beta_{\nu_m+1}^m \langle A_m\varphi_i,\varphi_j\rangle
 \end{equation}
 
 \section{Tensor Representation of the Model Problem}
 Let $V:= \mathcal{X}\otimes\mathcal{Y}$ with $\mathcal{X}:=H_0^1(D)$, $\mathcal{Y}:= \bigotimes_{m=1}^\infty L^2(\Gamma_m,\mu_m)$ and $V_M:=\mathcal{X}\otimes\mathcal{Y}_M$ with $\mathcal{Y}_M:=\bigotimes_{m=1}^M L^2(\Gamma_m, \mu_m)$. 
 Moreover, consider the full tensor set with dimensions $d_m$, i.e. 
 \begin{equation}
   \Lambda:= \{(\mu_1, \dots, \mu_M, 0,\dots)\in\mathcal{F} : \mu_m=0,\dots, d_m-1; m=1,\dots , M\}
 \end{equation}
 with cardinality $\vert\Lambda\vert = \prod_{m=1}^M d_m$ which grows exponentially for $d_m = 1$. Let 
 \begin{equation}
   V(\Lambda):= \mathcal{X}\otimes\mathcal{Y}(\Lambda):= \mathcal{X} \otimes (\bigotimes_{m=1}^M\mathcal{Y}_m) 
 \end{equation}
 with $\mathcal{Y}_m := \text{span}\{P_{\mu_m}:\mu_m=0,\dots,d_m-1\}$.
 The tensor structure of $V$ leads to a formulation in tensor representation. 
 For any $u_N\in \mathcal V_p(\Gamma;\Lambda)$,
 \begin{equation}
   %\label{76}
   u_N(x,y) = \sum_{i=0}^{N-1}\sum_{\nu\in\Lambda} U[i,\nu]\varphi_i(x)P_\nu(y)\text{.}
 \end{equation}
 Set 
 \begin{equation}
   K_m(i,j):= \int_D a_m(x)\nabla\varphi_i(x)\dot\varpi_j(x) dx
 \end{equation}
 for $i,j=0,\dots, N-1$ and 
 \begin{equation}
   B_m(\nu,\nu') := \int_{\Gamma_m} P_{\nu_m}(y_m)P_{\nu'_m}(y_m) d\mu_m(y_m)
 \end{equation}
 for $\nu,\nu'\in\mathcal{F}$ and $m=0,\dots, M$ with $y_0=1$. 
 Then, the model problem can be written as 
 \begin{equation}
   \mathbb{A}(U):= \left(\sum_{m=1}^M \mathbb{A}_m\right)(U) = \mathbb{F}
 \end{equation}
 with 
 \begin{equation}
   \mathbb{A}_m:= K_m\otimes I\otimes \dots \otimes B_m\otimes\dots \otimes I
\end{equation}  
\begin{equation}
   \mathbb{F} := f\otimes e_1\otimes\dots\otimes e_1 \textbf{ .}
\end{equation}