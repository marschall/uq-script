
\section{Kernel Ridge Regression}
Consider a compact set $D\subset\bbR^d$ and a continuous kernel 
\begin{equation}
k\colon D\times D\to\bbR\text{, }(x,y)\mapsto K(x,y)\in\bbR
\end{equation}
which is symmetric, i.e. $k(x,y) = k(y,x)$ for all $x,y\in D$.
Moreover, consider the integral operator $K\colon L^2(D)\to L^2(D)$ induced by the kernel, given by 
\begin{equation}
 \label{eq:integralOperator}
  K u(x) := \int_D k(x,y) u(y) dy
\end{equation}
By construction, $K$ is compact and symmetric and we have 
\begin{equation}
  \label{eq:Kernelsymmetric}
  \langle K u,v\rangle = \langle u,Kv\rangle 
\end{equation}
where $\langle\cdot,\cdot\rangle$ denotes the standard $L^2(D)$ inner product.
\begin{assumption}
  \label{Kernel Assumption 1}
  We assume $K$ is positive semi definite, i.e. 
  \begin{equation}
   \langle Ku,u\rangle \geq 0 \quad \text{for all } u\in L^2(D)\text{.}
  \end{equation}
\end{assumption}
\begin{corollary}
Given assumption \ref{Kernel Assumption 1} there exists an complete orthonormal system $(\tilde\phi_k)_{k\in\bbN}$ of $L^2(D)$ and a sequence $(\lambda_k)_{k\in\bbN}$ with $\lambda_k\geq 0$ for $k\in\bbN$, s.t.
\begin{equation}
  Ku(x) = \sum_{k=0}^\infty \lambda_k\tilde{\phi}(x)_k\langle\tilde{\phi}_k,u\rangle.
\end{equation}
\end{corollary}
\begin{proof}
  Follows directly from the compactness of $K$ and its spectral representation.
\end{proof}
Consider the following Regression Problem:

\noindent
Given sample points $x_i\in\bbR^d$ and sample values $y_i\in\bbR$ for $i=1,\dots, p$, 
we are interested in a function $f\in L^2(D)$, s.t. $y_i\approx f(x_i)$.

\subsection{Interpretation As Minimization Problem}
Take a CONS $(\tilde\phi_k)_{k\in\bbN}$ and try to approximate this basis.
Therefore, define a \emph{Loss-function}, which is interpreted as the \emph{empirical least square error}
\begin{equation}
  \frac{1}{2}\sum_{i=1}^p \vert y_i-f(x_i)\vert^2 \text{, where}\quad f(x_i) = \sum_{k=1}^\infty \tilde\alpha_k\tilde{\phi}_k(x)\text{.}
\end{equation}
Modification by $\phi_k = \lambda_k\tilde{\phi}_k$ and imposing a regularisation (or penalty) term 
\begin{equation}
  \frac{\lambda}{2}\sum_{k\in\bbN}\vert\alpha_k\vert^2 =: \frac{\lambda}{2}\|\alpha\|_{\ell^2(\bbN)}
\end{equation}
yields to the functional
\begin{equation}
  \label{eq:Functional}
  J(\alpha) := \frac{1}{2}\sum_{i=1}^p \vert y_i - \sum_{k\in\bbN}\alpha_k\phi_k(x_i)\vert^2 + \frac{\lambda}{2}\|\alpha\|^2_{\ell^2(\bbN)}
\end{equation}
which has to be minimized.
The problem is, in a deterministic setting, the functional without the regularisation term has in general no unique minimum.
On the other hand, the dependence on the \emph{hyperparemter} $\lambda$ yields a further variable and the correct choice of $\lambda$ is not trivial and mostly done heuristically via e.q. cross validation.
This idea is sometimes referred as \emph{Tychonev regularisation}.
Assuming a deterministric noise on the interpolation nodes 
\begin{equation}
  y_i = f(x_i) + \epsilon_i
\end{equation}
gives the chance to chose $\lambda$, such that the error, made by the regularisation, is below this $\epsilon$-noise.

The classical machine learning approach is to find a linear approximation in a transformed \emph{featured} space.
Therefore, consider $\text{span}\{\phi_k \;\colon\; k\in\bbN\}$ as featured space.
Our specific choice is $\phi_k := \sqrt{\lambda_k}\tilde{\phi_k}$ as eigenfunction of the integral operator $K$.
Furthermore, define the matrix 
\begin{equation}
  [\Phi_{k,i}]_{k\in\bbN, i=1,\dots, p} := [\phi_k(x_i)]_{k\in\bbN, i=1,\dots, p} \in \bbR^{\infty\times p}
\end{equation}
Then, using $\Phi^T\alpha = \sum_{k\in\bbN} \alpha_k\phi_k(x_i)$ we obtain the functional 
\begin{equation}
  J(\alpha) := \frac{1}{2}\left\langle y - \Phi^T\alpha, y- \Phi^T\alpha\right\rangle + \frac{\lambda}{2}\langle \alpha,\alpha\rangle
\end{equation}
which needs to be minimized.
Useful and sufficient is the Gaussian normal equation 
\begin{equation}
  0 = \left\langle\nabla J(\alpha),v\right\rangle = -\left\langle \Phi y, v \right\rangle + \frac{1}{2}\left\langle \left(\Phi\Phi^T-2\lambda I\right)\alpha,v\right\rangle \text{,}\quad \forall v\in\bbR^\infty\text{.}
\end{equation}
Therefore, by coefficient evaluation, the infinite equation system has to be solved 
\begin{equation}
  \frac{1}{2}\left(\Phi\Phi^T - 2\lambda I\right) \alpha = \Phi y\text{,}
\end{equation}
with the substituation $\alpha = \Phi\beta$ for $\beta = (\beta_i)_{i=1}^p\in\bbR^\infty$ and using inectivity of $\Phi$:
\begin{equation}
  \frac{1}{2}(\Phi\Phi^T + 2\lambda I) \Phi\beta = \Phi y 
\end{equation}
\begin{equation}
  \frac{1}{2}\Phi (\Phi^T\Phi + 2\lambda I)\beta = \Phi y
\end{equation}
\begin{equation}
  \underbrace{\frac{1}{2}(\Phi^T\Phi + 2\lambda I)}_{\in\bbR^{\infty\times p}} \beta = y
\end{equation}
\begin{equation}
  \beta = 2(\Phi^T\Phi + 2\lambda I) ^{-1}y
\end{equation}
whereas, the matrix 
\begin{equation}
 [\Phi^T\Phi]_{i,j} = \sum_{k\in\bbN}\phi_k(x_i) \phi_k(x_j) = k(x_i, x_j) =: T = (T_{i,j})\in\bbR^{p\times p}\text{.}
\end{equation}
 By that, we do not need the basis functions $(\phi_k)_{k\in\bbN}$ to describe the system. 
 For our desired function $f$ then it holds 
 \begin{equation}
   f(x) = \Phi^T(x)\alpha:=\sum_{k\in\bbN} \phi_k(x)\alpha_k = \sum_{k\in\bbN}\phi_k(x)\sum_{i=1}^p\phi_k(x_i)\beta_i = \sum_{i=1}^pk(x,x_i)\beta_i\text{,}
 \end{equation}
 which again makes no use of the basis $(\phi_k)_{k\in\bbN}$ but of the kernel $k$ instead.
 This trick, by introducing a kernel and getting rid of the basis functions, is called \emph{kernel trick}.
 \begin{remark}
   Summarizing the important facts
   \begin{enumerate}
   \item The basis functions $\phi_k$ are not needed explicit.
   \item The dimension of $D\subset\bbR^d$ enters only weakly, i.e. the curse of dimensionality is resolved.
   \item $\vert\beta\vert \leq p$, $O(p)$, $\vert K\vert= O(p^2)$, the $\vert $solution $\vert \cong O(p^3)$, $\vert f(x)\vert = O(p^2)$, i.e. the method is practical for moderate $p\cong 10^{3-5}$.
   \item The question is now, how to choose $k$?
   \end{enumerate}
 \end{remark}
\begin{example}
  Let $k(x,y) $ be the correlation kernel and $K$ the correlation operator of the Karhunen Loeve operator obtained from the covariance 
  \begin{equation}
    c(x,y) = k(x,y) - \int_D k(x,y) dy - \int_D k(x,y) dx\text{.}
  \end{equation}
\end{example}
\begin{example}
  Let $k(x,y) = \mathcal{K}(\vert x-y\vert)$ for $x,y \in \bbR^d$.
  In that case, the kernel $\mathcal{K}\colon \bbR\to\bbR$ consists of radial basis functions. 
  Examples are the \emph{Metern kernel}, the \emph{Laplace kernel}.
\end{example}
\begin{example}
  The standard and most cited kernel is the \emph{Gauss-kernel} for example in $\bbR^d$. 
  \begin{equation}
   k(x,y) = e^{-\delta\|x-y\|^2} = \prod_{k=1}^d e^{-\delta(x_k-y_k)^2}
  \end{equation}
\end{example}
\begin{example}
  Classically, one can consider the polynomial kernel 
  \begin{equation}
    k(x,y) = \langle x,y\rangle ^n
  \end{equation}
  or even better 
  \begin{equation}
  	 k(x,y) = \left(1+\langle x,y\rangle\right)^n
  \end{equation}
  for polynomials of degree $n$.
\end{example}
\begin{example}
  The wavelet kernel is given by 
  \begin{equation}
    k(x,y) = \sum_{l,k} \frac{1}{2^{lk}}\Psi_k^l(x)\Psi_k^l(y)
  \end{equation}
\end{example}
\begin{remark}
  For example consider the Gauss kernel to approximate a function $f(x) = \sum_{i=1}^p\beta_i e^{\delta\vert x_i -y_i\vert^2}$, using the $p$-(basis)-functions $x\mapsto k(x_i,x)$ for $i=1,\dots p$.
\end{remark}

\subsubsection{Reproducing Kernel Hilbert Spaces}
Fundamental theory depends on the work of \emph{Aronszayn} (1950-60).

Consider the Hilbert space $H\subset L^2(D)\cap C^0(D)$ with inner product $\langle\cdot,\cdot\rangle_H$ and induced norm $\|\cdot\|_H$.
This space is related to the operator $K$ in the sense that 
\begin{equation}
  \langle u,v\rangle_H = \langle u, K^{-1}v\rangle = \sum_{k\in\bbN}\langle u,\phi_k\rangle\langle\phi_k,v\rangle\text{.}
\end{equation}
For the Matern kernel,this space is just the Sobolev space.
For Gauss kernel, this space is a native function space.

This approach gives raise to the interpretation 
\begin{equation}
  J(f) := \frac{1}{2}\sum_{i=1}^p\vert y_i - f(x_i)\vert^2 + \frac{\lambda}{2}\langle f,f\rangle_H
\end{equation}
i.e. we minimize the empirical risk (or the loss functional) under the assumption that our function is element of a native space.
Even, this approach is equivalent to the Kernel Ridge regression.

\begin{corollary}
  It holds, for $f\in H$
  \begin{equation}
    f(x) = \langle k(x,\cdot), f(\cdot)\rangle_H = \sum_{k\in\bbN}\phi_k(x)\langle\phi_k,f\rangle \text{.}
  \end{equation}
  This property is called \emph{Reproducing property}.
\end{corollary}
\begin{remark}
  The native spaces are dense in $L^2(D)$.
\end{remark}

\subsection{Interpretation As Bayesian Inverse Problem}
\label{sec:RidgeRegressionBI}
An alternative to solve the minimization problem defined by $J$ is to define our desired interpolant function as expectation of a conditional random variable
\begin{equation}
  f(x) := \mathbb{E}[(y\;\vert\; x)]\text{.}
\end{equation}
This can be tackled by the Bayesian formulation, which yields the Radon-Nikodym derivative $p(y\; \vert\; x)$ w.r.t to some prior measure.