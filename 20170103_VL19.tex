\part{Tensor Products}
\label{part:tensor products}

\begin{example}
  As a motivating example we want to consider a stochastic field with physical and stochastic dependence
  \begin{equation}
  \label{eq:simpleTensorProduct}
  (x, y_1,\dots y_M)\mapsto u(x, y) \approx \sum_{k=1}^N f_k(x)\phi_k^1(y_1)\ldots \phi_k^M(y_M)
  \end{equation} 
  for e.g. $x\in D\subset\bbR^3, y\in\bbR^M$.
  The idea of the separation of variables goes back to the concepts of Bernoulli or Fourier.
  We call the separated expression a \emph{sum of simple (tensor) products}.
  Examples of decompositions of that fashion are 
  \begin{enumerate}[(i)]
    \item $\phi_k^i(y_i) = P_k(y_i)$ for polynomials $P_k$ of degree $k$,
    \item $\phi_k^l(y_l) = e^{iky_l}$ for trigonometric function systems,
    \item tensor product splines, tensor product wavelets,
    \item $\phi_k^i(y_i) = e^{-\sigma_k\vert y-y_i\vert^2}$ for Gaussian functions.
  \end{enumerate}
\end{example}

\section{Tensor Prouct spaces}
\label{tensor product spaces}

Define the Hilbert spaces $V_i$ for $i=1,\dots, M$ ,e.g. $V_i=L^2(\bbR)$, equipped with a complete ONB $\{\phi_k\colon\; k\in\bbN \}$.
Then, for some $\mathbf{n} = (n_1,\dots, n_M) \in\bbN_0^M$ we define the \emph{tensor representation} as a mapping
\begin{equation}
  \label{eq:tensorRepresentation}
  \mathbf x\mapsto U_{\mathbf{n}}[x] := \sum_{k_1,\dots, k_M}^{n_1,\dots, n_M} U[k_1,\dots, k_M]\phi_{k_1}^1(x_1)\dots \phi_{k_M}^M(x_M),\quad \mathbf x\in\bbR^M
\end{equation}
and it can be shown that the set
\begin{equation}
  \label{eq:denseSetOfTensors}
  \left\{U_{\mathbf{n}} \;\colon\; \left(\mathbf x\mapsto U_{\mathbf{n}}[\mathbf x] \right), \mathbf{n}=(n_1,\dots, n_M)\right\}
\end{equation}
is dense in $L^2(\bbR^M)$ w.r.t. the Lebesgue measure (Fubini, Tonelli, etc.).

\begin{remark}
  It is common in function theory to use the mapping notation \eqref{eq:tensorRepresentation} for functions. 
  In the following part we will introduce the same notion also for vector, matrices and general tensors to distinguish between the object (interpreted as a map) and a concrete function value.
  Moreover, we will (hopefully consistent) use discrete indices $j,k,l,\dots$ if we denote the finite dimensional case and continuous variables $x, y, \dots$ if we are talking about the infinite dimensional setting.
  Another slightly unusual fact is the use of $[\cdot ]$ brackets instead of $(\cdot)$ to denote a functional dependency.
  In tensor theory this method is used to express the connection to the matrix case for $M=2$.
\end{remark}

\subsection{finite dimensional case}
  Consider, for $i=1,\dots, M$, an index set $I_i:= \{1, \dots n_i\}$ and $\mathcal{I}:= I_1 \times\dots\times I_M$ as the cartesian product.
  Then, for any $k_i\in I_i$, and $M$-tuple $\mathbf{k}=(k_1,\dots, k_M)\in\mathcal{I}$ we can define the map 
  \begin{equation}
    \label{eq:tensor Map}
    \mathbf{k}\mapsto U[\mathbf{k}] = U[k_1,\dots, k_M]\in\mathbb{K}, \quad \mathbb{K} = \bbR \text{ or } \mathbb{C}.
  \end{equation}
  This map $U\colon \mathcal{I}\to \mathbb{K}$ is called a \emph{tensor of order $M$}.
  The indices $k_i$ are sometimes referred as variables and the $i=1,\dots M$ are the coordinates (modes).
  Moreover, the $n_i$ are meant to be the dimensions of the coordinates (modes).
  \begin{example}
    For $M=1$ the map $k\mapsto u[k]$ for $k=1,\dots, n$ is a \emph{vector} or $n$-tuple $\left( (u[k])_{k=1,\dots, n}\right)$ in $\bbR^n$.
    For vectors, $u_i \in\bbR^n$, $i=1,\dots, M$, we call the map
    \begin{align}
      \{1,\dots, n\}^M &\to \mathbb{K}, \\
      \mathbf{k} = (k_1,\dots, k_M) &\mapsto u_1\otimes\dots\otimes u_M[k_1,\dots k_M] := u_1[k_1]\dots u_M[k_M] 
    \end{align}
    a \emph{simple tensor product of vectors}.
  \end{example}
  \begin{example}
    For $M=2$, the map $(k_1, k_2) \mapsto u[k_1, k_2]$, with $k_i=1,\dots n_i$, $i+1,2$ is a matrix in $\bbR^{n_1\times n_2}$. 
    
  \end{example}
  \begin{example}
    In general, for $M\in\bbN$, we call $(k_1,\dots, k_M)\mapsto u[k_1,\dots k_M]$ with $k_i=1,\dots, n_i$, $i=1,\dots, M$ a \emph{multi-variate field}, \emph{hzpermatrix} or analog to computational arithmetic, an \emph{array}.
  \end{example}
  
  Consider the spaces $V_i = \bbR^{n_i}$ for some $n_i\in\bbN$, $i=1, 2,\dots, M$. 
  Then, 
  \begin{equation}
    [k_1,\dots,k_M]\mapsto u_1\otimes \dots \otimes u_M[k_1,\dots, k_M] \in \bbR^{n_1\times \dots \times n_M}
  \end{equation}
  is a simple tensor product of vectors , defining a tensor of order $M$, and we can consider the linear hull
  \begin{equation}
    \label{eq:FullTensorSpace}
    \mathcal{V} := \text{span}\{u_1\otimes \dots \otimes u_M \colon u_i \in\bbR^{n_i}\} =: V_1\otimes \dots \otimes V_M =: \bigotimes_{m=1}^M V_m
  \end{equation}
  \begin{example}
    For $M=2$, i.e. the matrix case, the spaces 
    \begin{equation}
      V_1 \otimes V_2 = \text{span}\{u_1\otimes u_2 \colon u_i\in\bbR^{n_i},\; i=1,2\}
    \end{equation}
    forms an algebraic tensor product of $V_1$ and $V_2$. 
    Sometimes, in literature it is denoted by $\otimes_a$, indicating that this product is an algebraic tensor space, in contrast to topological tensor spaces.
    For more details we refer to \cite{Hackbusch:Tensor}[sec. 4.2]
    
  \end{example}
  \begin{remark}
    The tensor product is associative, i.e.
    \begin{equation}
      \left( V_1\otimes V_2\right) \otimes V_3 = V_1\otimes \left(V_2\otimes V_3\right) = V_1\otimes V_2\otimes V_3
    \end{equation}
  \end{remark}
  \begin{proposition}
    The following properties hold:
    \begin{enumerate}
    \item $\bbR^n \otimes \bbR^m \cong \bbR^{m\cdot m} \cong \bbR^{n\times m}$
    \item $\bigotimes_{i=1}^M \bbR^{n_i} \cong \bbR^{n_1\times\ldots\times n_M}$
    \end{enumerate}
  \end{proposition}
  \begin{remark}
    Let $n_i=n$ for $i=1,\dots M$.
    Then, the dimension of the algebraic tensor space $\bigotimes_{i=1}^M \bbR^{n_i}$ is $n^M$ which results in the \emph{curse of dimensionality}.
  \end{remark}
  \begin{example}
    To illustrate the curse of dimensionality:
    \begin{enumerate}
    \item $n=10$, $M=8$. Then, $\dim(V)=10^8$. 
    \item $n=1000$, $M=4$. Then $\dim(V)=10^{12}$.
    \item Just for comparison: $10^{23}\approx$ Avogadro number, i.e. the number of atoms in a mol of carbonite-12.
    \item $n=2$, $M=500$ $>>$ approximated number of atoms in space.
    \end{enumerate}
    Due to the curse of dimensionality, the spaces $\bigotimes_{m=1}^M V_m$ are numerically not feasible.
    Possible loophole: approximation.
  \end{example}
  
  \section{The Canonical Format}
  \label{subsec:canonicalFormat}
  
  In the following, we will need complex classes which can be represented by much less data.
  This concept is sometimes referred as model order reduction.
  For example one considers linear subspaces.
  Even thought, one can argue that not everything can be represented in a linear fashion, this idea is widely used in numerics.    
  In contrast, it is possible to argue with connections to non linearity (not really linearity) or sparsity, which is subject to  compressive sensing.
  A novel approach uses multi-layer neuronal networks.
  The idea which will be treated in the following is approximation by simple tensor products, e.g. by the \emph{canoncial format} (CANDECOMP, PARAFAC)
  \begin{equation}
    \label{eq:canonicalFormat}
    U[k_1,\dots, k_M] = \sum_{\mu=1}^{r_c} u_1[\mu, k_1] \dots u_M[\mu, k_M] = \sum_{\mu=1}^{r_c}u_{1,\mu} \otimes \dots \otimes u_{M,\mu}[\mathbf{k}].
  \end{equation}
  This format represents an element of the full tensor space $\mathcal{V}$ from \eqref{eq:FullTensorSpace} as a linear combination of simple tensor product.
  The important quantity is the \emph{rank} $r_c$. 
  In the following we will introduce multiple concepts of \emph{ranks} for different tensor representations.
  In the canonical format, the rank gives rise to the dimensionality of the space $\mathcal{U}_{r_c}\subset \mathcal{V}$ with $U\in\mathcal{U}_{r_c}$.
  To motivate the perception of rank, we consider the simple (but interesting) matrix case.
  \begin{example}
    For $M=2$, i.e. the matrix case, there exists for every matrix $U\in\mathbb{K}^{n_1\times n_2}$ a decomposition
    \begin{equation}
      \label{eq:tensor_matrixsvd}
      (k_1, k_2) \mapsto U[k_1, k_2] = \sum_{\mu_1=1}^{r_U} u_1[\mu, k_1]\delta[\mu]u_2[\mu, k_2] \cong U_1^T\Sigma U_2
    \end{equation}
    where $r_U$ is the rank of $U$.
    This decomposition corresponds to the singular value decomposition. 
    A \emph{low-rank approximation} is given by the truncated singular value decomposition at rank $r_c\leq r_U$
    \begin{equation}
      \label{eq:tensor_truncatedmatrixsvd}
      (k_1, k_2) \mapsto U[k_1, k_2] \approx \sum_{\mu_1=1}^{r_c} u_1[\mu, k_1]\delta[\mu]u_2[\mu, k_2] \cong U_1^T\Sigma U_2
    \end{equation}
    The SVD is optimal in the matrix case, in the sense that \eqref{eq:tensor_truncatedmatrixsvd} yields the best rank $r_c$ approximation in the Frobenius Norm 
    \begin{equation}
      \label{eq:Frobenius}
      \norm{U}_F := \sqrt{\sum_{i=1}^{n_1}\sum_{j=1}^{n_2}U[i,j]^2}.
    \end{equation}
  \end{example}
  For the data complexity in the canonical format we obtain
  \begin{equation}
    \sum_{m=1}^M r_c n_m \leq O(r_c n M) << n^M \quad (\text{linear in } r_c, n, M).
  \end{equation}
  If $r_c$ is moderate, we break the curse of dimensionality. 
  
  Beside looking favorable, there is the question for usability, i.e. can we find algorithms to calculate numerical quantities, or even define linear algebraic operations? 
  \subsection{Linear Algebraic Operations In The Canonical Format}
  Some algebraic operations are listed below.
  \begin{enumerate}
  \item scalar multiplication $\lambda U$ for $\lambda\in\bbR$ and an tensor $U\in\bigotimes_{i=1}^M \bbR^{n_i} =: \mathcal{V}$ given in the canonical format \eqref{eq:canonicalFormat} is obvious. 
  \item addition (+): For $U, V \in\mathcal{V}$ in the canonical format we have 
  \begin{align}
    U + V[\mathbf{k}] &= \sum_{\mu=1}^{r_1}U_1[k_1]\dots U_M[k_M]\sum_{\nu=1}^{r_2} V_1[k_1]\dots V_[k_M] \\
     &= \sum_{\mu=1}^{r_1} \bigotimes_{i=1}^M U_i[\mu, k_i] + \sum_{\nu=1}^{r_2} \bigotimes_{i=1}^M V_i[\nu, k_i]
  \end{align}
  with $r_c \leq r_1 + r_2$.
  \item The Hadamard product ( $\circledcirc$ ):
  \begin{equation}
    U\circledcirc V[\mathbf{k}] = U[\mathbf{k}] V[\mathbf{k}] = \sum_{\mu=1}^{r_1} \sum_{\nu=1}^{r_2} \bigotimes_{i=1}^M U_i[\mu, k_i] V_i[\nu, k_i]
  \end{equation}
  with $r_c \leq r_1 r_2$.
  \item inner product :
  \begin{equation}
    \langle U, V\rangle = \sum_{\mu=1}^{r_1}\sum_{\nu=1}^{r_2} \prod_{i=1}^M\sum_{k_i=1}^{n_i} U_i[\mu, k_i] V[\nu, k_i]
  \end{equation}
  with complexity $O(r_1r_2 n M)$.
  \end{enumerate}
  All operations stay in the canonical format and can be executed with a complexity $\leq O(M n r_c^2)$ and $O(n^2 M^2 r_c^2)$ respectively.
  But all in all, the complexity grows only polynomial in the amount of modes $M$, the dimensions of the modes $n$ and the rank $r_c$.