%%% repetition

Let $\mathbf{k}=(k_1,\dots, k_M) \in\mathcal{I}= \bigtimes_{i=1}^M I_i$.
Then, we can write the canonical format \eqref{eq:canonicalFormat} as 
\begin{equation}
 U[\mathbf	k] = \sum_{\mu = 1}^{r_c} \prod_{i=1}^M U_i[\mu, k_i] \in \mathbb{K}
\end{equation}
Remind that we distinguish between the notion of a mapping 
\begin{equation}
  \mathbf{k}\mapsto U[\mathbf{k}]
\end{equation}
and a function value $U[\mathbf{k}]$. 
In the following we will abbreviate $\mathbf{k}\mapsto U[\mathbf{k}]$ with $U$ and if useful $U[\cdot]$.
Sometimes we write
\begin{equation}
  \label{eq:canonicalFormatFunction}
  U = \sum_{\mu=1}^{r_c} \bigotimes_{i=1}^M U_i[\mu, \cdot]
\end{equation}
if the tensor is in canonical format.
To ease notation even further, we restrict our concepts to the case $\mathbb{K} = \bbR$.

Of special interest are the \emph{components} of the representation \eqref{eq:canonicalFormatFunction}
\begin{equation}
  \mathbf{U} = (U_i)_{i=1}^M \in X_1\otimes \dots\otimes X_M = \mathcal{X}, \quad U_i\in X_i = \bbR^{r_c\times n_i},
\end{equation}
where the space $\mathcal{X}$ is called the \emph{parameter space}.
Let $n = \max_{1\leq i\leq M} n_i$, then $\text{dim}(\mathcal{X}) = r_c \sum_{i=1}^M n_i \lesssim M r_c n$
%%%

We will use a parametrization 
\begin{equation}
  \label{eq:tensor_canonicalparametrization}
 T\colon \mathcal{X} \to \mathcal{V} = \bigotimes_{i=1}^M V_i, \quad \mathbf{U}\mapsto T(\mathbf{U})= \sum_{\mu=1}^{r_c} \bigotimes_{i=1}^M U_i[\mu],
\end{equation}
which maps a given set of component tensors onto their tensor in the full tensor space, given in the canonical format.
\begin{proposition}
 The parametrization \eqref{eq:canonicalFormatFunction} fulfills the following properties 
 \begin{enumerate}[(i)]
   \item Given a set of components $\mathbf{U} = (U_i)_{i=1}^M$ with $U_i\in X_i$ and their corresponding canonical tensor \eqref{eq:tensor_canonicalparametrization} we have
   \begin{align}
     &\alpha \left( U_1\otimes \dots\otimes U_k\otimes\dots\otimes U_M\right) + \beta \left(U_1\otimes \dots  \otimes V_K \otimes\dots\otimes U_M\right) \\ 
     & \qquad\qquad = U_1\otimes\dots\otimes \left(\alpha U_k + \beta V_k\right) \otimes\dots\otimes U_M
   \end{align}
   for scalars $\alpha, \beta\in\mathbb{K}$ and another component $V_k\in\bbR^{n_k}$.
   \item   $T\colon \mathcal{X}\to\mathcal{V}$ is multi-linear, i.e. for all $i=1,\dots, M$ let $\mathbf{U}= (U_i)_{i=1}^M$ and $\mathbf{V}=(0,\dots, 0, V_k, 0,\dots, 0)^T\in\mathcal{X}$, we have 
  \begin{equation}
    T(U_1, \dots, U_k+V_k, \dots, U_M)^T = T(\mathbf{U})^T + T(U_1, \dots, V_k,\dots, U_M)^T
  \end{equation}
 \end{enumerate}
\end{proposition}

\subsection{Approximation problem}
  We consider an energy functional, or minimization functional, i.e. for given tensor $A\in \mathcal{V}$ 
  \begin{equation}
    J\colon\mathcal{V}\to \bbR, \quad J(V) = \norm{A - V}^2.
  \end{equation}
  Here, $\norm{\cdot}$ denotes the $L^2$ with respect to the Lebesgue measure.
  Our aim is to find a map $\mathbf{U}\in\mathcal{X}$, such that $T(\mathbf{U})\in\mathcal{V}$ minimizes the expression
  \begin{equation}
    J(T(\mathbf{U})) = \norm{A-T(\mathbf{U})}^2.
  \end{equation}
  Define $F = J\circ T\colon\mathcal{X}\to \bbR$. 
  Then, 
  \begin{equation}
    \mathbf{U} = \argmindown_{U\in\mathcal{X}} F(U).
  \end{equation}
  This problem yields the best \emph{rank $r_c$} approximation in the canonical format.
  Obvious questions are the existence and uniqueness of a solution and the creation of algorithms to solve the optimization problem.
  The first question of existence and uniqueness will be treated in the context of local optimization.

   \begin{example}
     \label{exampleNotClosedCP}
    Let $U=T(\mathbf{U})\in\mathcal{V}$ with corresponding component vector $\mathbf{U}=(U_1,\dots, u_M)\in\mathcal{X}$ as well as $V=T(\mathbf{V})\in\mathcal{V}$ with $\mathbf{V}=(V_1,\dots V_M)\in\mathcal{X}$ respectively. 
    Moreover, we assume orthogonality of the components in the sense 
    \begin{equation}
      \langle U_i, V_i\rangle = 0 \quad \forall i=1\dots, M.
    \end{equation}
    Then, we define $C_1 = V_1\otimes U_2\otimes\dots\otimes U_M$ and in general 
    \begin{equation}
      C_i = U_1\otimes\dots\otimes U_{i-1}\otimes V_i\otimes U_{i+1}\otimes\dots\otimes U_M, \quad i=2,\dots, M
    \end{equation}
    and it follows 
    \begin{equation}
      \langle C_i, C_j\rangle = \langle U_1, U_1\rangle \dots \langle U_i,V_i\rangle \dots \langle U_M, U_M\rangle = 0.
    \end{equation}
    Thus, $C := C_1 + \dots + C_M$ is a canonical tensor with $r_c = M$.
    Another way to find a representation is to look at a curvature parametrization 
    \begin{equation}
      (U_1 + tV_1)\otimes \dots \otimes (U_M + tV_M) = T(\mathbf{U}+ t\mathbf{V}).
    \end{equation}
    Considering the Fr\'{e}chet derivative yields
    \begin{align}
      \lim_{t\to 0} \frac{1}{t}\left( T(\mathbf{U} + t\mathbf{V}) - T(\mathbf{U]}\right) &= \lim_{t\to 0} U_1\otimes\dots\otimes U_M - \bigotimes_{i=1}^M U_i + U_1\otimes tV_1\otimes \dots \\ &= C_1 + \dots + C_M = C
    \end{align}
    But, $T(\mathbf{U} + t\mathbf{V})$ and $T(\mathbf{U})$ are rank one simple tensor products.
    Hence, the class of tensors of CP (canonical representation) rank $r_c$ is not closed.
  \end{example}
  As a consequence of example \ref{exampleNotClosedCP} we make the following remark 
  \begin{remark}
    \begin{enumerate}
    \item The gradients of $F$ can be calculated.
    \item  In general the minimization problem has no solution, since the space of CP tensors is not closed.
    \item A possible trivial redundance can be reduced by the restriction $\norm{U_i} = \norm{U_j}$ for all $i ,j = 1,\dots, M$.
    \item In the special case $M=3$ there exists a uniqueness result from Kruskal.
    \item For $M > 2$, the CP format suffers from multiple problems as e.g. the border rank problem, not forming a variety, ... \cite{Hillar:TensorNP}.
    \end{enumerate}
  \end{remark}