\section{Subspace Approximation and Tucker Format}
Let $\mathcal{V} = \bigotimes_{i=1}^M V_i$ a Hilbert tensor space with subspaces $U_i\subset V_i$ and dim$(U_i) = r_i (\leq n_i:= \text{dim}(V_i))$. 
Then, we define the \emph{Gra\ss manian} manifold 
\begin{equation}
  \label{eq:Grassman}
  \mathcal{G}_{r_i}(V_i) := \{ U_i\subset V_i\;\colon\; \text{dim}(U_i) = r_i \}.  
\end{equation}
\begin{remark}
  Possible characterizations of this manifold 
  \begin{enumerate}[(i)]
  \item define basis elements to construct the subspace and build equivalence classes
  \item look at the unique orthogonal projector onto the subspace 
  \end{enumerate}
\end{remark}
\subsection{Minimal Subspaces and Best Subspace Approximation}
We consider the case $M=2$.
Let $A\in V_1\otimes V_2 \left(\cong V_1'\otimes V_2 = \mathcal{L}(V_1, V_2)\right)$ with $\text{rank}(A) = r < \text{dim}(V_i)$, $i=1,2$.
Then, there exists (more than once) subspaces $W_i\subset V_i$ with $A\in W_1\otimes W_2$ with $r\leq\text{dim}(W_i)\leq\text{dim}(V_i)$, $i=1,2$.
Define the set $\mathcal{W}:= \{W_1\otimes W_2\;\colon\; A\in W_1\otimes W_2\}$.

\begin{proposition}
  \label{prop:minimalsubspace}
  There exists exactly one $(U_1, U_2) \in \mathcal{W}$, such that for all $(W_1, W_2)\in\mathcal{W}$ we have $U_i \subset W_i$ ($\text{dim}(U_i) \leq \text{dim}(W_i)$) and $\text{dim}(U_i) = r$, $i=1,2$ . 
\end{proposition}
\begin{proof}
  Consider $A\in V_1\otimes V_2' \cong \mathcal{L}(V_2,V_1)$. 
  Then, 
  \begin{equation}
    Av[x] := \int A[x,y] v[y] dy,\quad \forall v\in V_2
  \end{equation}
  It holds $U_1:= \text{Im}(A)$ and $U_2:= \text{Im}(A^T)$, since the singular value decomposition yields $A = U\Sigma V^T$ and the vectors $x\mapsto U[x,k], k=1,\dots, r$ are an ONS and hence $\space\{U[\cdot, k]\} = U_1$.
  The same can be done for $A^T$, which yields the space $U_2$.
  Given the singular value decomposition, without redundancy of zero singular values, we can write 
  \begin{equation}
    Av[x] = \sum_{k=1}^r U[x, k]\sigma[k]\langle v[\cdot, k], V\rangle.
  \end{equation}
  It remains to show that those spaces have minimal dimension, which follows immediately by definition of the image space.
  The dimension inequality is a consequence of the SVD and of $\Sigma$ being diagonal.
\end{proof}

Having the existence of a minimal subspace (prop. \ref{prop:minimalsubspace}), we can go on with the question of finding an appropriate approximation in this subspace.

Let $A\in V_1\otimes V_2$ arbitrary. 
We aim to find subspaces $U_i\in V_i$ with $\text{dim}(U_i)\leq r$, $i=1,2$ and an element $B\in W_1\otimes W_2$ of possibly different subspaces $\text{dim}(W_i) \leq r$ to $U_i$, which fulfills 
\begin{equation}
  \norm{A - A_r} := \inf_{W_i\subset V_i}\{\norm{A-B}\;\colon\; B\in W_1\otimes W_2,\; \text{dim}(W_i)\leq r\}.
\end{equation}
This setting is well known for matrices and is given by the singular value decomposition $H_rA := A_r = U\Sigma_r V^T$ where $\Sigma_r = H_r\Sigma$ is the hard thresholded singular value matrix.
Moreover, it yields $\text{dim}(U_i) = r$.

\section{Tucker Format}
Consider an element $U\in\mathcal{V}:=\bigotimes_{m=1}^M V_m$ and the Gra\ss mannian manifold 
\begin{equation}
  \mathcal{W} := \{ \mathbf{W} := (W_1,\dots, W_M)\;\colon\;  W_i\subset V_i,\; U\in \bigotimes_{i=1}^M W_i\}.
\end{equation}
The existence of a minimal subspace is formulated in the following theorem.
\begin{theorem}
  Given $U\in\mathcal{V}$. There exists $(U_1,\dots U_M)\in\mathcal{W}$ with $U_i\subset W_i$ for all $\mathbf{W}\in\mathcal{W}$..
\end{theorem}
\begin{definition}
  Given subspaces $(U_1, \dots U_M)$ we call $\mathbf{r} = (r_1,\dots r_M)$ with $r_i = \text{dim}(U_i)$ the \emph{Tucker rank}.
\end{definition}  

In view on our journey to find the best approximation to some $U\in\mathcal{V}$ we look for given subspaces $(U_1,\dots, U_M)$ for the minimum  
\begin{equation}
  U_{\leq\mathbf{r}} := \argmin\{\norm{U-W} \;\colon\; W\in\bigotimes_{i=1}^M W_i,\; W_i\subseteq U_i,\; \text{dim}(U_i) \leq r_i,\; \}.
\end{equation}
If it exists, then $U_{\leq\mathbf{r}}\in\bigotimes_{i=1}^M U_i$, which has to be proven later.

If we have $U\in\bigotimes_{i=1}^M U_i$ given in the canonical format, then $ x_i\mapsto \Psi_i[x_i, k_i] = \Psi_{k_i}[x_i]$, for $k_i = 1,\dots, r_i$ forms an ONB of $U_i$ and $\{(\Psi_{k_1}\otimes \dots \otimes \Psi_{k_M})\;\colon\; k_i =1,\dots, r_i, i=1,\dots, M\}$ is an ONB of $\bigotimes_{i=1}^M U_i =:\mathcal{U}$ with $\text{dim}(\mathcal{U}) = \prod_{i=1}^M r_i.$
By this construction we can write 
\begin{equation}
  \label{eq:tensor_TuckerFormat}
  U[\mathbf{x}] = \sum_{k_1=1}^{r_1}\dots \sum_{k_M=1}^{r_M} C[k_1,\dots, k_M]\bigotimes_{i=1}^M\Psi_{k_i}[x_i]
\end{equation}
We call $\mathbf{k}\mapsto C[\mathbf{k}]\in\bbR^{r_1\times\ldots\times r_M}$ the \emph{Core} tensor and \eqref{eq:tensor_TuckerFormat} the \emph{Tucker Format} of $U$. 
It holds 
\begin{equation}
  C[\mathbf{k}] = \langle U, \bigotimes_{i=1}^M\Psi_{k_i}\rangle, \quad \mathbf{k} = (K_1,\dots k_M)
\end{equation}

For the complexity we have 
\begin{equation}
  \sum_{i=1}^M n_i r_i + \prod_{i=1}^M n_i r_i \cong Mnr + r^M.
\end{equation}