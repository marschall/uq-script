\section{Higher Order SVD (HOSVD)}

Let the tensor $U\in\mathcal{V}$ be given in the Tucker format \eqref{eq:tensor_TuckerFormat}, which, for given rank vector $\mathbf{r}=(r_1,\dots, r_M)$ will be rewritten as
\begin{equation}
  U[x] = \sum_{\mathbf{k} \leq \mathbf{r}} C[\mathbf{k}]\bigotimes_{i=1}^M U_i[k_i,  x_i], \quad x_i\mapsto U_i[k_i, x_i] =: u_{k_i}
\end{equation}
with spaces $\mathcal{U}_i = \text{span} \{u_{k_i}\;\colon\; k_i=1,\dots, n\}$. 
W.l.o.g. the set of core tensors forms an ONB with respect to the inner product
\begin{equation}
  \langle u_{k_i}, u_{k_j}\rangle := \int_{I_i}u_{k_i}(x)u_{k_j}[x]d\mu_i(x)
\end{equation}
and product measure $\mu(x) = \bigotimes_{i=1}^M \mu_i$.

Given a singular value decomposition with matrix $\Sigma=\text{diag}(\sigma_1 \geq \sigma_2 > \cdots \geq \sigma_s \geq 0)$ we define the truncated matrix $\Sigma=\text{diag}_r(\sigma_1 \geq \sigma_2 \geq \cdots \geq \sigma_r) $ for $r\leq s$.

Given $v\in \mathcal{V}:= \bigotimes_{i=1}^M V_i$ we are looking for a rank vector $\mathbf{r}$ with 
\begin{equation}
  \norm{U-V} = \inf\{\norm{W-V}\;\colon\; W\in\bigotimes_{i=1}^M U_i,\; \text{dim}(U_i) \leq r_i \} 
\end{equation}
where 
\begin{equation}
  \mathcal{M}_{\leq \mathbf{r}} := \{U\in \mathcal{V}\;\colon\; U = \sum_{\mathbf{k}\leq \mathbf{r}} W[\mathbf{k}]\bigotimes_{i=1}^M u_{k_i},\; U_i=\text{span}\{u_{k_i}\colon k_i=1,\dots, r_i\}\}
\end{equation}
% redefine the operator T such that the first index is the small one 
\begin{definition}[principal matricisation]
  For given tensor $U\in\mathcal{V}$ and index $i\in\{1,\dots, M\}$ define 
  \begin{align}
    \mathcal{M}_i(U) ) &:= U[y, x'], \; y:= x_i, \quad x'=(x_1,\dots,x_{i-1},x_{i+1},\dots, x_M) \\ 
    &\cong T_U^i \in\bigotimes_{j=1, j\neq i}^M V_j\otimes V_i' \cong \mathcal{L}(V_i, \bigotimes_{j\neq i} V_j)\;\colon\; (T_U^i v)[x'] = \int_{I_i} U[y, x'] v[y] d\mu_i(y)
  \end{align}
  i.e. the map $(y, x')\mapsto U[y, x']$ is the kernel of the integral operator $T_U^i$.
\end{definition}

\begin{remark}
  \begin{enumerate}
  \item In finite dimensions, for all $i\in\bbN$ the operator $T^i\in\bbR^{\bigtimes_{i\neq j}^M n_i}$ is a matrix and the integrals corresponds to sums with row index $x_i$ and the column index $x'=(x_1,\dots, x_{i-1}, x_{i+1},\dots, x_M)$.
  \item In infinite dimensional Hilbert spaces, i.e. ($V_i \cong V_i'$) and moreover, 
  \begin{proposition}
      The integral operator $M_I(u)=T^i\colon(\bigotimes_{j\neq i} V_j)'\times V_i$ is a Hilbert Schmidt operator.
    \begin{proof}
    TODO
    \end{proof}
  \end{proposition}
  \end{enumerate}
\end{remark}

Remind, $V_i' = L^2(I, d\mu_i)$.
Since, for $U\in\mathcal{V}$, the operator $T_U^i$ is compact and hence continuous.      

Given the Schmidt-decomposition (SVD) we have 
\begin{equation}
  (T_U^if)[x'] = \sum_{k=1}^\infty \sigma^i[k]U[k,x'] \langle v[k],f\rangle ,\quad f\in V_i
\end{equation}

There exists multiple different principal flattenings of a general tensor $U$, $\mathcal{M}_1(U), \dots, \mathcal{M}_M(U)$. 
\begin{equation}
  U_i = \text{span}\{u_{k_i}\;\colon\; k_i=1,\dots, r_i\}\subset V_i ,\quad r_i:=\text{rank}(\mathcal{M}_i(U))
\end{equation}
\begin{proposition}
  Given $U\in\bigotimes_{i=1}^M U_i$ and Tucker rank $\mathbf{r}=(r_1,\dots, r_d)$ of $U$ define the sets 
  \begin{equation}
    \mathcal{M}_{\mathbf{r}}:=\{W\in \bigotimes_{i=1}^M U_i\;\colon\; \text{Tuckerrank of } W= \mathbf{r} \}
  \end{equation}
   
  \begin{equation}
    \mathcal{M}_{\leq\mathbf{r}}:=\{W\in \bigotimes_{i=1}^M U_i \;\colon\; \text{Tuckerrank of } W \leq \mathbf{r} \}
  \end{equation}
\end{proposition}
\begin{theorem}
  $\mathcal{M}_{\leq\mathbf{r}}\subset\mathcal{V}$ is weakly closed 
\end{theorem}
\begin{proof}[Falco, Hackbusch]
For $\text{dim}(V_i) < \infty$, $\mathcal{M}_{\mathbf{r}}=\{A \;\colon\, \text{rank}(A) \leq r\}$ is closed and 
\begin{equation}
  \mathcal{M}_{\mathbf{r}}=\cap_{i=1}^M \{u\in\mathcal{V}\;\colon\; \text{rank}(\mathcal{M_i}(u))\leq r_i \}.
\end{equation}
The map $u\mapsto \text{rank}(\mathcal{M}_i(u))$ is semi continuous , i.e. 
\begin{equation}
  u=\text{weak} \lim u_k \Rightarrow \text{rank}(\mathcal{M}_i(u)) \leq \liminf\text{rank}(\mathcal{M}_i(u_k))
\end{equation}

The infinite case is more complicated
\end{proof}

\begin{corollary}
  Given $V\in\mathcal{V}$ and Tucker rank $\mathbf{r}=(r_1,\dots, r_M)$, 
  there exists a best approximation 
  \begin{equation}
    \label{eq:optimizationTucker}
    U = \argmin \{\norm{W-V}\;\colon\; W\in\bigotimes_{i=1}^M U_i, \; \text{dim}(U_i)\leq r_i\}.
  \end{equation}
\end{corollary}
\begin{remark}[Hillar-Lim]
   For $\mathbf{r} = (1,\dots, 1)$, simple tensor products. 
   Finding the solution to \ref{eq:optimizationTucker} is NP hard for $d\geq 3$.
\end{remark}
\begin{remark}
  In general there exists multiple stationary points and even local minima.
  Nevertheless, finding the best approximation is often not necessary.
  We are interested in a reasonably quasi best approximation.
\end{remark}

\begin{algorithm}
  \caption{One Version of the HOSVD}
  \label{AlgoHOSVD}
  \begin{algorithmic}[1]
    \REQUIRE Tensor $U\in\bigotimes_{i=1}^M V_i$ and truncation rank $\mathbf{r}=(r_1,\dots, r_M)$.
	\STATE $U^1 = U$
	\FOR {$i=1 : M$}
      \STATE \COMMENT{truncated Schmidt decomposition}
      \STATE $\mathcal{M}_i(u^i) = U^i\sigma_{r_i}^iV^i = \sum_{k_i=1}^{r_i}u_{k_i}\times v_{k_i} \sigma^i[k_i]$.
      \STATE Store core tensor $u_{k_i}$
	  \STATE $U^{i+1} = P_i U^i$
	\ENDFOR
	\RETURN $U[\mathbf{k}] = \langle U, \bigotimes_{i=1}^M U_{k_i}\rangle$
  \end{algorithmic}
\end{algorithm}

\begin{lemma}[Projection trick]
  Let $u\in V_1\otimes V_2$ be given as SVD 
  \begin{equation}
    u=U\Sigma V^T =\sum_{k=1}^\infty \sigma_k u_k\otimes v_k.
  \end{equation}
  Define the spaces $U_1 = \text{span}\{u_k\;\colon\; k=1,\dots, r\}\subset V_1$ and $U_2 = \text{span}\{v_k\;\colon\; k=1,\dots, r\}\subset V_2$.
  Then, the truncated SVD is given by
  \begin{equation}
    U_r = U\Sigma_r V^T \cong \sum_{k=1}^r\sigma_k u_k\otimes v_k 
  \end{equation}
  which corresponds to the projection  
  \begin{equation}
    U_r = P_1\otimes P_2 U = P_1\otimes I u = I \otimes P_2 U
  \end{equation}
  where $P_1$ is the projection onto $U_1$ and $P_2$ the projection onto $U_2$ respectively.
\end{lemma}
\begin{proof}
  $P_1u=P_1 \otimes I u = (((\sum_{k=1}^r u_k \sum_{k' = 1}^\infty \langle v_{k'}, u_{k'}\rangle u_k\otimes v_k \sigma_{k'}))) = \sum_{k=1}^r u_k\otimes v_k \sigma_k$
\end{proof}
\begin{theorem}[Lathauwer]
  Let $U_\mathbf{r}:=HOSVD(U, \mathbf{r})$ and 
  \begin{equation}
    u* = \argmin\{\norm{V-W}\;\colon\; w\in\bigotimes U_i, \text{dim}(U_i) \leq r_i\}.
  \end{equation}
  For given $u\in\mathcal{V}$ 
  \begin{equation}
    \norm{u_{\mathbf{r}} - u}\leq \sqrt{d-1}\norm{u* - u} = \sqrt{d}\inf_{w\in\mathcal{M}_{\mathbf{r}}}\norm{u-w}
  \end{equation}
  For some variants of HOSVD $\sqrt{d}$ can grow to $d$.
\end{theorem}
\begin{proof}
  $U_{\mathbf{r}} ) P_d P_{d-1}u\dots P_1 = P_{d-1}\dots P_1 u$ orthogonal.
  Then, by Pythagoras
  \begin{align}
    \norm{U_{\mathbf{r}} - U}^2 &= \norm{(1-P_{d-1}\cdots P_1)u}^2 \\ 
    &= \norm{(P_{d-1}\cdots P_2)(1 - P_1)u}^2 + \dots \norm{(P_{d-1} -I)u}^2 \\ 
    &\leq \sum_{i=1}^{d-1}\norm{(1-P_i)u}^2 \leq (d-1)\norm{u-u*}
  \end{align}
\end{proof}
\begin{remark}
  $ [P_i, P_j] = P_iP_j - P_jP_i = 0$ for $i\neq j$, i.e. the projections commute.
\end{remark}
\begin{remark}
  Hard thresholding $H_{\mathbf{r}}= HOSVD(u, \mathbf{r})$.
\end{remark}
\begin{proposition}
  \begin{equation}
    \norm{u-u_{\mathbf{r}}}^2\leq \sum_{i=1}^{M} \sum_{k\geq r_i} (\sigma^i[k_i])^2.
  \end{equation}
\end{proposition}



%%%% Nachtrag zum Tucker Format nach Hackbusch philosophie

\begin{remark}[Minimal Subspaces]

Consider spaces $U_i\subset W_i \subset V_i$ for $i=1,\dots, M$.
Given a tensor $u\in \bigotimes_{i=1}^M U_i$ with $\text{dim}(U_i) = r_i$ and $u\in\mathcal{M}_{\mathbf{r}}$.

Question: Is there a $\tilde{U}_i\subset U_i$ with $\text{dim}(\tilde{U}_i) < r_i$ and $u\in\bigotimes_{j\neq i} V_j \otimes \tilde{U}_i$. 

Attention: the tensor product space structure is not commutative, hence we should write $u\in\bigotimes_{j<i} V_i \otimes \tilde{U}_i \bigotimes_{j>i}V_i$.

Answer: No there is no such tensor representation, since $\text{rank}(\mathcal{M}_i(u))=r_i$.
\end{remark}
\begin{proposition}
  Define the spaces 
  \begin{equation}
    \mathcal{W}_i := \{W_i\subset V_i\;\colon\; u\in \bigotimes_{j\neq i} V_j \otimes W_i\}.
  \end{equation}
  Then, there exists exactly one $U_i\subset V_i$, s.t. 
  \begin{enumerate}[(i)]
  \item $U_i\in\mathcal{W}_i$
  \item $U_i \subseteq W_i, W_i \in \mathcal{W}_i$
  \item $u = HOSVD(u, \mathbf{r}), \quad U_i = \text{span}\{u_{k_i}\;\colon\; k_i=1,\dots, r_i\}$
  \end{enumerate}
\end{proposition}
We define the equivalence class: the given component tensors $(u_{k_i}) \sim (v_{k_i})$ are equivalent if and only if $\{(v_{k_i})_{k_i=1}\}, \{(v_{k_i})_{k_i=1}\}\in U_i$.