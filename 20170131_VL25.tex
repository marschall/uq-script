
Given a dimension tree $\mathbb{T}$ of an index set $D\in\mathbb{T}$ which is the root at the same time.
A node $t\in\mathbb{T}$ is a father if there exists sons $s, s'\subset t$ and $s, s'\in\mathbb{T}$ with $t=s\cup s'$ disjoint. 
It holds $U^{s}\otimes U^{s'} \supset U^t = \text{span}\{\varphi_\ell^t \;\colon\; \ell =1,\dots, r_t\}$ and 
\begin{equation}
  \varphi_\ell^t = \sum_{j=1}^{r_{s}}\sum_{k=1}^{r_{s'}}U^t[j, k, \ell]\varphi_j^{s} \otimes \varphi_k^{s'}
\end{equation}
forms a basis of $U^t$.
The component tensors or transfer matrices are given by 
\begin{equation}
  (\ell, j, k) \mapsto U^t[j, k, \ell]\in\bbR^{r_s, r_{s'}, r_t} =: X^t.
\end{equation}
We call $X^t$ the parameter space and we have 
\begin{equation}
  \text{dim}(X^t) = r_tr_sr_{s'}.
\end{equation}

\begin{proposition}
The orthogonal basis  
\begin{equation}
  \langle \varphi_\ell^t, \varphi_{\ell'}^t\rangle = \delta_{\ell, \ell'}
\end{equation}
is a sufficient condition for the orthogonality of the components, since
\begin{equation}
  \delta_{\ell, \ell'} = \langle U^t[\cdots, \ell], U^t[\cdots, \ell']\rangle = \sum_{j=1}^{r_s}\sum_{k=1}^{r_{s'}} U^t[k, j,\ell]U^t[j,k,\ell'].
\end{equation}
\end{proposition}
\begin{proof}
  By recursion.
\end{proof}
\begin{example}[evaluation of a tensor in tensor train format]
  We omit the Tucker approximation for the leafs and consider directly a tensor given according to figure \ref{fig:tt_tree}.
\begin{figure}[h]
	\begin{center}
		\begin{tikzpicture}[scale=0.4]
% one core of a tensor matrix
			\node[anchor=north] at (-2,-2) {$i=$}; 
			\node[anchor=north] at (0,-2) {$1$}; 
			\node[anchor=north] at (2,-2) {$2$}; 
			\node[anchor=north] at (4,-2) {$3$}; 
			\node[anchor=north] at (8,-2) {$M$};
			\draw(0,0)--(5,5); 
			\draw[dashed](5,5)--(7,7); 
			\draw(7,7)--(8,8);
			\draw(0,0)--(0,-2);
			\draw(2,2)--(2,-2);
			\draw(4,4)--(4,-2);
			\draw(8,8)--(8,-2);
			\draw[black,fill=black] (0,0) circle (1ex);
			\draw[black,fill=black] (2,2) circle (1ex); 
			\draw[black,fill=black] (4,4) circle (1ex); 
			\draw[black,fill=black] (8,8) circle (1ex);
		\end{tikzpicture}
	\end{center}
	\label{fig:tt_tree}
	\caption{Degenerated dimension tree leading to the tensor train tree.}
\end{figure}
  Namely a dimension tree with nodes $t=\{q,\dots, i\}$ and according sons $s=\{1,\dots, i-1\} and r=\{i\}$. 
  Then, $U^t\subset U^s\otimes V_i$ and to simplify notation we write $U^t =: U^i$ with indices $x_t = (x_1,\dots x_i)=x^i =y$.
  Then, 
  \begin{align}
    \varphi_{k_i}^i (x_1,\dots, x_i) &= \sum_{k_{i-1}=1}^{r_{i-1}}\sum_{k=1}^{n_i} U^i[k_{i-1}, k, k_i]\varphi^{i-1}_{k_{i-1}}(x_1,\dots, x_{i-1}\otimes e_k(x_i) \\ 
    &= \sum_{k_{i-1=1}}^{r_{i-1}}U^i[k_{i-1},k,k_i]\varphi_{k_{i-1}}^{i-1}(x_1,\dots, x_{i-1})
  \end{align}
  Hence, for $u\in\mathcal{V}$ we have 
  \begin{equation}
    u(x_1,\dots, x_M) = \sum_{k_1=1}^{r_1}\ldots \sum_{k_{M-1}=1}^{r_{M-1}} U^q[x_1,k_y]U^2[k_1, x_2, k_2]\ldots U^M[k_{M-1}, x_M]
  \end{equation}
  And using the matrix multiplication of the core matrices 
  \begin{equation}
    \mathbf{U}^i[x_i]:=\left(U^i[k_{i-1}, x_i, k_i]\right)_{k_{i-1}=1,\dots, r_{i-1}, k_i=1,\dots, r_i}
  \end{equation}
  \begin{equation}
    u(x_1,\dots, x_M) = \mathbf{U}^1[x_1]\mathbf{U}^2[x_2]\ldots \mathbf{U}^M[x_M].
  \end{equation}
  with $\mathbf{U}^1[x_1]\in\bbR^{1\times r_1}$, $\mathbf{U}^M[x_M]\in\bbR^{r_{M-1}\times 1}$ and $\mathbf{U}^i[x_i] \in\bbR^{r_{i-1}\times r_i}$. 
  This format is called Matrix product representation (states) (MPS) which is known in Quantumphysics since 1989.
  A single element can hence be calculated by vector matrix multiplication, starting from the last index.
\end{example}
\begin{remark}
  In the tensor train setting, the dimension tree can be organized, such that every index is the root. 
\end{remark}
\begin{remark}
  The operation complexity is given by 
  \begin{equation}
    \# op = \sum_{i=1}^{M-1}r_i, r_{i+1}r_{i-1} = O(nMr^2)
  \end{equation}
  which is equivalent to the storage complexity of a tensor train.
\end{remark}
\begin{remark}
  Calculation of an inner product of two tensor trains (figure \ref{fig:tt_ip})
  \begin{equation}
    \langle U, V, \rangle = \sum_{x_1=1}^{n_1}\ldots\sum_{x_M=1}^{n_M} U[x_1,\dots, x_M]V[x_1,\dots, x_M].
  \end{equation}
  \begin{figure}[h]

\begin{center}
\begin{tikzpicture}[scale=0.4]
% tensor matrix times tensor in tensor train format
\node[anchor=east] at (-2,0) {$U\in\mathcal{V}$}; 
\node[anchor=east] at (-2,-5) {$V\in\mathcal{V}$};
\node[anchor=west] at (26,-2.5) {$\langle U, V\rangle\in\bbR$};  
\node[anchor=north] at (0,-2) {$n_1$}; 
\node[anchor=south] at (0, 0) {$U_1$}; 
\node[anchor=north] at (0, -5) {$V_1$};
\node[anchor=north] at (2,-2) {$n_2$}; 
\node[anchor=south] at (2, 0) {$U_2$};
\node[anchor=north] at (2, -5) {$V_2$};
\node[anchor=north] at (8,-2) {$n_{M-1}$}; 
\node[anchor=south] at (8, 0) {$U_{M-1}$};
\node[anchor=north] at (8, -5) {$V_{M-1}$};
\node[anchor=north] at (10,-2) (a) {$n_M$}; 
\node[anchor=south] at (10, 0) {$U_M$};
\node[anchor=north] at (10, -5) {$V_M$};
\draw(0,0)--(0,-2);
\draw(2,0)--(2,-2);
\draw(8,0)--(8,-2);
\draw(10,0)--(10,-2);
\draw(0,0)--(3,0);
\draw[dashed](3,0)--(7,0); 
\draw(7,0)--(10,0);
\draw[black,fill=black] (0,0) circle (1ex); 
\draw[black,fill=black] (2,0) circle (1ex);
\draw[black,fill=black] (8,0) circle (1ex); \draw[black,fill=black] (10,0) circle (1ex); 

\draw(0,-3)--(0,-5);
\draw(2,-3)--(2,-5);
\draw(8,-3)--(8,-5);
\draw(10,-3)--(10,-5);
\draw(0,-5)--(3,-5);
\draw[dashed](3,-5)--(7,-5); 
\draw(7,-5)--(10,-5);
\draw[black,fill=black] (0,-5) circle (1ex); 
\draw[black,fill=black] (2,-5) circle (1ex);
\draw[black,fill=black] (8,-5) circle (1ex); 
\draw[black,fill=black] (10,-5) circle (1ex); 

\node[inner sep=0, minimum size=0, right of=a] (k) {};

%\node[anchor=east] at (-2,0) {$U\in\mathcal{V}$}; 
%\node[anchor=east] at (-2,-5) {$V\in\mathcal{V}$};
%\node[anchor=west] at (12,-2.5) {$\langle U, V\rangle\in\bbR$};   
\node[anchor=east] at (14, -2.5) (b) {$M_1$};

\node[inner sep=0, minimum size=0, left of=b] (i) {};
\draw[-latex](a.east)->(b.west);


\node[anchor=north] at (16,-2) {$n_2$}; 
\node[anchor=south] at (16, 0) {$U_2$};
\node[anchor=north] at (16, -5) {$V_2$};
\node[anchor=north] at (22,-2) {$n_{M-1}$}; 
\node[anchor=south] at (22, 0) {$U_{M-1}$};
\node[anchor=north] at (22, -5) {$V_{M-1}$};
\node[anchor=north] at (24,-2) {$n_M$}; 
\node[anchor=south] at (24, 0) {$U_M$};
\node[anchor=north] at (24, -5) {$V_M$};
\draw(14,-2.5)--(16,0);
\draw(16,0)--(16,-2);
\draw(22,0)--(22,-2);
\draw(24,0)--(24,-2);
\draw(16,0)--(17,0);
\draw[dashed](17,0)--(21,0); 
\draw(21,0)--(24,0);
\draw[black,fill=black] (14,-2.5) circle (1ex); 
\draw[black,fill=black] (16,0) circle (1ex);
\draw[black,fill=black] (22,0) circle (1ex); 
\draw[black,fill=black] (24,0) circle (1ex); 

\draw(14,-2.5)--(16,-5);
\draw(16,-3)--(16,-5);
\draw(22,-3)--(22,-5);
\draw(24,-3)--(24,-5);
\draw(16,-5)--(17,-5);
\draw[dashed](17,-5)--(21,-5); 
\draw(21,-5)--(24,-5);
\draw[black,fill=black] (16,-5) circle (1ex);
\draw[black,fill=black] (22,-5) circle (1ex); 
\draw[black,fill=black] (24,-5) circle (1ex); 


\end{tikzpicture}
\end{center} 
\caption{Calculation of an inner product of two tensor given in the tensor train format by contraction of the cores at the dimension index.}
\label{fig:tt_ip}
\end{figure}
  The complexity of this operation is dominated by the contraction result and given by 
  \begin{equation}
    \# \leq O(M r^4 n).
  \end{equation}
\end{remark}
\begin{remark}
  The summation of two tensor trains $\alpha U + \beta V$ results in a rank which, in a worst case, is the sum of the individual ranks.
  
\end{remark}
\begin{remark}
  The Hadamard product $U\odot V[\mathbf{x}] = U[\mathbf{x}]V[\mathbf{x}]$ results in a rank which, in a worst case, is the product of the individual ranks.
\end{remark}
For a more detailed explanation of algebraic operations in the tensor train format we refer to \cite{fundamentalTenOperations}.
Consider a linear operator $A\colon\mathcal{V}\to \mathcal{V}$, 
\begin{equation}
 A(x,y) = A(x_1,\dots, x_M, y_1,\dots, y_M) = A^1(x_1,y_1)\ldots A^M(x_M, y_M)
\end{equation}
with components 
\begin{equation}
  A^i[k_{i-1}, x_i, y_i, k_i] 
\end{equation}

\begin{figure}[h]

\begin{center}
\begin{tikzpicture}[scale=0.4]
% one core of a tensor matrix
\node[anchor=west] at (4,0) {$A^i$}; 
\node[anchor=east] at (-2,0) {$k_{i-1}$}; 
\node[anchor=west] at (2,0) {$k_{i}$}; 
\node[anchor=north] at (0,-2) {$x_i$}; 
\node[anchor=south] at (0, 2) {$y_i$};
\draw(2,0)--(0,0)--(0,-2); 
\draw(0,0)--(-2,0); 
\draw(0,0)--(0,2);
\draw[black,fill=white] (0,0) circle (1.8ex); \draw[black,fill=black] (2,0) circle (1ex); 
\draw[black,fill=black] (0,-2) circle (1ex); \draw[black,fill=black] (-2,0) circle (1ex); \draw[black,fill=black] (0,2) circle (1ex);
\end{tikzpicture}

\begin{tikzpicture}[scale=0.4]
% tensor matrix times tensor in tensor train format
\node[anchor=east] at (-2,0) {$A\colon\mathcal{V}\to\mathcal{V}$}; 
\node[anchor=east] at (-2,-5) {$v\in\mathcal{V}$};
\node[anchor=west] at (12,-2) {$v\mapsto Av\in\mathcal{V}$};  
\node[anchor=north] at (0,-2) {$x_1$}; 
\node[anchor=south] at (0, 2) {$y_1$};
\node[anchor=north] at (2,-2) {$x_2$}; 
\node[anchor=south] at (2, 2) {$y_2$};
\node[anchor=north] at (8,-2) {$x_{M-1}$}; 
\node[anchor=south] at (8, 2) {$y_{M-1}$};
\node[anchor=north] at (10,-2) {$x_M$}; 
\node[anchor=south] at (10, 2) {$y_M$};
\draw(0,2)--(0,-2);
\draw(2,2)--(2,-2);
\draw(8,2)--(8,-2);
\draw(10,2)--(10,-2);
\draw(0,0)--(3,0);
\draw[dashed](3,0)--(7,0); 
\draw(7,0)--(10,0);
\draw[black,fill=black] (0,0) circle (1ex); 
\draw[black,fill=black] (2,0) circle (1ex);
\draw[black,fill=black] (8,0) circle (1ex); \draw[black,fill=black] (10,0) circle (1ex); 

\draw(0,-3)--(0,-5);
\draw(2,-3)--(2,-5);
\draw(8,-3)--(8,-5);
\draw(10,-3)--(10,-5);
\draw(0,-5)--(3,-5);
\draw[dashed](3,-5)--(7,-5); 
\draw(7,-5)--(10,-5);
\draw[black,fill=black] (0,-5) circle (1ex); 
\draw[black,fill=black] (2,-5) circle (1ex);
\draw[black,fill=black] (8,-5) circle (1ex); \draw[black,fill=black] (10,-5) circle (1ex); 
\end{tikzpicture}
\end{center} 
\caption{Tensor matrix core $A^i$ in tensor network notation.}
\end{figure}
\hfill


with complexity of the (tensor) matrix tensor multiplication 
\begin{equation}
  O(M n^2 r^4)
\end{equation}

\subsection{Hierarchical Reduced Basis Function}
Consider a tensor $U\in V_1\otimes V_2$ with $(x, y)\mapsto U[x,y]$, where $y\in I_y := \{1,\dots, n_y\}$.
Then, $U\in U_1\otimes U_2$, where 
\begin{equation}
  U_1 = \text{span}\{\varphi_k\;\colon\; k=1,\dots, r\}
\end{equation}
forms a reduced basis and 
\begin{equation}
  U[x, y] = \sum_{k=1}^r \varphi_k(x) c_k[y].
\end{equation}
The flattening in tensor train format at $i$, $i=1,\dots, M-1$ 
\begin{equation}
  U[x_1,\dots, x_i;x_{i+1}, \dots, x_M] = A[x, y], \quad y=(x_{i+1},\dots, x_M), x=(x_1,\dots, x_i)
\end{equation}
can be decomposed into 
\begin{equation}
  U[x_1,\dots, x_i;x_{i+1}, \dots, x_M] = \sum_{k_i=1}^{r_i} \varphi_{k_i}^i(x)c_{k_i}[y].
\end{equation}