\part{Inverse Problems}

\section{General Problem Structure}
Our aim is to gain knowledge about an unknown system input datum $u$ taking values in some polish space $(X, \|\cdot\|_X)$ from some given model observations $\delta$ in another polish space $(Y, \|\cdot\|_Y)$ often isomorphically to some finite real vector space.
Usually, the unknown represents a system parameter, e.q. a diffusion coefficient, a boundary condition or some geometric properties.
The described relation yields the equation
\begin{equation}
  \label{eq:inverseEquation}
  \delta = (\mathcal{O}\circ G) (u)\text{.}
\end{equation}
The \emph{input to solution} or \emph{forward} operator $G\colon X\to V$ maps the input parameter onto the solution of the mathematical model.
E.q. the permeability coefficient of a soil layer setting onto the hydraulic head of the ground water flow.
Whereas, the \emph{observation} operator $\mathcal{O}\colon V\to Y$ yields the measurement, e.q. at specified sensors.
Typically, those class of problems is ill-posed.
Either due the lack of a unique solution in general or the a sensitive dependence on the measurements.
Classical approaches try to find a solution of \eqref{eq:inverseEquation} by optimization of e.q. a least squares functional 
\begin{equation}
  \label{eq:leastSquaresOptim}
  \argmin_{u\in V} \frac{1}{2}\| y-(\mathcal{O}\circ G) (u)\|^2_Y\text{.}
\end{equation}
Just scratching the topic, solving the above minimization can be difficult.
The Functional does not need to have a global minimum or minimizing sequences may have no limit in $X$.
By introducing suitable and feasible regularisations, one can solve the modified system 
\begin{equation}
  \label{eq:leasSquaresRegularizedOptim}
    \argmin_{u\in E} \left(\frac{1}{2}\| y-(\mathcal{O}\circ G) (u)\|^2_Y\text{.} + \frac{1}{2}\|u-m_0\|_E^2\right)
\end{equation}
for a restricting domain $E\subset X$ and a value $m_0\in E$.
However, the choice of the control term and the space $E$ and $m_0$ respectively is somewhat arbitrary.

We will introduce a statistical approach that desires to determine the distribution of the unknown datum given the knowledge from the measurements.
In contrast to the deterministic approach, we can show well-posedness of the probabilistic formulation using Bayes Theorem and we will even present an \`a priori convergence analysis for the polynomial chaos approximation.

\subsection{Bayesian Inversion}
\label{subsec: BayesianInversion}
We aim to find a \emph{posterior} measure $\mu_\delta$ on $X$ that contains the information of $u\in X$, taking the system responses $\delta\in Y\cong \bbR^K$ in account.
Moreover, we assume there is \`a priory knowledge about $u$, modeled in a \emph{prior} measure $\mu_0$.
By introducing a prior measure, different to a uniform distribution, we add an additional regularization into the model, analogously to \eqref{eq:leasSquaresRegularizedOptim}.
The probabilistic formulation allows to assume a model error 
\begin{equation}
  \label{eq:InverseEquationNoise}
  \delta = (\mathcal{O}\circ G) (u) + \eta \text{,}\quad\eta \sim Q_0 \text{.} 
\end{equation}
This random variable with values in $Y$ contains errors due to measurements inaccuracy and model mismatch as well.
We call $\eta$ the \emph{observational noise}.

The random variable $\delta \vert u$ ($\delta$ given $u$) is a conditional random variable, whose distribution $Q_u$ is the translation of $Q_0$ by $(\mathcal{O}\circ G) (u)$.
Assuming $Q_u \langle \langle Q_0$ for $u\in X$ $\mu_0$-a.s., there exists a functional $\Phi\colon X\times Y \to \bbR$, such that the Radon-Nikodym derivative is given by
\begin{equation}
  \label{eq:RNDerivErrorMeasure}
  \frac{dQ_u}{dQ_0}(\delta) = \exp(-\Phi(u,\delta))\text{.}
\end{equation}
Assuming measureability of $\Phi$ with respect to the joint (probably non product) measure $\nu_0 := Q_0\otimes \mu_0$, then the joint random variable $(u,\delta)\in X\times Y$ is distributed according to some $\nu\langle\langle\nu_0$ with
\begin{equation}
  \label{eq:RNDerivProductMeasure}
  \frac{d\nu}{d\nu_0}(u,\delta) = \exp(-\Phi(u,\delta))\text{.}
\end{equation}
Finally, using the specific choice of measurements $\delta\in Y$ we obtain Bayes Theorem.
\begin{theorem}
  Let $\Phi\colon X\times Y\to\bbR$ be $\nu_0$ measureable and for $\delta\in Y$, $Q_0$-a.s. 
  \begin{equation}
    \label{eq:ZPositive}
  Z:=\mathbb{E}_{\mu_0}\left[\exp\left(-\Phi(u,\delta)\right)\right] = \int_X e^{-\Phi(u,\delta)}d\mu_0(u) > 0\text{.}
  \end{equation}
  Then, the distribution of $u\vert\delta \sim \mu_\delta$ exists under $\nu$ and is absolutely continuous w.r.t $\mu_0$.
  Furthermore, the Radon-Nikodym derivative is given by 
  \begin{equation}
    \label{eq:RNDBayes}
    \frac{d\mu_\delta}{d\mu_0}(u) = \exp(-\Phi(u,\delta)) \quad \nu\text{-a.s}\text{.}
  \end{equation}
\end{theorem}
\begin{remark}
  Denote the density of the noise measure by $Q_0(d\delta) = \rho(\delta)$.
  Then, 
  \begin{equation}
    \label{eq:TranslatedErrorDensity}
    Q_u(\delta) = \rho(\delta - (\mathcal{O}\circ G)(u))
  \end{equation}
  and the \emph{Bayesian potential} 
  \begin{equation}
    \label{eq:logLikelihood}
    \Phi(u,\delta) = - \log\;\rho(\delta-(\mathcal{O}\circ G)(u))
  \end{equation}
  is the \emph{negative log likelihood}.
\end{remark}
\begin{example}
  A widely accepted choice for the noise measure is a Gaussian measure $Q_0=\mathcal{N}(0,\Gamma)$, for some symmetric positive definite covariance operator $\Gamma\colon Y\times Y\to Y$.
  Then, the potential takes the form of the least squares function in \eqref{eq:leastSquaresOptim} measured in a norm induced by the noise covariance
  \begin{equation}
    \label{eq:BayesPotential}
    \Phi(u,\delta) = \frac{1}{2}\| \delta - (\mathcal{O}\circ G)(u)\|_\Gamma^2:= \frac{1}{2}\| \Gamma^{-\frac{1}{2}}(y-(\mathcal{O}\circ G)(u))\|_Y\text{.}
  \end{equation}
\end{example}
In the following the want to get rid of the assumptions in Bayes theorem and replace them with natural assumptions on our model
Therefore, we introduce the following assumptions, containing modeling statements and assumptions on the input to observation operator.
\begin{assumption}
  \label{ForwardAssumption}
  $ $ \\
  \begin{enumerate}[(i)]
  \item The space of measurements is identified with $\bbR^K$ for some $K\in\bbN$.
  \item The measurement operator is a vector of bounded linear functionals $\mathcal{O}=(o_1,\dots,o_K)$.
  \item The input to measurement operator $(\mathcal{O}\circ G)\colon X\to\bbR^K$ is bounded in the sense that for any $\epsilon>0$ exists an $M>0$, such that for all $u\in X$ 
  \begin{equation}
    \label{eq:boundedMeasurement}
      \vert (\mathcal{O}\circ G)(u)\vert_\Gamma \leq \exp\left(\epsilon \|u\|_X^2 + M\right)\text{.}
  \end{equation}
  Here, $\vert\cdot\vert_\Gamma$ denotes the norm induced by the usual euclidean inner product weighted by $\Gamma$, i.e.
  \begin{equation}
    \label{eq:EuclideanIPGamma}
    \vert u\vert_\Gamma^2 =  \langle v , v\rangle_\Gamma := \langle v,\Gamma^{-\frac{1}{2}}v\rangle\text{.} 
  \end{equation}
  \item The operator $(\mathcal{O}\circ G)$ is Lipschitz continuous, i.e. for all $r>0$ there exists a $K>0$, such that for all $u_1,u_2\in X$ it holds 
  \begin{equation}
    \label{eq:LipschitzMeasurement}
    \|(\mathcal{O}\circ G)(u_1) - (\mathcal{O}\circ G)(u_2)\|_\Gamma \leq K \|u_1-u_2\|_X\text{.}
  \end{equation}
  \end{enumerate}
\end{assumption}
\begin{proposition}
  \label{lemma Measureability Potential}
  Given assumption \ref{ForwardAssumption}, the bayesian potential is measureable with respect to $\nu_0$ and $Z>0$ $Q_0$-a.s.
\end{proposition}
\begin{proof}
  Measureability follows directly from the continuity of $(\mathcal{O}\circ G)$ and the second statement is a result of \eqref{eq:boundedMeasurement}.
\end{proof}
\subsection{Parametric Diffusion Problem}
Consider the diffusion model problem \eqref{MP} with unknown coefficient field $u\in X$ and solution $q\in V$.
Moreover, assume the UEA($r$,$R$) for $0<r\leq R<\infty$ holds for $u$.
\begin{lemma}
  \label{lemma ForwardLipschitz}
  Let $u_1,u_2\in X$ with UEA($r$,$R$), yielding solutions $q_1,q_2\in V$ of the diffusion variational problem for the same r.h.s $f\in V^*$.
  Then, the forward map $G\colon X\to V$, $u\mapsto q$ is Lipschitz 
  \begin{equation}
    \label{eq:LipschitsForward}
    \|G(u_1) - G(u_2)\|_V = \|q_1 - q_2\|_V \leq \frac{\|f\|_{V^*}}{r^2}\|u_1-u_2\|_{L^\infty(D)}\text{.}
  \end{equation}
\end{lemma}
\begin{proof}
  The result follows analogously to \eqref{reg} applied to the solution difference.
\end{proof}
Using the parametrization assumption \ref{49} for the unknown $u$ we can show that the Bayesian potential is holomorphic in some complex domain.

\begin{assumption}
  \label{UEAC+}
  Additional to UEAC($r$,$R$) for some $0<r\leq R< \infty$ we assume there exists a real number $\kappa\in (0,1)$, such that 
  \begin{equation}
    \label{eq:kappaBound}
    \left\|\|\psi_j\|_{L^\infty(D)}\right\|_{\ell^1(\bbN)}\leq \kappa r\text{.}
  \end{equation}
\end{assumption}
\begin{proposition}
  Given \ref{UEAC+} there exists a sequence $(\kappa_j)_{j\geq 1}$ such that $\|\psi_j\|_{L^\infty(D)}\leq \kappa_k r$ and for all $x\in D$ and $y\in[-1,1]^\bbN$ we have for any $m\in\bbN$
  \begin{align}
    u(x,y) &\geq r\left(1-(\kappa-\kappa_m)-\kappa_m\right) \\
           &\geq r\left(1-(\kappa-\kappa_m)\right)\left(1-\frac{\kappa_m}{1-(\kappa-\kappa_m)}\right) \\
           &\geq \tilde{r}\left(1-\tilde{\kappa}_m\right)\text{,}
  \end{align}
  where $\tilde{r}:= r(1-\kappa)$ and $\tilde{\kappa}_m:= \kappa_m(1-(\kappa-\kappa_m))^{-1}\in(0,1)$.
\end{proposition}
\begin{lemma}
  \label{lemma potentialHolomorphy}
  Under \ref{UEAC+} and given measurements $\delta\in\bbR^K$ the mappings 
  \begin{equation}
    \Phi(u(x,\cdot),\delta) \colon[-1,1]^\bbN\to\bbR\text{,}
  \end{equation}
  \begin{equation}
    \exp\left(-\Phi(u(x,\cdot),\delta)\right) \colon[-1,1]^\bbN\to\bbR
  \end{equation}
  and
  \begin{equation}
    \label{eq:QOI}
    \exp\left(-\Phi(u(x,\cdot),\delta)\right)G(u(x,\cdot)) \colon[-1,1]^\bbN\to V
  \end{equation}
  admits a unique extension to strongly holomorphic functions on the strips 
  \begin{equation}
    \label{eq:complexStrips}
    S_\rho := \bigotimes_{j\geq 1} \left\{y_j+iz_j \colon \vert y_j\vert y \frac{\rho_j}{\tilde{\kappa}_j}\text{, } z_j\in\bbR\right\}
  \end{equation}
  for any sequence $\rho=(\rho_j)_{j\geq 1}$ with $\rho_j\in(\tilde{\kappa_j}, 1)$.
\end{lemma}
\begin{proof}
  See Lemma 4.7. in Schwab, Stuart - Sparse Deterministic Approximation of Bayesian Inverse Problems.
\end{proof}
We have seen that system responses, the Bayesian potential and a special quantity of interest \eqref{eq:QOI} depend holomorphically on the parameter $z\in\mathcal{A}_r\subset\mathcal{C}^\bbN$ in the representation 
\begin{equation}
  u = u_0 + \sum_{j\geq 1} z_j \psi_j\text{.}
\end{equation}
Given that, we are able to define bounds on the parametric density expression.
\begin{theorem}
  \label{theorem parametricDensityBound}
  Under assumption \ref{UEAC+} we have for the parametric density expression 
  \begin{equation}
    \label{eq:theta}
    \Theta (z) := \exp\left(-\Phi(u,\delta)\vert_{u = u_0 + \sum_{j\geq 1} z_j \psi_j} \right)
  \end{equation}
  and a $\tilde{r} < r$
  \begin{align}
    \label{eq:thetaBound}
    \sup_{z\in\mathcal{A}_{\tilde{r}}}\vert\Theta (z)\vert &= \sup_{z\in\mathcal{A}_{\tilde{r}}} \left\vert \exp\left(-\Phi(u,\delta)\vert_{u = u_0 + \sum_{j\geq 1} z_j \psi_j} \right)\right\vert \\ &\leq \exp\left(\frac{\| f\|_{V^*}}{\tilde{r}^2}\sum_{k=1}^K\|o_k\|^2_{V^*}\right)\text{.}
  \end{align}
\end{theorem}

\subsection{Polynomial Chaos Approximation}
The parametric posterior density can be expressed in terms of a Legendre basis, analogously to the forward solution
\begin{equation}
  \label{eq:posteriorLegendre}
  \Theta(y) = \sum_{\nu\in\mathcal{F}}\theta_\nu P_\nu(y)\text{.}
\end{equation}
Truncation to some finite dimensional index set $\Lambda_N\subset\mathcal{F}$ with maximal gpc degree $N\in\bbN$ yields the approximation 
\begin{equation}
  \label{eq:posteriorLegendreApprox}
  \Theta_N(y) = \sum_{\nu\in\Lambda}\theta_\nu P_\nu(y)\text{.}
\end{equation}
\begin{lemma}
  Assume, for some $\sigma\in (0, 1]$, $(\theta_\nu)_{j\geq 1}\in\ell^\sigma(\bbN)$.
  Then,
  \begin{equation}
    \label{eq:posteriorConvergence}
    \|\Theta(y)-\Theta_N(y)\|_{L^2([-1,1]^\bbN,\mu_0)} \leq N^{-(\frac{1}{\sigma}-\frac{1}{2})}\|(\theta_\nu)\|_{\ell^\sigma(\mathcal{F})}\text{.}
  \end{equation}
\end{lemma}
\begin{proof}
  The argument follows directly from the previous section and the application of the Stechkin Lemma and Parsevall's identity.
\end{proof}
\begin{remark}
  Taylor expansion yields analogous results for the $L^1$-norm and point-wise estimates.
\end{remark}
\subsection{Sparsity of the Posterior}
TO obtain the desired rates of convergency $ > \frac{1}{2}$, we need the summability of the coefficient series of the Legendre expansion \eqref{eq:posteriorLegendre} and the Taylor expansion 
\begin{equation}
  \label{eq:posteriorTaylor}
  \Theta(y) = \sum_{\nu\in\mathcal{F}}\tau_\nu y^\nu
\end{equation}
respectively.
This sparsity will be obtained under the following assumption 
\begin{assumption}
  \label{assumption Sparsity}
  The fluctuation of the input is $\sigma$-summable, i.e. there exists a $\sigma\in (0,1]$, such that 
  \begin{equation}
    \label{eq:summability}
    \left(\|\psi_j\|_{L^\infty(D)}\right)_{j\geq 1}\in \ell^\sigma(\mathcal{\bbN})\text{.}
  \end{equation}
\end{assumption}