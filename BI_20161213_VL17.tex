\section{The Kalman Filter for Bayesian Inversion}
\label{sec:KalmanFilter}
We already mentioned the connection of Bayesian inversion and the computation of a minimizer of some functional, by introducing a regularization term that corresponds to the choice of a suitable prior measure.
In this chapter, we want to describe a numerical scheme, which interprets the problem of finding the uncertainty from given noise measurements as an orthogonal projection onto a subspace spanned by the information contained in the measurements.
This concept leads to conditional expectations and makes use of their optimality in Hilbert spaces, which gives rise to the problem of finding a suitable approximation to this projector in a least square sense. 
We will present the concept of the widely used Kalman filter and its application to Bayesian inversion \cite{Matthies:GaussMarkovKalmanFilter}. 
Recent work for polynomial chaos approximation for the Kalman Filter setting is outlines in \cite{StarkloffSprungk:PCKalmanBI}.

\subsection{Orthogonal decomposition}
As a recall we introduce the idea of orthogonal decomposition of inner product spaces to motivate the conditional expectation (definition \ref{1.25}) as an orthogonal projection in the special case of Hilbert spaces.

Consider the Hilbert spaces $(S, \norm{\cdot}_S)$ and $(V, \norm{\cdot}_V)$ and define the tensor inner product 
\begin{equation}
  \label{eq:tensorIP}
  \langle v_1,\otimes r_1, v_2\otimes r_2\rangle_\mathcal{V} := \langle v_1, v_2\rangle_V \langle r_1, r_2\rangle_S \quad \forall v_1\otimes r_1, v_2\otimes r_2\in \mathcal{V}:= V\otimes S.
\end{equation}

For the induced norm on $\mathcal{V}$ this gives rise to the concept of \emph{cross norms}, which play an important role in the tensor space theory.
Here, we are interested in subspace approximations.
Therefore, define the space $\mathcal{V}_B := V\otimes S_B$ for some closed $S_B\subset S$ and the orthogonal projector $P_B\colon\mathcal{V}\to\mathcal{V}_B$. 
Then, for any $v\in\mathcal{V}$ it holds 
\begin{equation}
  \label{eq:orthogonal decomposition}
  v = P_B(v) + \underbrace{(I_{\mathcal{V}} - P_B)(v)}_{\in\mathcal{V}_B^{\perp}} = P_B(v) + (v-P_B(v)). = P_B(v) + (v-P_B(v)).
\end{equation}
Moreover, since we are in Hilbert spaces, the projector yields a best approximation in the subspace, i.e.
\begin{equation}
  \label{eq:orthProjector optimal}
  P_B(v) = \argmin_{w\in\mathcal{V}_B}\norm{v - w}_{\mathcal{v}}^2.
\end{equation}

In what follows, the notation of correlation will be useful 
Therefore, for $v_1\in V_1 \otimes S =: \mathcal{V}_1$, and $v_2\in V_2\otimes S = \mathcal{V}_2$, we define the correlation mapping 
\begin{equation}
  \hat{C}_{v_1, v_2}\in\mathcal{L}(\mathcal{V}_2, \mathcal{V}_1),\quad \hat{C}_{v_1, v_2}\colon \mathcal{V}_2\to\mathcal{V}_1,\; w\mapsto\mathbb{E}_{\mathcal{V}_1}\left[\langle v_2, w\rangle_{\mathcal{V}_2}v_1\right].
\end{equation}
By centering the used random variables in the construction of the map, we obtain
\begin{equation}
  \label{eq:correlation}
  C_{v_1, v_2} := \hat{C}_{v_1 -\mathbb{E}{v_1}, v_2-\mathbb{E}_{v_2}}.
\end{equation}

\subsection{Connection to inverse problems}
To recall section \ref{subsec: BayesianInversion}, we consider the stochastic interpretation of the inverse problem, namely estimating $u\in\mathcal{V} = V\otimes S$ given measurements $\delta\in\bbR^K$ and the relation 
\begin{equation}
  \delta = (\mathcal O\circ G)(u) + \eta 
\end{equation}
for some $\bbR^K$ valued random variable $\eta$.
To use the previous concept of orthogonal  projections, we introduce the subspace 
\begin{equation}
  \label{eq:sigmaAlgebraSubspace}
  \mathcal{V}_{\sigma(\delta)}:= V\otimes S_{\sigma(\delta)} := V \otimes \left\{ r\in S\;\colon\; r=\psi \circ \delta, \psi\colon\bbR^K\to S \text{ measurable}\right\} \subset \mathcal{V}.
\end{equation}
Intuitively, this space represents anything we can learn from the observations as a function of those.
Then, using \eqref{eq:orthogonal decomposition} applied to our uncertainty $u$, we obtain 
\begin{equation}
  \label{eq:kalmanProjection}
  u = P_{\sigma(\delta)}(u) + (I_\mathcal{V}- P_{\sigma(\delta)})(u) = \mathbb{E}_{\mathcal{V}}\left[u \;\vert\; \sigma(\delta)\right] + \left( u- \mathbb{E}_{\mathcal{V}}\left[u\;\vert\; \sigma(\delta)\right]\right).
\end{equation}

Assume we have, prior to our measurements, a \emph{forecast} $u_f$ of $u$. 
For example a start value or a sample of the prior measure.
By considering the system response $\delta_f = (\mathcal{O}\circ G)(u_f) + \eta_f$, for some realization of the noise $\eta$, one obtains by \eqref{eq:kalmanProjection} an \emph{analysis} variable

\begin{align}
  \label{eq:KalmanFormula}
  u_a &= \mathbb{E}_{\mathcal{V}}\left[u_f \;\vert\; \sigma(\delta)\right] + \left( u_f- \mathbb{E}_{\mathcal{V}}\left[u_f\;\vert\; \sigma(\delta_f)\right]\right) \\
      &= u_f + \mathbb{E}_{\mathcal{V}}\left[u_f\;\vert\; \sigma(\delta)\right] - \mathbb{E}_{\mathcal{V}}\left[ u_f \;\vert\; \sigma(\delta_f)\right] =: u_f + u_i
\end{align}
$u_i$ is called the \emph{innovation}, modeling the gained knowledge from the forecast measurements.

\begin{proposition}
  \label{prop:kfproperty}
  The following properties hold
  \begin{enumerate}[i]
  \item The innovation $u_i$ is uncorrelated to all random variables in $\mathcal{V}_{\sigma(\delta)}$, i.e. for all $v\in\mathcal{V}_{\sigma(\delta)}$ we have 
  \begin{equation}
    \mathbb{E}_{\mathcal{V}}\left[u_i v\right] = \mathbb{E}_{\mathcal{V}}\left[u_i\right]\mathbb{E}_{\mathcal{V}}\left[v\right].
  \end{equation}
  \item Given the forecast measurements, the estimator $u_a$ is unbiased w.r.t. the forecast, i.e.
  \begin{equation}
    \mathbb{E}_{\mathcal{V}}\left[u_a\;\vert\; \sigma(\delta_f)\right] = \mathbb{E}_{\mathcal{V}}\left[u_f\;\vert\;\sigma(\delta_f)\right].
  \end{equation}
  \item There exists a measurable function $w\colon \bbR^K\to\mathcal{V}$, such that 
  \begin{equation}
    \mathbb{E}_{\mathcal{V}}\left[u_f v\;\vert\;\sigma(\delta)\right] = P_{\sigma(\delta)}(u_f) = w(\delta)
  \end{equation}
  and this map is the optimal approximation in $\mathcal{V}_{\sigma(\delta)}$
  \begin{equation}
    \label{eq:findw}
    \norm{u_f - w(\delta)}_\mathcal{V}^2 = \min_{\psi \text{ measurable}}\norm{u_f - \psi(\delta)}_{\mathcal{V}}^2 = \min_{v\in\mathcal{V}_{\sigma(\delta)}}\norm{u_f -v}_{\mathcal{V}}^2
  \end{equation}
  \end{enumerate}
\end{proposition}
\begin{proof}
  \begin{enumerate}[i]
  \item The fact that the innovation is an orthogonal error can be seen by the definition of the conditional expectation and a small calculation.
  \item The estimator property follows immediately from the tower property for conditional expectations. \todo{add this one to the cond. Expect chapter}
  \item Existence can be deduced from the Doob-Dynkin Lemma and optimality is a result of the orthogonal projection property. \todo{add Doob-Dynkin Lemma to W'theory chapter}
  \end{enumerate}
\end{proof}

By the last part of proposition \ref{prop:kfproperty} we have 
\begin{equation}
  u_a = u_f + (w(\delta_f) - w(\delta)).
\end{equation}
Concluding we mention that the process of \eqref{eq:findw} is a challenging task since we have to find the minimizier in a set of measurable functions. 
Suitable approximations and restrictions needs to be incorporated.

\subsection{The Linear Filter Approach}
  One possible and computational feasible ansatz is the introduction of linear spaces. 
  Define 
  \begin{equation}
    \mathcal{V}_1 := \left\{ z\colon \bbR^K\to \mathcal{V}\;\colon\; z = \psi(\delta) = L(\delta) +b,\; L\in\mathcal{B}(\bbR^K, \mathcal{V}), b\in\mathcal{V}\right\}.
  \end{equation}
  Finding an approximation in $\mathcal{V}_1$ leads to the reformulation of \eqref{eq:findw}
  \begin{equation}
    \label{eq:findwlinear}
    \norm{u_f - (K(\delta) + a)}_{\mathcal{V}}^2 = \min_{(L,b)}\norm{u_f - (L(\delta) + b)}_{\mathcal{V}}^2
  \end{equation}
  and therefore 
  \begin{equation}
    \label{eq:analysisLinear}
    u_a = u_f + (K(\delta_f) - K(\delta)) = u_f + K(\delta_f - \delta).
  \end{equation}
  Note that the dependence on the constant term vanishes in the analysis variable.
  The main result of this section is the following explicit representation of the linear operator $K$ in \eqref{eq:findwlinear}
  \begin{theorem}
    The solution of \eqref{eq:findwlinear} is given by  $K:=C_{u_f, \delta} C^{-1}_{\delta,\delta}$ and the constant term $a:=\mathbb{E}_{\mathcal{V}}\left[u_f\right] - K(\mathbb{E}_{\mathcal{\bbR^K}}\left[\delta\right])$.
    Moreover, the covariance operator of the analysis variable is given by 
    \begin{equation}
      C_{u_a, u_a} = C_{u_f,u_f} - K C_{u_f, \delta}^T = C_{u_f, u_f} - C_{u_f, \delta}C_{\delta,\delta}^TC_{u_f,\delta}^T.
    \end{equation}
    The operator $K$ is often referred as the \emph{Kalman gain}. 
  \end{theorem}
  