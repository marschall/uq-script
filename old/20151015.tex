%Lecture 1 -- Thu Oct 15 
\section{Stochastic Processes}
\subsection{Random Walk and Brownian Motion}
\begin{beisp}[Random Walk]
  \label{1.1}
Let $(\Omega, \mathscr F, \mathbb P)$ be a probability space.  We consider a sequence $(X_n)_{n\in \mathbb N_0}$ of random variables (r.v.) on ($\Omega, \mathscr F, \mathbb P)$ with values in $\mathbb Z$ defined by
\[\left\{\begin{array}{l}X_1 := 0, \\ X_{n+1} := X_n + \xi_{n}, \qquad n \in \mathbb N\end{array}\right.\]
where $(\xi_n)_{n\in \mathbb N}$ is in turn a sequence of i.i.d.\ r.v.\ with $\mathbb P[\xi_n = 1] = \mathbb P[\xi_n = -1] = \frac12$ for $n \in \mathbb N$. 

Then, for arbitrary $n \in \mathbb N$, we have $\mathbb E[X_n] = 0$ since also $\mathbb E[\xi_j] = 0$ for all $j \in \llbracket n \rrbracket n$.
Since also $\mathbb E[\xi_i \cdot \xi_j] = \delta _{i,j}$ for arbitrary $i,j \in \mathbb N$, 
we have 
\begin{align*}
\Cov [X_n, X_m] &:= \mathbb E[(X_n - \mathbb E[X_n]) (X_m - \mathbb E[X_m])] \\
&= \mathbb E[X_n X_m] = \mathbb E\left[\left( \sum^{n-1}_{j=1} \xi_{j} \right) \left(\sum^{m-1}_{k=1} \xi_{k}\right)\right] \\
&= \min \{n-1,m-1\}.  
\end{align*}

Now we define $X(t)$ to be the piecewise linear interpolation of $(X_n)_{n\in \mathbb N}$. This gives a set of random variables indexed by $t \in \mathbb R^+_0$. This we will call a stochastic process.
\end{beisp}

\begin{def2}[Stochastic process]
  \label{1.2}
  Given a set $\mathcal T \subset \mathbb R$, a measurable space $(H,\mathscr H)$ and a probability space $(\Omega, \mathscr F, \mathbb P)$, a family $X = \{X(t) : t \in \mathcal T\}$ is called \textbf{$H$-valued stochastic process} if for every $t \in \mathcal T$ the function $X(t)$ is $\mathscr F$-$\mathscr H$-measurable.
\end{def2}

\begin{def2}[Sample Path]
  \label{1.3}
  For a given $\omega \in \Omega$ the mapping defined by $\mathcal T \rightarrow H, t \mapsto X(t, \omega)$ is called \textbf{sample path} of $X$. 
\end{def2}

\begin{bem}
  \label{1.4}
  Diffusion scaling of $X$ as in Example \ref{1.1} leads to a Brownian motion.
\end{bem}

\begin{beisp}[Distribution of a random walk]
  \label{1.5}
  With the notation from example \ref{1.1} let $p_{n,j} = \mathbb P[X_n = j]$, $j\in \mathbb Z$, $n\in \mathbb N_0$. Then we have for arbitrary $n \in \mathbb N$ and $j \in \mathbb Z$
  \[p_{n+1,j} = \frac12 (p_{n,j-1} + p_{n,j+1})\]
  as well as $p_{0,j} = \delta _{0,j}$. 

  Note that $p_{2n, 2k+1} = 0$ and $p_{2n+1, 2k+1}$ for arbitrary $n \in \mathbb N_0$ and $k \in \mathbb Z$.
\end{beisp}

\begin{lem}
  \label{1.6}
  With the notation of examples \ref{1.1} and \ref{1.5} we have
  \begin{enumerate}[(i)]
    \item $\frac1n X_n \to 0$ $\mathbb P$-a.s.
    \item $\frac1{\sqrt{n}} X_n \xrightarrow{\text{Law}} \mathcal N(0,1)$.
  \end{enumerate}
\end{lem}
\begin{proof}
  We have $X_n = \sum^{n-1}_{j=1} \xi_{j}$ with $\mathbb E[\xi_j] = 0$ and $\mathbb V[\xi_j] = 1$ for all $j \in \mathbb N$. Since $(\xi_n)_{n\in \mathbb N}$ is i.i.d.\ Bernoulli r.v.\ (centered around zero), the two statements are exactly the law of large numbers and the central limit theorem.
\end{proof}

The random walk is approximately Gaussian for $n$ large. We derive a sequence of stochastic procceses $Y_N = \{Y_N(t) : t \in \mathbb R^n\}$ for $N \in \mathbb N$ where $Y_N(t)$ is approximately Gaussian for large $N$.

Let again $X$ be the linear interpolation of a random walk $(X_n)_{n\in \mathbb N}$. Now we define 
\[Y_N (t) := \frac1{\sqrt{N}} X(tN) = \frac1{\sqrt{N}} X_{\lfloor tN \rfloor} + \frac{tN - \lfloor tN\rfloor}{\sqrt N} \xi_{\lfloor tN \rfloor}\]
Then $Y_N = \{Y_N(t) : t \in \mathbb R^+_0\}$ is a real-valued stochastic process for each $N\in \mathbb N$ with continuous sample paths.

\begin{lem}
  \label{1.7}
  With $Y_N$ as above we have for $t \in \mathbb R^+_0$ arbitrary $Y_N(t) \xrightarrow{\text{Law}} \mathcal N(0,t)$ as $N \to \infty$.
\end{lem}
\begin{proof}
  The second term in the definition of $Y_N(t)$ converges to zero in probability, i.e.\ for every $\varepsilon > 0$
  \[\mathbb P\left[ \left| \frac{tN - \lfloor tN\rfloor}{\sqrt{N}} \xi_{\lfloor tN \rfloor} \right| > \varepsilon\right] \xrightarrow{N \to \infty} 0.\]
  The first term in the definition converges to the desired distribution by Lemma \ref{1.6}. Since convergence in probability implies convergence in distribution, we are already done.
\end{proof}

\begin{bem}
  \label{1.8}
  For every $t\in \mathbb R^+_0$ we have $Y_N(t) \in L^2(\Omega)$ for each $N \in \mathbb N$, however, the sequence $(Y_N(t))_{N\in \mathbb N}$ is not Cauchy, i.e. it has no limit in $L^2(\Omega)$.
\end{bem}

\begin{def2}[Finite-Dimensional Distributions]
  \label{1.9}
  Let $X = \{X(t) : t \in \mathcal T\}$ be a real-valued stochastic process. For $t_1,\ldots,t_M \in \mathcal T$ we define
  \[\mathbf X := (X(t_1),\ldots,X(t_M))^T.\]
  Then $\mathbf X$ is a $\mathbb R^M$-valued r.v.\ and the probability distribution $\mathbb P_{\mathbf X}$ on $(\mathbb R^M, \operatorname{Bor}(\mathbb R^M))$ is known as the \textbf{finite-dimensional distribution} of $X$ at $t_1,\ldots,t_M$.
\end{def2}

\begin{lem}
  \label{1.10}
  For the diffusion scaled random walk $Y_N$ as above and $\mathbf Y_N := (Y_N(t_1),\ldots, Y_N(t_M))^T$ a finite-dimensional distribution we have  
  $\mathbf Y_N \xrightarrow{\text{Law}} \mathcal N(0,C)$ as $N \to \infty$, where $C \in \mathbb R^{M\times M}$ with entries $c_{i,j} = \min\{t_i, t_j\}$. 
\end{lem}

\begin{def2}[Second Order Process]
  \label{1.11}
  A stochastic process $X = \{X(t) : t\in \mathcal T\}$ is said to be of \textbf{second order}, if $X(t) \in L^2(\Omega)$ for all $t \in \mathcal T$. 

  If $X$ is of second order, we define the \textbf{mean function} $\mu : t \mapsto \mathbb E[X(t)]$ and the \textbf{covariance function} $C : (s,t) \mapsto \Cov(X(s), X(t))$ of $X$. 
\end{def2}
\begin{def2}[Real-valued Gaussian Process]
  \label{1.12}
  A real-valued stochastic proccess $X$ is called \textbf{Gaussian process}, if all its Finite-dimensional distributions are multivariate Gaussian distributions.
\end{def2}

\begin{def2}[Brownsche Bewegung]
  \label{1.13}
  The processes $W = \{W(t) : t \in \mathbb R^+_0\}$ is called \textbf{Brownian motion}, if it is a real-valued Gaussian process with continuous sample paths, mean function $\mu(t) = 0$ and covariance function $C(s,t) = \min\{s,t\}$. 
\end{def2}

\begin{bem}
  \label{1.14}
  \begin{itemize}
    \item The Browniam motion $W$ can be understood as limiting process of therescaled linear interpolants $Y_N$.
    \item As with a random walk, the increments $W(t) - W(s)$ over disjoint intervalls are independent. 
      In particular, for $p \leq r \leq s \leq t$ we find $\Cov[W(r) - W(p), W(t) - W(s)] = 0$. Since $W$ is a Gaussian process, for which correlation zero  already  shows independence, we showed that increments of the Brownian motion are independent.

      We further can calculate $\mathbb V[W(t) - W(s)] = |t-s|$, which implies that $W(t) - W(s) \sim \mathcal N(0,|t-s|)$.
    \item An alternative definition of the Brownian motion is: $W = \{W(t) : t \in \mathbb R^+_0\}$ is a real valued stochastic procces with 
      \begin{enumerate}[(i)]
        \item $W(0) = 0$ a.s.
        \item $W(t) - W(s) \sim \mathcal N(0,|t-s|)$ for all $t ,s \in \mathbb R^+_0$,
        \item for $p,r,s,t \in \mathbb R$ with $p\leq r\leq s\leq t$ we have $W(r) - W(p)$ and $W(t)-W(s)$ are independent
        \item The paths of $W$ are almost surely continuous.
      \end{enumerate}
  \end{itemize}
\end{bem}

\begin{bem}[Relation of the random walk and the heat equation]
  \label{1.15}
  Rescale the random walk $(X_n)$ to $(Y_n) = \left(\frac1{\sqrt{N}} X_n\right)$ for some $N \in \mathbb N$. On the lattice $(t_n, x_n) = (n \Delta t_j , \Delta x)$, $j \in \mathbb Z$, $n \in \mathbb N_0$ with $\Delta t = \Delta x^2 = \frac1N$. 
  As above we have 
  $q_{n,j} = \mathbb P[Y_n = j]$ satisfies
  \[q_{n+1,j} := \frac12 (q_{n,j-1} + q_{n,j+1})\]
  and thus
  \[\frac{q_{n+1,j} - q_{n,j}}{\Delta t} = \frac12 \frac{q_{n,j-1} - 2 q_{n,j} + q_{n,j+1}}{\Delta x^2}.\]
  This is equivalent to the finite difference approximation of the heat equation
  \begin{equation}
    \frac{\partial u}{\partial t} = \frac12 \frac{\partial ^2 u}{\partial x^2}, \qquad x\in \overline{\mathbb R}, t \in \mathbb R^+_0
    \label{eqn:diffusionProblem}
    \tag{5}
  \end{equation}
  for initial conditions $u(0,x) = \phi(x)$, $\phi : \mathbb R \rightarrow \mathbb R$. 

  We define 
  \[U_{n,j} := \mathbb E[\phi(x_j + Y_n)] = \sum_{k\in \mathbb Z} \phi(x_j + x_k) q_{n,k}\]
  By the above definition of $q_{n,j}$, we have
  \begin{multline*}
    U_{n+1,j} = \sum_{k\in \mathbb Z} \phi(x_j + x_k) q_{n+1, k} = \sum_{k\in\mathbb Z} \phi_{x_j + x_k} \cdot \frac12 (q_{n,k-1} + q_{n,k+1}) \\
    = \frac12 \sum_{k\in \mathbb Z} (\phi(x_j + x_{k+1}) + \phi(x_j + x_{k-1}) ) q_{n,k} = \frac12 (U_{n,j+1} + U_{n,j-1}) 
  \end{multline*}

  This shows
  \begin{equation}
    \frac{U_{n+1,j} - U_{n,j}}{\Delta t} = \frac12 \frac{U_{n,j-1} - 2 U_{n,j} + U_{n,j+1}}{\Delta x^2}
    \label{eqn:discreteDiffusion}
    \tag{6}
  \end{equation}
  with starting value $U_{0,j} = \phi(x_j)$.
