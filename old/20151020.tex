%Lecture 2 -- Tue Oct 20

  The finite differences (FD) discretization of \eqref{eqn:diffusionProblem} with initial condition $u(0,x) = \phi(x)$ on the other hand is given by
  \begin{equation}
    \frac{u_{n+1,j} - u_{n,j}}{\Delta t} = \frac12 \frac{u_{n,j-1} - 2 u_{n,j} + u_{n,j+1}}{\Delta x^2}, \qquad u_{0,j} = \phi(x_j)
    \label{eqn:diffusionFDdiscretization}
    \tag{7}
  \end{equation}

  Since in \eqref{eqn:discreteDiffusion} and \eqref{eqn:diffusionFDdiscretization} update rules and initial conditions are the same, the FD approximation $u_{n,j}$ and the $U_{n,j}$ of the random walk are equivalent.
\end{bem}

\begin{satz}
  \label{1.16}
  For continuous bounded initial data $\phi$, the FD approximation $u_{n,j}$ defined by \eqref{eqn:diffusionFDdiscretization} converges to the solution of \eqref{eqn:diffusionProblem} as $\Delta t = \Delta x^2 \to 0$. 
\end{satz}
\begin{proof}
  Recall that $Y_n := Y_n(t_n)$, then $Y_n \xrightarrow{\text{Law}} \mathcal N(0,t_n)$ by Lemma \ref{1.10}. Hence $x_j + Y_n \xrightarrow{\text{Law}} \mathcal N(x_j, t_n)$ as $N\to \infty$ with $(t_n, x_j)$ fixed. 

  Equivalently, 
  \[U_{n,j} = \mathbb E[\phi(x_j + Y_n)] \to \int_\mathbb R \phi(x) \cdot \frac1{\sqrt{2\pi t_n}} e^{- \frac{(x-x_j)^2}{2t_n}} \dx{x}.\]
  It is well known, that the exact solution of \eqref{eqn:diffusionProblem} is given by
  \[u(t,x) = \frac1{\sqrt{2\pi t}} \int_\mathbb R e^{-\frac{(x-y)^2}{2t}} \phi(y) \dx{y}.\]
  Sitge $U_{n,j} = u_{n,j}$, this yields $u_{n,j} \to u(t_n, x_j)$ for $N \to \infty$ and fixed $(t_n,x_j)$. 
\end{proof}

\begin{bem}
  \label{1.17}
  \begin{itemize}
    \item The distribution of a real-valued multivariate Gaussian r.v.\ is uniquely determined by its mean vector $\mu$ and covariance matrix $C$. Here $C$ must be symmetric and non-negative definite.
    \item Similarly for a Gaussian process $X = \{X(t) : t \in \mathcal T\}$, the covariance function $C : \mathcal T \times \mathcal T \rightarrow \mathbb R$ must be symmetric and non-negative definite, i.e.\ for arbitrary $N \in \mathbb N$, $t_1,\ldots,t_N \in \mathcal T$ and $a_1,\ldots,a_N \in \mathbb R$ we always have 
      \[\sum^N_{j,k = 1} a_j C(t_j, t_k) a_k \geq 0.\]
    \item Let $\mathbb R^{\mathcal T}$ be the set of all functions $f : \mathcal T \rightarrow \mathbb R$ and $\operatorname{Bor}(\mathbb R^{\mathcal T})$ the smallest $\sigma$-field containing all open sets of $\mathbb R^{\mathcal T}$. For $t_1,\ldots,t_N \in \mathcal T$ and $F \in \operatorname{Bor}(\mathbb R^{\mathcal T})$, we define
      \[B := \{f \in \mathbb R^{\mathcal T} : (f(t_1), \ldots, f(t_N))^T \in F\}.\]
      Note that Definition \ref{1.3} implies that the sample paths $X(\cdot,\omega)$ of a real-valued stochastic process $X = \{X(t) : t\in \mathcal T\}$ along $\mathbb R^{\mathcal T}$. Moreover, the sample paths define a $\mathbb R^{\mathcal T}$-valued r.v.
  \end{itemize}
\end{bem}

\begin{lem}
  \label{1.18}
  Since $\omega \mapsto X(\cdot,\omega)$ is $(\Omega,\mathscr F)$-$(\mathbb R^\mathcal T, \operatorname{Bor}(\mathbb R^\mathcal T))$-measurable, the sample paths
\end{lem}

\begin{def2}[Independet processes, sample paths]
  \label{1.19}
  \begin{enumerate}[(i)]
    \item Two real-valued stochastic processes $X = \{X(t) : t\in \mathcal T\}$ and $Y = \{Y(t) : t \in \mathcal T\}$ are called \textbf{independent} if the associated $(\mathbb R^{\mathcal T}, \operatorname{Bor}(\mathbb R^\mathcal T))$-valued r.v.\ are independent, i.e.\
      \[\mathbf X = (X(t_1),\ldots,X(t_N))^T \quad \text{and} \quad \mathbf Y = (Y(s_1),\ldots,Y(s_N))^T\]
      are independent $\mathbb R^N$-valued r.v.\ for arbitrary $t_1,\ldots,t_N, s_1,\ldots,s_N \in \mathcal T$. 
    \item The functions $f : \mathcal T \rightarrow \mathbb R$, $i\in \{1,2\}$, are inpendent sample paths of a real-valued processes $X = \{X(t) : t \in \mathcal T\}$ if $f_i(t) = X_i(t,\omega)$ for some $\omega \in \Omega$, where $X_i$ are i.i.d.\ processes with the same distribution as $X$.
  \end{enumerate}
\end{def2}

\begin{satz}[Daniel-Kolmogorov]
  \label{1.20}
  Let $\mathcal T \subset \mathbb R$. The following statements are equivalent.
  \begin{enumerate}[(i)]
    \item There exists a real-valued second order stochastic processes with mean function $\mu$ and Covariance function $C(s,t)$.
    \item $\mu : \mathcal T \rightarrow \mathbb R$, $C : \mathcal T \times \mathcal T \rightarrow \mathbb R$ with $C$ symmetric and non-negative definite.
  \end{enumerate}
\end{satz}

\begin{kor}
  \label{1.21}
  The probability distribution $\mathbb P_X$ on $(\mathbb R^\mathcal T, \operatorname{Bor}(\mathbb R^\mathcal T))$ of a real-valued Gaussian processes is uniquely determined by mean function $\mu$ and covariance function $C$.
\end{kor}

\begin{beisp}[Cosine Covariance]
  \label{1.22}
  Let $C(s,t) = \cos(s-t)$ and $\mu \equiv 0$. Obviously $C$ is symmetric and it is easy to show that for $t_1,\ldots,t_N \in \mathcal T = \mathbb R$, $a_1,\ldots,a_n \in \mathbb R$
  \[\sum^N_{j,k=1} a_j \cos(t_j - t_k) a_k = \left| \sum_{j=1}^N a_j \cos(t_j) \right|^2 + \left| \sum^N_{j=1} a_j \sin(t_j)\right|^2 \geq 0.\]
  In fact, $X(t) = \xi_i \cos(t) + \xi_2 \sin(t)$ with $\xi_1,\xi_2 \overset{\text{i.i.d.}}{\sim} \mathcal N(0,1)$ is the Gaussian processes defined by the above $\mu$ and $C$.
\end{beisp}

For the existance of a \emph{Bm} we need the following
\begin{lem}
  \label{1.23}
  $C : \mathbb R^+_0 \times \mathbb R^+_0 \rightarrow \mathbb R, (s,t) \mapsto \min \{s,t\}$ is symmetric and non-negative definite.
\end{lem}

\subsection{Conditional Expectation for Square Integrable $H$-valued r.v.}
\begin{def2}[$L^2(\Omega, \mathscr F, \mathbb P)$]
  \label{1.24}
  For a Hilbert space $(H,\left<\cdot,\cdot\right>_H, \|\cdot\|_H)$, the space of $\mathscr F$-measurable r.v. from $(\Omega, \mathscr F, \mathbb P)$ to $(H, \operatorname{Bor}(H))$ with $\mathbb E[\|X\|^2] < \infty$ is denoted by $L^2(\Omega, \mathscr F, \mathbb P; H) =: L^2(\mathscr F; H)$ and forms a Hilbert space with the inner product
  \[\left<X,Y\right>_{L^2(\mathscr F; H)} = \mathbb E\left[\left<X,Y\right>_H\right]\]
  for $X, Y \in L^2(\mathscr F; H)$.
\end{def2}

\begin{def2}[Conditional Expectation Given a $\sigma$-field]
  \label{1.25}
  Let $X \in L^2(\mathscr F; H)$ and $\mathscr A$ a sub-$\sigma$-field of $\mathscr F$. Then, the \textbf{conditional expectation} of $X$ given $\mathscr A$, denoted by $\mathbb E[X |\mathscr A]$, is defined as orthogonal projection of $X$ onto the space $L^2(\mathscr A; H) \subset L^2(\mathscr F; H)$.
\end{def2}

Consider a Hilbert space $\Psi$ and a $\sigma$-field $\mathscr G$. The probability distribution $\mathbb P_X$ of a $\Psi$-valued r.v.\ $X$ is 
\[\mathbb P_X (G) = \mathbb P(\{\omega \in \Omega : X(\omega) \in G\}) = \mathbb E[\mathbbm 1_G(X)], \qquad \text{for }G \in \mathscr G.\]
Now we can use the definition of conditional expectations to define also conditional probability distributions. 

\begin{def2}[Conditional Probability]
  \label{1.26}
  For a $\Psi$-valued r.v.\ $X$ on $(\Omega, \mathscr F, \mathbb P)$ and $\mathscr A$ a sub-$\sigma$-field of $\mathscr F$ we define 
  \[\mathbb P_X [G | \mathscr A] := \mathbb E[ \mathbbm 1_G(X) | \mathscr A].\]
  Then $\mathbb P_X [G|\mathscr A]$ is a $[0,1]$-valued r.v.\ and $G \mapsto \mathbb P_X [G | \mathscr A]$ is a measure-valued r.v.
\end{def2}

For $H$-valued r.v.\ $Y$ and $y \in H$, define 
\[\mathbb P_X [G | Y = y] := \mathbb P[X \in G | Y = y] = \mathbb E[\mathbbm 1_G(X) | Y = y].\footnotemark\]
\footnotetext{More precisely, the definition is based on the following 
  \begin{lem*}
A r.v. $Y$ is $\sigma(X)$-measurable if and only if there exists a measurable function $f$ such that $Y = f(X)$. This function is $\mathbb P_X$-almost surely unique.
  \end{lem*}
Hence there exists a unique function $f$ such that $\mathbb E[Y | X] := \mathbb E[Y | \sigma(X)] = f(X)$ $\mathbb P$-a.s. Now we simply define symbolically $\mathbb E[Y | X = x] = f(x)$.}
The map $G \mapsto \mathbb P_X [G | Y = y]$ is a measure on $(\Psi, \mathscr G)$. Then $\mathbb P_X[G | Y = y]$ is called conditional probability that $X \in G$ given $Y = y$. 

\begin{beisp}[Brownian Bridge]
  \label{1.27}
  Let $\mathcal T = [0,T]$ for some fixed $T \in \mathbb R^+$. We want to define a new stochastic process, the Brownian bridge (\emph{Bb}), which behaves like a standard Brownian motion restricted to $\mathcal T$, except for the fact that we impose that its final point shall be equal to a beforehand fixed value $b \in \mathbb R$. 

  We realise this by conditioning $W$ to $W(0) = 0$ (as it is already defined) and $W(T) = b$. Hence the finite-dimensional distribution of a \emph{Bb} $B$ at $t_1,\ldots,t_N \in \mathcal T$ are given by
  \[\mathbb P\left[\left(\begin{array}{c}B(t_1)\\ \vdots \\ B(t_N)\end{array}\right) \in F\right] = \mathbb E\left[\mathbbm 1_F \left(\begin{array}{c}W(t_1)\\\vdots\\W(t_N)\end{array}\right) \middle| W(T) = b\right],\]
  i.e. for measurable $\phi : \mathbb R^N \rightarrow \mathbb R$ we have 
  \[\mathbb E[\phi(B(t_1),\ldots,B(t_N))] = \mathbb E[\phi(W(t_1),\ldots,W(t_n))| W(T) = b].\]

  From a more indepth study of conditional expression and the resulting disintegration theorem, one sees that a thusly defined process is again Gaussian and we can calculate the mean and covariance functions of the \emph{Bb} as
  $\mu(t) = \mathbb E[W(t) | W(T) = b]$ and $C(s,t) = \mathbb E[(W(s) - \mu(s)) (W(t) - \mu(t)|W(T) = b]$.
\end{beisp}

\begin{lem}[Standard Brownian Bridge]
  \label{1.28}
  The stochastic process $B = \{B (t) : t \in [0,1]\}$ with $B(0) = 0 = B(1)$ is a Gaussian process 
  with mean function $\mu \equiv 0$ and $C : (s,t) \mapsto \min\{s,t\} - s\cdot t$.
\end{lem}
