
%Lecture 3 -- Thu Oct 22 11:31:57 CEST 2015
\subsection{White and Coloured Noise}
\begin{bem}[White Noise]
  \label{1.29}
  Let $\mathcal T = [0,1]$ and $(\phi_n)_{n\in \mathbb N}$ be a orthonormal basis of $L^2(\mathcal T)$. For instance 
  \begin{equation}
    \phi_j(t) = \sqrt{2} \sin(j\pi t)
    \tag{$*$}
    \label{eqn:basisFkt}
  \end{equation}
  and a stochastic process $\zeta$ (the \textbf{wite noise}) by
  \begin{equation}
    \zeta(t) = \sum^\infty_{j=1} \xi_j \phi_j(t)
    \tag{$**$}
    \label{eqn:summingProcess}
  \end{equation}
  with $(\xi_j)_{j\in \mathbb N}$ a sequence of i.i.d.\ $\mathcal N(0,1)$-r.v.
  Hence $\zeta$ has mean function $\mu \equiv 0$ and the covariace
  \[C(s,t) = \Cov[\zeta(s), \zeta(t)] = \sum^\infty _{j,k =1} \Cov[\zeta_j, \zeta_k] \phi_j(s) \phi_k(t) = \sum^\infty _{j,k=1}\phi_j(s) \phi_k(t).\]
  For the basis \eqref{eqn:basisFkt}, we get hence $C(s,t) = \delta(s-t)$. Hence, for $s \neq t$, we get that the covariance is zero, i.e. the process is self-uncorrelated and for $s=t$ we get
  \[C(t,t) = \mathbb V[\zeta(t)] = \mathbb E[\zeta(t)^2] =\delta (0) = \infty.\]
  In particular, the process $\zeta$ does not converge and is hence not in $L^2(\mathcal T)$.
\end{bem}

\begin{bem}[Coloured Noise]
  \label{1.30}
  With the notation from Remark \eqref{1.29} define a processes $X$ on $\mathcal T$ via
  \[X(t) = \sum^\infty _{j=1} \sqrt{\nu_j} \xi_j \phi_i(t).\]
  When the $\nu_j$ vary with $j \in \mathbb N$, then $X(t)$ is called \textbf{coulered noise} and r.v.\ $X(t)$ and $X(s)$ are correlated.
\end{bem}

\subsection{Karhunen-Lo\`eve expansion}

\begin{bem}[Matrix Spectral Decomposition, Discrete KLE]
  \label{1.31}
  Let $X = \{X(t) : t \in \mathcal T\}$ a real-valued Gaussian processes with mean function $\mu$ and covariance function $C$. For $t_1,\ldots,t_N \in \mathcal T$ define 
  \[\mathbf X = (X(t_1),\ldots,X(t_N))^T \sim \mathcal N(\bm \mu, C_N)\]
  with $\bm \mu = (\mu(t_1),\ldots,\mu(t_N))^T$ and $C_N \in \mathbb R^{N\times N}$ with entries $c_{i,j} = C(t_i,t_j)$. 
  Samples of $X$ can be generated with $C_N = V^TV$ (Cholesky decomposition of $C_N$) and 
  \begin{equation}
    X = \bm \mu + V^T \bm \xi
    \tag{8}
    \label{eqn:8}
  \end{equation}
  with $\bm \xi = (\xi_1,\ldots,\xi_N)^T \sim \mathcal N(0,I_N)$ which can be generated as $N$ i.i.d.\ normal r.v.\ (e.g.\ via Box-Miller).
\end{bem}

\begin{satz}[Spectral Decomposition]
  \label{1.32}
  Every symmetric $A \in \mathbb R^{N\times N}$ can be decomposed as $A = U \Sigma U^T$ where $U$ is orthonormal with columns $u_j$, which are eigenvectors of $A$ and $\Sigma = \operatorname{diag}(\nu_1,\ldots,\nu_N)$ the associated eigenvalues of $A$.
\end{satz}

\begin{bem}
  \label{1.33}
  \begin{itemize}
    \item The advantage of the spectral decomposition is, that one can use the singular value decomposition, which is more robust than the Cholesky decomposition.
    \item Since the covariance matrix $C_N$ in Remark \ref{1.31} is square, real-valued, symmetric we have $C_N = U \Sigma U^T$ and since $C_N$ is non-negative definite, we can order the eigenvalues of $C_N$ as $\nu_1 \geq \nu_2 \geq \ldots \geq \nu_N \geq 0$. Let now
      \[X = \bm \mu + \sum^N_{j=1} \sqrt{\nu_j} u_j \xi_j,\]
      with $\xi_j \overset{\text{i.i.d.}}{\sim} \mathcal N(0,1)$. 
    \item Efficiency gains by truncation of spectral decomposition: In the decomposition $C_N = \sum^N_{j=1} \nu_j u_j u_j^T$ the first terms (those with biggest eigenvalues) contribute most. For $n < N$ define $\Sigma_n := \operatorname{diag}(\nu_1,\ldots,\nu_n)$ and $U_n = (u_1,\ldots,u_n) \in \mathbb R^{n \times N}$. Then we define the truncated spectral decomposition
      \[C_{N,n} = U_n \Sigma_n U_n^T = \sum_{j=1}^n \nu_j u_j u_j^T.\]
      The approiximation in operator norm is
      \[\| C_N - C_{N,n} \|_{L(\mathbb R^N)} = \sup_{x \neq 0} \frac{\|(C_N - C_{N,n})x \|_2}{\|x\|_2} = \left\| \sum^N_{j=n+1} \nu_j u_j u_j^T\right\|_{L(\mathbb R^N)} = \nu_{n+1}.\]
    \item The truncation of \eqref{eqn:8} gives 
      \[\hat X = \mu + U_n \Sigma_n^\frac12 \bm \xi = \mu + \sum^n_{j=1} \sqrt{\nu_j} u_j \xi_j\]
      with $\bm \xi = (\xi_1,\ldots,\xi_n) \sim \mathcal N(0,I_n)$, i.e. $\hat X \sim \mathcal N(\bm \mu, C_{N,n})$. If $X$ is defined by \eqref{eqn:8}, then we have
      \[\mathbb E\left[\left\|X - \hat X\right\|^2_2\right] = \mathbb E\left[\sum^N_{j=n+1} \sum^N_{k=n+1} \sqrt{\nu_j \nu _k} u_j^T u_k \xi_j \xi_k\right] = \sum^N_{j=n+1} \nu_j.\]
      and the error in approximation averages with respect to test functions $\phi \in L^2(\mathbb R^N)$ satisfies
      \[\left|\mathbb E[\phi(X)] - \mathbb E\left[\phi\left(\hat X\right)\right]\right| \leq k \|\phi\|_{L^2} \|C_N - C_{N,n}\|_F^2 \leq k \|\phi\|_{L^2} \sum^N_{j=n+1} \nu_j^2.\]
    \item Hence the decay of the eigenvalues of $C_N$ determines the approximability by few terms.
    \item Trunctation is a method for model order reduction: Approximating a $N$-dimensional Gaussian r.v.\ $X$ is approximated by an $n$ ($< N$) uncorrelated $\mathcal N(0,1)$-r.v.
  \end{itemize}
\end{bem}

\begin{bem}[Karhunen-Loève Expansion]
  \label{1.34}
  A generalization of \ref{1.31} to \ref{1.33} to stachastic processes $X = \{X(t) : t \in \mathcal T\}$ is called KLE.  Let $\mu(t) = \mathbb E[X(t)]$ and consider $Y(t) = X(t) - \mu(t)$ (the centering of $X$). 

  Our aim is to sample paths in a orthonormal basis $(\phi_j)_{j\in \mathbb N}$ of $L^2(\mathcal T)$, i.e.
  \[X(t,\omega) - \mu(t) = \sum^\infty _{j=1} \gamma_j(\omega) \phi_j(t)\]
  with r.v.\ $\gamma_j(\omega) := \left<X(t,\omega) - \mu(t), \phi_j(t)\right>_{L^2(\mathcal T)}$. In the KLE we chose $(\phi_j)_{j\in \mathbb N}$ as the eigenfunctions of the integral operator $\mathcal C$, which is defined by 
  \[(\mathcal Cf)(t) := \int_\mathcal T C(s,t) f(s) \dx{t}, \qquad \text{for } f \in L^2(\mathcal T).\]
\end{bem}

\begin{lem}
  \label{1.35}
  If $X \in L^2(\Omega; L^2(\mathcal T))$, then $\mu \in L^2(\mathcal T)$ and the sample paths $X(t,\omega) \in L^2(\mathcal T)$ a.s.\ for $\omega \in \Omega$.
\end{lem}

\begin{proof}
  By assumption $\|X\|_{L^2(\Omega; L^2(\mathcal T))} = \mathbb E[\|X\|^2_{L^2(\mathcal T)}] < \infty$ and $\|X(t,\omega)\|_{L^2(\mathcal T)} < \infty$ a.s.\ $w \in \Omega$ and sample paths $X(\cdot,\omega) \in L^2(\mathcal T)$ a.s.
  Jensen's inequality gives $\mu(t) ^2 = \mathbb E[X(t)]^2 \leq \mathbb E[X(t)^2]$ and hence 
  \[\|\mu\|^2_{L^2(\mathcal T)} = \int_\mathcal T \mu(t) \dx{t} \leq \int_\mathcal T \mathbb E[X(t)^2] \dx{t} = \|X\|^2_{L^2(\Omega; L^2(\mathcal T))} < \infty.\] 
\end{proof}

\begin{satz}[$L^2$-Convergence of the KLE]
  \label{1.36}
  Consider $X = \{X(t) : t \in \mathcal T\}$ and suppose $X \in L^2(\Omega; L^2(\mathcal T))$. Then
  \begin{equation}
    X(t,\omega) = \mu(t) + \sum^\infty_{j=1} \sqrt{\nu_j} \phi_j(t) \xi_j(\omega)
    \tag{10}
    \label{eqn:10}
  \end{equation}
  converges in $L^2(\Omega;L^2(\mathcal T))$, with 
  \[\xi_j (\omega) = \frac{1}{\sqrt{\nu_j}} \left<X(t,\omega) - \mu(t), \phi_j(t)\right>_{L^2(\mathcal T)},\]
  and $\{\nu_j, \phi_j\}_{j\in \mathbb N}$ denotes eigenpairs of the integraloperator $\mathcal C$ with kernel function $C$, the covariance function of $X$, with $\nu_i \geq \nu_{n+1} \geq 0$ for all $n \in \mathbb N$. The $\xi_n$ have mean zero, variance one and are pairwise uncorrelated. If $X$ is a Gaussian process, we have $\xi_j \sim \mathcal N(0,1)$.
\end{satz}

\begin{satz}[Uniform Convergence of the KLE]
  \label{1.37}
  Consider a real valued stochastic process $X \in L^2(\Omega; L^2(\mathcal T))$ and let $X_J (t,\omega)$ be defined as
  \[X_J (t,\omega) := \mu(t) + \sum^J_{j =1} \sqrt{\nu_j} \phi_j(t) \xi_j(\omega).\]
  If $\mathcal T\subset \mathbb R$ is a compact set and the covariance function $C$ of $X$ belons to $C(\mathcal T \times \mathcal T)$, then $\phi_j \in C(\mathcal T)$ and the expansion of $C$ converges uniformly, i.e.
  \[\sup_{s,t \in \mathcal T} |C(s,t) - C_J(s,t)| \leq \sup_{t\in \mathcal T} \sum^\infty _{j=J+1} \nu_j \phi_j(t) ^2 \to 0, \qquad \text{for } J \to \infty,\]
  where $C_J= \sum^J_{j=1} \nu_j \phi_j(t) \phi_j(s)$ is the covariance function of $X_J$ and 
  \[\mathbb E[|X(t) - X_J(t)|^2] \to 0, \qquad \text{for } J\to \infty.\]
\end{satz}
