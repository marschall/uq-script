
\subsection{Regularity of Stochastic Processes}

\begin{def2}[Mean-Square Continuity]
  \label{1.38}
  A stochastic process $X = \{X(t) : t \in \mathcal T\}$ is \textbf{mean-square continuous}, if, for all $t\in \mathcal T$, 
  \[\mathbb E\left[(X(t+h) - X(t))^2\right] \to 0 \qquad \text{as } h \to 0.\]
\end{def2}

\begin{satz}
  \label{1.39}
  Let $X = \{X(t) : t \in \mathcal T\}$ be a centered processes. The covariance function $C$ of $X$ is continuous at $(t,t) \in \mathcal T \times \mathcal T$ if and only if $X$ is mean-square continuous in $t$.
  In particular, if $C \in C(\mathcal T\times \mathcal T)$, then $X$ is mean-square continuous everywhere.
\end{satz}

\begin{def2}[Mean-Square Derivative]
  \label{1.40}
  A stochastic process $X = \{X(t) : t \in \mathcal T\}$ is \textbf{mean-square differentiable} with mean-square derivative $\frac{\dx{X(t)}}{\dx{t}}$, for all $t \in \mathcal T$, we have 
  \[ \mathbb E\left[\left| \frac{X(t+h) - X(t)}{h} - \frac{\dx{X(t)}}{\dx{t}} \right|^2 \right]^\frac12 \to 0, \qquad \text{as } h \to 0.\]
\end{def2}

\begin{satz}
  \label{1.41}
  Given a centered stochastic-process $X$ with covariance function $C \in C^2(\mathcal T,\mathcal T)$. Then $X$ is mean-square differentiable in any $t \in \mathcal T$and the derivative $\frac{\dx{X(t)}}{\dx{t}}$ has the covariance function $\frac{\partial ^2 C(s,t)}{\partial s \partial t}$.
\end{satz}

\begin{bem}
  \label{1.42}
  Analogously, if it exist, the $r$-th mean-square derivative has covariance function $\frac{\partial ^{2r} C(s,t)}{\partial s^r \partial t^r}$.
\end{bem}

\begin{def2}[Stationary Process]
  \label{1.43}
  A stochastic processes $X$ is called \textbf{(weakly) stationary}, if it has constant mean function $\mu(t) \equiv \mu$ and its covariance function $C(s,t)$ depends only on the difference $s -t$, i.e. $C(s,t) = C(s-t,0) = c(s-t)$ for some so called stationary covariance function $c(t)$.
\end{def2}

\begin{bem}
  \label{1.44}
  \begin{itemize}
    \item The distribution of a stationary Gaussian process is invariant to translation in $t$.
    \item Examples are centered Gaussian processes with $C(s,t) = \delta (s-t)$, $\cos(s-t)$ or the exponential covariance function $e^{-|s-t|}$.
  \end{itemize}
\end{bem}

\section{Random Fields}

\begin{def2}
  \label{2.1}
  For $D \subset \mathbb R^d$, a real-valued random field $U = \{U(x) : x \in D\}$ is a family of real-valued r.v.\ on $(\Omega, \mathscr F,\mathbb P)$.
\end{def2}

\begin{beisp}[$L^2(D)$-vaued r.v.]
  \label{2.2}
  For $D \subset \mathbb R^d$, consider a $L^2(D)$-valued r.v.\ $u$ with mean $\mu \in L^2(D)$ and covariance operator $\mathcal C : L^2(D) \rightarrow L^2(D)$ of the form
  \[\left<\mathcal C \phi, \psi\right> = \Cov[\left<w,\phi\right>, \left<v, \psi\right>],\]
  for $\phi, \psi \in L^2(D)$ and general $L^2(D)$-valued r.v.\ $w$ and $v$.

  Then $u(x)$ is a real-valued r.v.\ for each $x\in D$ for which $\mathbb E[u(x)]$ and $\Cov[u(x),u(y)]$ are well defined for $\dx{x}$-a.e.\ $x, y \in D$. 

  For $\phi, \psi \in L^2(D)$ it holds
  \begin{align*}
    \left<\mathcal C\phi, \psi\right> &= \Cov[\left<\phi,u\right>, \left<\psi, u\right>] = \mathbb E\left[\left(\int_D \phi(x) (u(x) - \mu(x)) \dx{x}\right)\cdot \left(\int_d \psi(x) (u(y) - \mu(y) )\dx{y}\right)\right]\\
  &= \int_D\int_D \Cov[u(x),u(y)] \phi(x) \dx{x} \psi(y) \dx{y}.
  \end{align*}
  Thus 
  \[(\mathcal C\psi) (x) = \int_D \Cov[u(x),u(y)] \phi(y) \dx{y},\]
  which shows that the kernel of $\mathcal C$ is $\Cov[u(x),u(y)]$. Hence any $L^2(D)$-valued r.v.\ $u$ defines a second order random field with mean $u(x)$ and covariace function $C(x,y)$ equal to the kernel of $\mathcal C$.
\end{beisp}

\begin{satz}[Wiener-Khintchine]
  \label{2.3}
  The following statements are equivalent:
  \begin{enumerate}[(i)]
    \item There exists a stationary random field $u = \{u(x) : x \in \mathbb R^d\}$ wih stationary covariance function $c$ and $u$ is mean-square continuous.
    \item The function $c : \mathbb R^d \rightarrow \mathbb R$ can be written as
      \[c(x) = \int_{\mathbb R^d} e^{i \nu^T x} \dx{F(\nu)}\]
      for some finite measure $F$ on $(\mathbb R^d, \operatorname{Bor}(\mathbb R^d))$.
  \end{enumerate}
  The measure $F$ is called \textbf{spectral distribution}. If it exists, the density function $f$ of $F$ is called \textbf{spectral density} and 
  \[c(x) = (2\pi)^\frac d2 \mathcal F^{-1}[f](x) = \int_{\mathbb R^d} f(x) e^{i \lambda^T x} \dx{\lambda}.\]

  Alternatively, given $c : \mathbb R^d \rightarrow \mathbb R$, the spectral density $f$ is 
  \[f(\nu) = (2\pi)^{-d} \int_{\mathbb R^d} e^{-i\nu^T x} c(x) \dx{x} = (2\pi)^{-\frac d2} \mathcal F[c](\nu).\]
\end{satz}

\begin{beisp}[Gaussian Covariance]
  \label{2.4}
  For symmetric positive definite $A \in \mathbb R^{d\times d}$ the stationary covariance function
  \[c(x) = e^{-x^T A x}, \quad x \in \mathbb R^d\]
  has the Fourier transform
  \[f(\nu) = (2\pi)^{-d} \int_{\mathbb R^d} e^{-ix^T \nu} e^{-x^T Ax} \dx{x} = \frac{e^{-\frac14 \nu^T A^{-1} \nu}}{(2\pi)^\frac d2 2^\frac d2 \sqrt{\det A}}\]
  Hence $f$ is the density of the measure $F$ of the Gaussian distribution $\mathcal N(0,2A)$ and thus Theorem \ref{2.3} implies that $c(x)$ is a valid stationary covarinace function.
\end{beisp}

\begin{def2}[Isotropic Random Field]
  \label{2.5}
  A stationary random field is \textbf{isotropic} if the covariance $c(x)$ is invariant to rotations. Then $\mu$ is constant and $c(x) = c^0(r)$ with 
  $r := \|x\|$, with the \textbf{isotropic covariance function} $c^0 : \mathbb R^+ \rightarrow \mathbb R$.
\end{def2}

\begin{beisp}
  \label{2.6}
  With $A=I_d$ and $c(x) = e^{-x^T A x}$. Then $c^0(t) = e^{-r^2}$. $c$ is isotropic for any $A = \sigma I_d$ for any $\sigma > 0$.
\end{beisp}

\begin{bem}[KL of Random Fields]
  \label{2.7}
  Analog to stochastic processes, we consider the $L^2(\Omega; L^2(D))$-convergent KL expansion
  \[u(x,\omega) = \mu(x) + \sum^\infty _{j=1} \sqrt{\nu_j} \phi_j (x) \xi_j (\omega)\]
  with $\xi_j(\omega) := \frac1{\sqrt{\nu_j}} \left<u(x,\omega) - \mu(x), \phi_j(x)\right>_{L^2(D)}$ and $\{\nu_j, \phi_j\}_{j\in \mathbb N}$ are eigenpairs of $\mathcal C$ with $\nu_1 \geq \nu_2 \geq \ldots \geq 0$, and 
  \[(\mathcal C\phi)(x) = \int_D C(x,y) \phi(y) \dx{y}, \qquad x\in D,\]
  where $C$ is the covariance function of the random field $u$.
\end{bem}

\begin{satz}[KL uniform convergence]
  \label{2.8}
  If $D \subset \mathbb R^d$ is closed and bounded and $C\in C(D \times D)$, then $\phi_j \in C(D)$ and 
  \[\sup_{x,y \in D} | C(x,y) - C_J(x,y) | \leq \sup_{x\in D} \sum^\infty _{j=J+1} \nu_j \phi_j(x)^2 \to 0, \qquad \text{as } J \to 0\]
  with $C_J (x,y) = \sum^J_{j=1} \nu_j \phi_j(x) \phi_j(y)$ and 
  \[\sup_{x\in D} \mathbb E[(u(x)-u_J(x))^2] \to 0, \qquad \text{as } J \to \infty.\]
\end{satz}

\begin{kor}
  \label{2.9}
  If $C(x,y) = c(x-y)$, then
  \begin{equation}
    \int_D \mathbb V[u(x)] \dx{x} = c(0) |D| = \sum^\infty _{j=1} \nu_j
    \label{eqn:12}
    \tag{12}
  \end{equation}
  and
  \[\int_D \mathbb V[u(x) - u_J(x)] \dx{x} = c(0) |D| - \sum^J_{j=1} \nu_j.\]
\end{kor}
\begin{proof}
  We have
  \[\mathbb V[u(x)] = C(x,x) = \sum^\infty_{j=1} \nu_j \phi_j(x) ^2.\]
  Integrating over $D$ gives
  \[\int_D \mathbb V[u(x)] \dx{x} = c(0) |D| = \sum^\infty _{j=1} \nu_j,\]
  as $\|\phi_j\|_{L^2(D)} = 1$. Moreover
  \[\int_D \mathbb V[u(x) - u_J(x)] \dx{x} = \mathbb E\left[\left\| \sum_{j=J+1}^\infty \sqrt{\nu_j} \phi_j \xi_j\right\|^2_{L^2(D)} \right] = \sum_{j=J+1}^\infty \nu_j.\qedhere\]

\end{proof}

\begin{bem}
  \label{2.10}
For numerical computations, the relative error is of importance
\[E_J := \frac{\int_D \mathbb V[u(x) - u_J(x)] \dx{x}}{\int_D \mathbb V [u(x)] \dx{x}} = \frac{c(0) |D| - \sum^J_{j=1} \nu_j}{c(0) |D|}.\]

\begin{itemize}
  \item The decay rate of eigenvalues determines the decay of $E_J \to 0$.
  \item For a truncated KL, one has to solve the eigenvalue problem $\mathcal C \phi = \nu \phi$ for $\nu > 0$ and $\phi \in L^2(D)\setminus \{0\}$.
  \item Analytical solutions only available in few cases.
\end{itemize}
\end{bem}

\begin{beisp}
  \label{2.11}
  \begin{enumerate}[(i)]
    \item Exponential covariance in $d = 1$:
      \[C(x,y) = e^{-\frac{|x-y|}{l}} \qquad \text{on } D=[-a,a].\]
      The associated eigenvalue problem is finding $\{\nu_i,\phi_i\}$ such that
      \[\int_{-a}^a e^{-\frac{|x-y|}{l}} \phi(y) \dx{y} = \nu \phi(x), \qquad x \in D.\]
      Dervinig two times leads to the ODE 
      \[\frac{\dx{^2 \phi}}{\dx{x^2}} + \omega^2 \phi = 0 \qquad \text{with } \omega^2 := \frac{2l^{-1} - l^{-2} \nu}{\nu}.\]
      The solutions are
      \[\phi_i(x) = \begin{cases}A_i \cos(\omega_i x), &\text{if } i \text{ odd},\\B_i \sin(\omega_i x), &\text{if }i \text{ odd}.\end{cases}\]
      and
      \[\nu_i := \frac{2l^{-1}}{\omega_i^2 + l^{-2}}\]
      with $A_i$, $B_i$ such that $\|\phi_i\|_{L^2(-a,a)} = 1$.
    \item Separable exponential covariance for $d = 2$:
      \[C(x,y) = \prod^2_{m=1} e^{-\frac{|x_m-y_m|}{l_m}} \qquad \text{on } D=[-a_1,a_1] \times [-a_2,a_2].\]
      Then we have $\phi_j(x) = \phi_i^1(x_1) \cdot \phi_k^2(x_2)$ and $\nu_j = \nu_i^1 \nu_k^2$ with $\{\nu_i^1,\phi_i^1\}$ and $\{\nu_k^2, \phi_k^2\}$ eigenpairs of the 1-dimensional problems 
      \[\int_{-a_m}^{a_m} e^{-\frac{|x_m-y_m|}{l_m}} \phi^m(y) \dx{y} = \nu^m \phi^m(x), \qquad x \in [-a_m,a_m], m \in \{1,2\}.\]
  \end{enumerate}
\end{beisp}

In the following, for functions $f, h : \mathbb R^+ \rightarrow \mathbb R$, we use the notation ``$f(s) \asymp h(s)$ as $s \to \infty$'', read as \textbf{$f$ is asymptotic to $h$}, if $\frac{f(s)}{h(s)} \to 1$ as $s \to \infty$.

\begin{satz}[Widom]
  \label{2.12}
  Let $c^0(r)$ be an isotropic covariance function on $\mathbb R^d$ with spectral density function $f^0(s)$.
  Assume that $f'(s) \asymp bs^{-\rho}$ as $s \to \infty$ for some $b, \rho > 0$. Let $D$ be a bounded domain in $\mathbb R^d$ and consider eigenvalues $\nu_1 \geq \nu_2 \geq \ldots \geq 0$ of the covariance operator $\mathcal C$. As $j \to \infty$ 
  \[\nu_j \asymp k(D,d,\rho, b)\cdot j^{-\frac\rho d} \quad \text{for} \quad k(D,d,\rho,b) := (2\pi) ^{d-\rho} b (|D| V_d)^{\frac\rho d}\]
\end{satz}

\begin{beisp}
  \label{2.13}
  For the exponential covariance function $c(x) = e^{-\frac{|x|}{l}}$, spectral density $f(s) = \frac l{\pi (1+ l^2 s^2)}$. Hence $f(s) \asymp \frac 1{\phi l^2s^2}$, i.e. $b = (\pi l)^{-1}$ and $\rho = 2$.
\end{beisp}

\begin{beisp}[Whittle-Mat\'ern]
  \label{2.14}
  We consider the covariance function
  \[c^0_q(r) = \frac1{2^{q_1 \Gamma(q)}} \left(\frac rl\right)^q k_q\left(\frac rl\right)\]
  with spectral density
  \[f^0(s) = \frac{\Gamma\left(q+ \frac d2\right)}{\Gamma(q) \pi^\frac d2} \cdot \frac{l^d}{(1+l^2s^2)^{q + \frac d2}}.\]
  For $q = \frac12$, $l=1$ is $c^0_\frac12 (r) = e^{-\frac rl}$, i.e. the exponential covariance. 

  The parameter $q$ controls the regularity of the random field in $\mathbb R^d$. It is $n$ times mean-squared differentiable if $n < q$.

  We see
  \[f^0(s) \asymp bs^{-\rho} \quad \text{for} \quad b = l^{-2q} \cdot \frac{\Gamma\left(q + \frac d2\right)}{\Gamma(q) \pi^{\frac d2}}, \rho = 2q + d.\]
  Theorem \ref{2.12} yields
  \[\nu_j \asymp (2\pi l)^{-2q} \frac{\Gamma\left( q + \frac d2\right)}{\Gamma(q) \pi^{\frac d2}} \left(\frac{|D| V_d}{j}\right)^\frac \rho d.\]
\end{beisp}

\begin{satz}[$E_J$ for Whittle-Mat\'ern]
  \label{2.15}
  Let $D \subset \mathbb R^d$ be closed, bounded and let $u = \{u(x) : x \in D\}$ be a random field with Whittle-Mat\'ern covariance. Then 
  \[E_J \asymp k_{q,d} \frac 1{l^{2q}} \frac1{J^\frac{2q}d}\]
  where
  \[k_{q,d} := \frac{\Gamma\left(q+\frac d2\right)}{\Gamma(q) \phi^{\frac d2} (2\pi)^\frac1q} |D|^{\frac{2q}{d}} V_d^{1+\frac{2q}{d}} \cdot \frac d {2q}\]

  In the isotropic exponential case
  \[E_J \asymp k_{\frac12, d} \frac1{lJ^{\frac1d}}.\]
\end{satz}

\begin{bem}[Approximating Realisations]
  \label{2.16}
  Suppose $u$ is a gaussian field with mean function $\mu$ and covariance function $C$ and known eigenpairs $\{\nu_j, \phi_j\}$ of its covariance operator. Let $x_0,\ldots,x_{N-1}$ be sample points and $\mathbf u = (u(x_0),\ldots,u(x_{N-1}))^T$. Then $\mathbf u \sim \mathcal N(\bm \mu, C)$ with $\bm \mu = (\mu(x_0),\ldots,\mu(x_{N-1}))^T$ nd $C$ a matrix with entries $C_{i,j} = C(x_i, x_j)$. 

  An approximation of the random vector $\mathbf u$ can be calculated as $\mathbf u_J \sim \mathcal N(\bm\mu,C_J)$ with $C_J$ a matrix with entries $[C_J]_{i,j} = C_J(x_i,x_j) = \sum_{m=1}^J \nu_m \phi_m(x_i) \phi_m(x_j)$. Let $\bm \phi_j := (\phi_j(x_0),\ldots,\phi_j(x_{N-1}))^T$, $j\in \llbracket J\rrbracket$ and $\Phi_J = [\bm \phi_1,\ldots,\bm \phi_J] \in \mathbb R^{N\times J}$ and $\Lambda_J = \operatorname{diag}(\nu_1,\ldots,\nu_J) \in \mathbb R^{J\times J}$. Then
  \[\mathbf u_J = \mu + \Phi_J \Lambda^\frac12 \bm \xi\]
  with $\bm \xi \sim \mathcal N(0,I_J)$. Then the covariance matrix of $\mathbf u_J$ is $C_J = \Phi_J \Lambda \Phi_J^T$ and if it is close to $C$, then the distribution of $\mathbf u_J$ is close to the distribution of $\mathbf u$.

  Recall the third lecture: for $\phi \in L^2(\mathbb R^N)$ we have
  \[|\mathbb E[\phi(\mathbf u)] - \mathbb E[\phi(\mathbf u_J)]| \leq \|\phi\|_{L^2(\mathbb R^N)} \cdot \|C - C_J\|_F.\]
\end{bem}

\begin{bem}[Approximating Eigenpairs]
  \label{2.17}
  Note that discretizing $\mathcal C \phi = \nu \phi$ leads to large dense matrices. 

  \textbf{Collocation}: Let $x_1,\ldots,x_p \in D$ and define the residuals
  \[R_j (x) := \int_D C(x,y) \hat \phi_j (y) \dx{y} - \hat \nu_j \hat\phi_j(x).\]
  Approximating $\{\hat \nu_j, \hat \phi_j\}$  requires $R_j(x_k) = 0$ for $k = 1,\ldots,P$.

  Assume we dispose of a quadtrature rule with points and weights $(x_k,q_k)_{k\in \llbracket p\rrbracket}$, then 
  \[\sum^p_{i=1} q_i C(x_k, x_i) \phi(x_i) \approx \int_D C(x_k, y) \phi(y) \dx{y} = \nu_j \hat \phi_j (x_k)\]

  Solving yields $\{\hat \nu_j, \hat \phi_j\}$. In matrix notation this is $CQ \hat \phi_j = \hat\nu_j \hat \phi_j$ with $C \in \mathbb R^{P\times P}$, $Q \in \mathbb R^{P\times P}$ a diagonal weight matrix.

  Conversion to a symmetric problem of the form $Sz_j = \hat \nu_j z_j$ is possiblevia $S := Q^\frac12 C Q^\frac12$ and $z_j = Q^\frac12 \hat \phi_j$.

  \textbf{Galerkin}: Assume $V_P = \operatorname{span} \{ \varphi_1,\ldots,\varphi_P\}$ and determine $\hat \phi_j \in V_P$ by solving
  \[\int_D\int_D C(x,y) \hat \phi_j(y) \dx{y} \varphi _i(x) \dx{x} = \hat \nu_j \int_D \hat \phi_j \varphi _i(x)\dx{x}, \qquad i\in \llbracket P\rrbracket.\]

  or $\int R_j(x) \varphi _i(x) \dx{x} = 0$ with
  \[R_j (x) = \int_DC(x,y) \hat \phi_j (y) \dx{y} - \hat \nu_j \hat \phi_j(x).\]
  The choice $\varphi _j (x) = \delta (x-x_j)$ is the  collocation approach. 

  Since $\hat \phi_j (x) = \sum^P_{k=1} a_k^j \varphi _k(x)$ for some coefficients $a_k^j$

  \[\sum^P_{j=1} a_k^j \int_D \int_D C(x,y) \varphi _i(y) \varphi _i(x) \dx{y}\dx{x} = \hat \nu_j \sum^P_{k=1} a_k^j \int_D \varphi _j(x) \varphi _i(x) \dx{x}.\]
$\Rightarrow$ $Ka_j = \hat \nu_j Ma_j$, which is a generalized eigenvalue problem with $K_{i,j} = \int_D \int_D C(x,y) \varphi _i(y) \varphi _j(x) \dx{y}\dx{x}$ and $M_{i,j} = \int_D \varphi _i(x) \varphi _j(x) \dx{x}$.
\end{bem}
