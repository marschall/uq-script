
\section{Elliptic PDEs with Correlated Random Data}
\begin{bem}[Model Problem]
  \label{3.1}
  Assume $D \subset \mathbb R^2$ and data 
  \[a,f \in L^2(\Omega; L^2(D)) = \left\{v : D\times \Omega \rightarrow \mathbb R \middle| \|v\|_{L^2(\Omega; L^2(D))} := \mathbb E\left[\|v\|^2_{L^2(D)}\right] < \infty\right\}.\]
  Our model problem is to find $u : \overline D \times \Omega \rightarrow \mathbb R$ such that for $\mathbb P$-a.e. $\omega \in \Omega$ we have
  \[\left\{\begin{array}{l}-\nabla \cdot (a(x,\omega) \nabla u(x,\omega)) = f(x,\omega), x\in D\\
    f(x,\omega) = g(x), x \in \partial D\end{array}\right.\]
  in the weak sense with a deterministic $g \in H^\frac12 (\partial D)$.
\end{bem}

\begin{bem}[Variational Formulation]
  \label{3.2}
  We first simplify by assuming $f \in L^2(D)$, i.e. $f$ does not depend $\omega \in \Omega$, and $g = 0$.
  Given realisations $a(\cdot,\omega)$ of the diffusion coefficient, consider the random field $u$ with realisations $u(\cdot,\omega) \in V := H^1_0 (D)$ satisfying 
  \begin{equation}
    \int_D a(x,\omega) \nabla u(x,\omega) v(x) \dx{x} = \int_D f(x) v(x) \dx{x}, \qquad \text{for all } v \in V.
    \tag{WP}
    \label{eqn:WP}
  \end{equation}
  We assume uniform ellipticity for the coefficient $a$, i.e.
  \begin{equation}
    0 < a_\text{min} \leq a(x,\omega) \leq a_\text{max} < \infty
    \tag{UB}
    \label{eqn:UB}
  \end{equation}
  for $\mathbb P$-a.e.\ $\omega \in \Omega$ and every $x \in D$, i.e. $a \in L^\infty(\Omega; L^\infty(D))$
\end{bem}

\begin{beisp}
  \label{3.3}
  In $D = (0,1)$, consider $\mu > 0$ and 
  \[a(x,\omega) = \mu + \sum^p_{k=1} \frac{\sigma}{k^2 \pi^2} \cos(\pi k x) \xi_k (\omega), \qquad \xi_1,\ldots,\xi_p \overset{i.i.d.}{\sim} \mathcal U([-1,1]).\]
  For $x \in D$ and $\omega \in \Omega$ we have thus
  \[\mu - \sum^p_{k=1} \frac{\sigma}{k^2 \pi^2} \leq a(x,\omega) \leq \mu + \sum^p_{k=1} \frac{\sigma}{k^2 \pi^2}\]
  Since $\sum^p_{k=1} \frac1{k^2} \xrightarrow{p \to \infty} \frac{\pi^2}{6}$, \eqref{eqn:UB} holds for $\sigma < 6 \mu$ for $p\geq0$.
\end{beisp}
\begin{beisp}[Essential Boundedness]
  \label{3.4}
  A weaker assumption for the coefficient is $a(\cdot,\omega) \in L^\infty(D)$ a.s., i.e.
  \begin{equation}
    0 < a_\text{min}(\omega) \leq a(x,\omega) \leq a_\text{max} (\omega) < \infty, \qquad \text{a.e.\ in } D
    \tag{EB}
    \label{eqn:EB}
  \end{equation}
  Hence we can set $a_\text{min} (\omega) := \essinf_{x\in D} a(x,\omega)$ and $a_\text{max}(\omega) := \esssup_{x\in D} a(x,\omega)$.

  Obviously \eqref{eqn:UB} implies \eqref{eqn:EB} but not vice versa.
\end{beisp}

Many random fields satisfy some Hölder continuity. Let $a := e^{z}$ with $z$ a mean-zero random field such that, for $L, s > 0$ we get
\begin{equation}
  \mathbb E\left[|z(x) - z(y)|^2\right] \leq L\|x-y\|_2^s, \qquad x,y \in \overline D.
  \tag{14}
  \label{eqn:14}
\end{equation}

\begin{satz}
  \label{3.5}
  If \eqref{eqn:14} holds for $a = e^z$, then $a$ fulfills \eqref{eqn:EB}.
\end{satz}
\begin{proof}
  Based on the following theorem (which is some variant of Kolmogorov's continuity theorem): Let $D$ be bounded, $z = \{z (x) : x\in \overline D\}$ mean-zero and Gaussian such that \eqref{eqn:14} holds. Then, for $p \geq 1$, there exists a random variable $k$ such that $e^k \in L^p(\Omega)$ and
  \[|z(x) - z(y) | \leq k \cdot \|x-y\|_2^{\frac12 (s-\varepsilon)}, \qquad x,y \in \overline D \text{ a.s.}\]
  This then implies that $z$ has Hölder-continuous sample paths with an exponent $\gamma < \frac12 s$ and is uniformly bounded over $x \in \overline D$ a.s. Since $a(x,\omega) = e^{z(x,\omega)}$ is always positive, $a$ satisfies \eqref{eqn:EB}.
\end{proof}

\begin{kor}
  \label{3.6}
  Let $z$ be a Gaussian random fields with isotropic covariance $c^0(r) = e^{-\frac{r^2}{l^2}}$ and mean zero. Then, $a = e^z$ satisfies \eqref{eqn:EB}.
\end{kor}
\begin{proof}
  Since $\mathbb E\left[|z(x)-z(y)|^2\right] = 2(c^0(0) - c^0(r))$ with $r := \|x-y\|_2$ and hence 
  \[\mathbb E\left[|z(x)-z(y)|^2\right] = 2 \left(e^0 - e^{-\frac{r^2}{l^2}}\right) \leq 2 \left|\left(0 - \frac{-r^2}{l^2}\right)\right| = \frac2{l^2} \|x-y\|^2_2.\]
  Hence \eqref{eqn:14} holds with $L = \frac2{l^2}$ and $s=2$ and
\end{proof}

\begin{satz}
  \label{3.7}
  If $a$ fulfills \eqref{eqn:EB}, $f \in L^2(D)$ and $g = 0$. Then, $\mathbb P$-a.s. \eqref{eqn:WP} has a unique solution $u(\cdot,\omega) \in V = H^1_0(D)$. 
\end{satz}
\begin{proof}
  Lax-Milgram.
\end{proof}

\begin{proof}
  \begin{itemize}
    \item This is a ``pathwise'' result for almost all $\omega \in \Omega$.
    \item In the sense, classical deterministic estimates can be applied.
    \item We study $p$-th moments $\|u\|_{L^p(\Omega;V)} := \mathbb E[\|u\|_V^p]$
    \item For this, the random variables $a_\text{min}$ und $a_\text{max}$ have to be in certain $L^p$-spaces.
  \end{itemize}
\end{proof}

\begin{lem}
  \label{3.8}
  Let \eqref{eqn:EB} hold, then 
  $a_\text{min}^{\lambda _1}, a_\text{max}^{\lambda _2}, a_\text{min}^{\lambda _1} \cdot a_\text{max}^{\lambda _2} \in L^p(\Omega)$, for $p \geq 1$, $\lambda _1, \lambda _2 \in \mathbb R$. 
\end{lem}

\begin{satz}
  \label{3.9}
  Let the conditions of Theorem \ref{3.7} hold such that $u(\cdot,\omega) \in V$ satisfies \eqref{eqn:WP} $\mathbb P$-a.s. If \eqref{eqn:EB}, then $u \in L^p(\Omega; V)$ for $p \geq 1$ and 
  \begin{equation}
    \|u\|_{L^p(\Omega;V)} \leq K_p \|a_\text{min}^{-1}\|_{L^p(\Omega)} \|f\|_{L^2(\Omega)}.
    \tag{15}
    \label{eqn:15}
  \end{equation}
\end{satz}

\begin{proof}
  Recall the following result from classical PDEs:

  Assume we dispose of a weak solution $u \in V$ to \eqref{eqn:WP} with $f \in L^2(D)$, i.e. $a(u,v) = l(v)$ with $a(u,v) = \int_D a(x) \nabla u(x) \nabla v(x) \dx{x}$ and $l(v) = \left<f,v\right>$. Then $|u|_{H^2(D)} \leq K_p a^{-1}_\text{min} \|f\|_{L^2(D)}$.

  Hence, $\|u(\cdot,\omega)\|_V \leq K_p a_\text{min}^{-1} (\omega) \| f\|_{L^2(D)}$ for almost all $\omega \in \Omega$. Hence, 
  \[ \mathbb E\left[\|u\|_V^p\right] \leq K_p^p \mathbb E\left[a_\text{min}^{-p}\right] \|f\|^p_{L^2(D)}\]
  and taking $p$-th roots gives \eqref{eqn:15}. Since $a_\text{min}^{-1} \in L^p(\Omega)$ for $p\geq 1$ by Lemma \ref{3.8}. This shows $\mathbb E\left[\|u\|_V^p\right] < \infty$ or $u\in L^P(\Omega; V)$
\end{proof}

\begin{bem}
  \label{3.10}
  Consider a FEM space $V^h \subset V = H^1_0(D)$ on a shape-regular mesh $\mathcal T_h$. Then, the discrete solution $u_h(\cdot,\omega) \in V^h$  satisfies \eqref{WP} with $v \in V_h$. Then Theorem \ref{3.9} holds verbatim. To establish a bound for the $L^2$-error $\|u-u_h\|_{L^2(\Omega; V)}$, we assume to have $K > 0$ such that for $f \in L^2(D)$, we have $u \in L^4(\Omega; H^2(D))$ and 
  \begin{equation}
  |u|_{L^4(\Omega; H^2(D))} := \mathbb E[|u|_{H^2(D)}^4]^\frac14 \leq K \|f\|_{L^2(D)}
    \label{eqn:16}
    \tag{16}
  \end{equation}
\end{bem}

\begin{satz}
  \label{3.11}
  Let the conditions of Theorem \ref{3.7} hold and suppose \eqref{eqn:14} and \eqref{eqn:16} hold. Let $V_h \subset V$ be piecewise linear, then 
  \[\|u-u_h\|_{L^2(\Omega;V)} \leq Kh\|a_\text{min}^{-\frac12} a_\text{max}^\frac12 \|_{L^4 (Omega)} \|f\|_{L^2(D)}\]
  for $K> 0$ independent of $h$.
\end{satz}

\begin{proof}
  For each $u(\cdot,\omega)\in V$ and the FEM approximation $u_h (\cdot,\omega) \in V_h$,
  \[\|u-u_h\|_V \leq K h a_\text{min}^{-\frac12} (\omega) a_\text{max}^\frac12 (\omega) |u(\cdot,\omega)| _{H^2(D)}.\]
  Hence, 
  \[\mathbb E\left[\|u-u_h\|_V^2\right] \leq K^2 h^2 \mathbb E\left[a^{-1}_\text{min} a_\text{max}^{-1} |u|^2_{H^2(D)}\right] \leq K^2 h^2 \mathbb E\left[\left(a_\text{min}^{-\frac12} a_\text{max}^\frac12\right)^4\right]^\frac12 \mathbb E\left[|u|^2_{H^2(D)}\right]^\frac12.\]

  Taking square roots and \eqref{eqn:16} gives the result.
\end{proof}


Assume $\tilde a$ to be an approximation of $a$ and $\tilde u_h$ be the solution of the perturbed weak problem \eqref{eqn:WP} with $\tilde a$ in $V_h$.

\begin{satz}[Implications of Approximate Data]
  \label{3.12}
  Let $\tilde a$ satisfy \eqref{eqn:EB} with random variables $\tilde a_\text{min}, \tilde a_\text{max}$. Let the conditions of Theorem \ref{3.7} hold such that $u_h(\cdot,\omega) \in V_h$ satisfies \eqref{eqn:WP}. Then 
  \[\|u_h - \tilde u_h\| \leq \|\tilde a^{-1}_\text{min}\|_{L^8(\Omega)} \|a-\tilde a\|_{L^8(\Omega; L^\infty (D))} \|u_h\|_{L^4(\Omega; V)}.\]
\end{satz}
\begin{proof}
  Recall the standard FEM perturbation result
  \[\|u_h (\cdot,\omega) - \tilde u(\cdot,\omega)\|_V \leq \tilde a^{-1}_\text{min}(\omega) \|a(\cdot,\omega) - \tilde a(\cdot,\omega)\|_{L^\infty(D)} \|u_h(\cdot,\omega)\|_V, \qquad \mathbb P\text{-a.s.}\]
  Hence taking the expected values and proceding as in the above proof gives the result:
  \[\mathbb E[\|u-u_h\|_V^2] \leq \mathbb E\left[a_\text{min}^{-4} \|a-\tilde a\|^4_{L^\infty(D)}\right]^\frac12 \mathbb E\left[\|u_h\|^4_V\right]^\frac12.\]
  Taking square roots concludes the proof.
\end{proof}

%Tue Nov 17 10:24:41 CET 2015
\begin{bem}[Monte Carlo FEM -- MCFEM]
  \label{3.13}
  For $Q \in \mathbb N$ samples $\tilde a_r := \tilde a(\cdot, \omega_r)$, $r \in \llbracket Q \rrbracket$ of $\tilde a$, e.g. by piecewise constant approximation on $T_K 
  \in \mathcal T_h$ such that 
  \[\tilde a(x,\omega) \big|_{T_k} := \frac1J \sum^J_{j=1} a(x_j^k, \omega)\]
  with $T_k = \operatorname{conv}\{x^k_j | j \in \llbracket J \rrbracket\}$, generate i.i.d.\ samples $\tilde u^r_h (x) := \tilde u_h(x,\omega_r)$ of the FEM solution $\tilde u_h(x)$ by solving $Q$ variational problems $(\tilde WP)$
  Then, $\mathbb E[\tilde u(x)] \approx \mu_{Q,h}(x) := \frac1Q \sum^Q_{r=1} \tilde u_h^r(x)$, $\mathbb V[\tilde u(h)] \approx \sigma^2_{Q,h} (x) := \frac1{Q-1} \sum^Q_{r=1} \left(\tilde u^n_h(x) - \mu_{Q,h} (x)\right)^2$.

  With approximations $\tilde a(x,\omega)$ it can be shown, for sufficiently smooth covariance, 
  \begin{itemize}
    \item $\|a-\tilde a\|_{L^\infty (D)} \leq a_\text{max} k h$ for r.v. $k \in L^p(\Omega)$
    \item $\|u_h - \tilde u_h \|_{L^2(\Omega; V)} \lesssim h\|u\|_{L^4(\Omega; V)}$.
  \end{itemize}
  
  \textbf{Error analysis}
  \begin{equation}
    \|\mathbb E[u] - \mu_{Q,h}\|_V \leq \underbrace{\|\mathbb E[u] - \mathbb E[u_h] \|_V}_{=:E_{h,a}} + \underbrace{\|\mathbb E[u_h] - \mu_{Q,h}\|_V}_{E_{MC}}
    \tag{17}
    \label{eqn:17}
  \end{equation}
  We do have
  \[E_{h,a} \leq \|\mathbb E[u] - \mathbb E[u_h] \|_V + \|\mathbb E[u_h] - \mathbb E[\tilde u_h]\|_V \leq \|u - u_h \|_{L^2(\Omega;V)} + \|u_h - \tilde u_h\|_{L^2(\Omega; V)} \lesssim h\|f\|_{L^2(D)} = \mathcal O(h),\]
  where we used theorem \ref{14} for $u_h$ and theorem \ref{16}.

  An analysis of the statistical error $\mathbb E[E_{MC}^2] = \mathcal O(Q^\frac12)$
\end{bem}

\begin{satz}
  \label{3.14}
  For $k > 0$ independent of $h$, $\mathbb E[E_{MC}^2] \leq kQ^\frac12$.
\end{satz}
\begin{proof}
  \[\mathbb E[E_{MC}^2] = \mathbb E[\|\mathbb E[\tilde u_h] - \mu_{Q,h}\|^2_V] = \frac1{Q^2} \mathbb E\left[\left\| \sum^Q_{r=1} \left(\mathbb E[\tilde u_h] - \tilde u_h^r\right)\right\|^2_V\right]\]
  Here, we remark, that $\mathbb E[\tilde u_h] - \tilde u_h^r =: X_r$ are i.i.d.\ $V$-valued r.v.\ which are uncorrelated and
  \[\mathbb E[\|X_r\|^2_V] \leq \mathbb E[\|\tilde u_h^r\|_V] = \mathbb E[\|\tilde u_h\|_V]\]
  for each $r\in \llbracket Q \rrbracket$, since the $\tilde u^r_h$ are i.i.d.\ samples of $\tilde u_h$. Hence
  \[\mathbb E[E_{MC}^2] = \frac1Q \mathbb E[\|\tilde u_h\|^2_V] = \frac1Q \|\tilde u_h\|^2_{L^2(\Omega;V)}.\]
  With a result analog to Theorem \ref{14} and $p=2$, the claim follows with $k = k_p \|a^{-1}_\text{min}\|_{L^2(\Omega)}\|f\|_{L^2(D)}$.
\end{proof}

\begin{kor}
  \label{3.15}
  \label{kor:4}
  For any $\varepsilon > 0$ and some $L > 0$,
  \[\mathbb P\left[E_{MC} > Q^{-\frac12 + \varepsilon}\right] \leq LQ^{-2\varepsilon}.\]
\end{kor}

\begin{proof}
  If $X_n \xrightarrow{\mathbb P} X$, i.e.\ for any $\varepsilon >0$ we have
  \[\mathbb P[\|X_n - X \| > \varepsilon] \xrightarrow{n\to\infty} 0,\]
  then we have also 
  \[\mathbb P[\|X_n-X\| \geq n^{-r + \varepsilon} ] \leq k(p) n^{-p\varepsilon}.\]
  Apply this with $p = 2$, $r = \frac12$.
\end{proof}

Hence $\|\mathbb E[u] - \mu_{Q,h}\|_V = \mathcal O(h) + \mathcal O(Q^\frac12)$.

\begin{bem}
  \label{3.16}
  \begin{itemize}
    \item The convergence may be extremely slow, since $L$ depends on $a_\text{min}^{-1}$.
    \item The costs consist of computation of $Q$ samples, i.e. generation of field samples and the associated solution of the PDE
    \item For standard implementation, costs can be $\mathcal O(\varepsilon ^{-4})$
  \end{itemize}
\end{bem}
