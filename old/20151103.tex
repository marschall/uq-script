\begin{bem}[Variational formulation on $D\times \Omega$]
  \label{3.17}
  Weak solution $u : D \times \Omega \rightarrow \mathbb R$ for \ref{eqn:MP} and $g = 0$ with $u \in L^2(\Omega; H^1_0(D))$. We assume \eqref{eqn:UB}, i.e.\ $a_\text{min}, a_{\text{max}}$ to be consants.
\end{bem}

\begin{def2}[Weak solution of $D \times \Omega$]
  \label{3.18}
  A weak solution to \eqref{MP} with boundary data $g = 0$ and right-hand side $f \in V$ is a function $u \in V := L^2(\Omega; H^1_0(D))$ such that 
  \begin{equation}
    a(u,v) = l(v) \text{ for all } v \in V 
    \tag{$WP_\Omega$}
    \label{eqn:18}
  \end{equation}
  where $a(\cdot, \cdot) : V \times V \rightarrow \mathbb R$, $l : V \rightarrow \mathbb R$ are defined by
  \[a(u,v) := \mathbb E\left[ \int _D a(x,\cdot) \nabla u(x,\cdot) \cdot \nabla v(x,\cdot) \dx{x} \right], \]
  \[l(v) := \mathbb E\left[\int_D f(x,\cdot) v(x,\cdot) \dx{x}\right].\]
\end{def2}

\begin{satz}[well-posedness]
  \label{3.19}
  \label{thm:19}
  If $f \in L^2(\Omega; L^2(D))$, $g = 0$ and coefficients $a(x,\cdot)$ is \eqref{eqn:UB}, then \eqref{eqn:18} has a unique solution $u \in V$. Further, if $a(x,\cdot)$ and $f(x,\cdot)$ is $\mathscr G$-measurable, for some sub-$\sigma$-field $\mathscr G \subset \mathscr F$, then $u(x,\cdot)$ is $\mathscr G$-measurable. 
\end{satz}
\begin{proof}
  For $v \in V$ define the norm
  \[\|v\|_V := \mathbb E[|v|_{H^1(D)}^2]^\frac12.\]
  The form $a(\cdot,\cdot)$ is bounded on $V\times V$, since 
  \[|a(u,v)| \leq a_\text{max} \mathbb E[|u|^2_{H^1(D)}]^\frac12 \mathbb E\left[|v|^2_{H^1(D)}\right]^\frac12 = a_\text{max} \|u\|_V \|v\|_V\]
  Coercivity is immediate, 
  \[a(v,v) = \mathbb E[\int_D a(x,\cdot) |\nabla v(x,\cdot)|^2 \dx{x}] \geq a_\text{min}\|v\|^2_V.\]

  By Cauchy-Schwartz
  \[|l(v)| \leq \|f\|_{L^2(\Omega;L^2(D)} \|v\|_{L^2(\Omega; L^2(D))} \leq \|f\|_{L^2(\Omega; L^2(D))} k_p \|v\|_V.\]
  This gives existance and uniqueness by Lax-Mailgram.
  Since $u \in L^2(\Omega, \mathscr F, \mathbb P; H^1_0(D))$, $u(x,\cdot)$ is $\mathscr F$-measurable.
\end{proof}

\begin{bem}
  \label{3.20}
  \begin{enumerate}[(i)]
    \item extensions to non-homogeneous boundary conditions as in the deterministic case. In particular for $g \in H^\frac12(\partial D)$, $f \in L^2(\Omega; L^2(D))$, 
      \[|u|_W := \mathbb E[|u|^2_{H^1(D)}]^\frac12 \leq k\left(\|f\|_{L^2(\Omega; L^2(D))} + \|g\|_{H^\frac12 (\partial D)}\right).\]
      with 
      \[k := \max \left\{\frac{k_p}{a_\text{max}}, k_\gamma\left(1+\frac{a_\text{max}}{a_\text{min}}\right)\right\},\]
      with $k_\gamma$ such that for any $ g \in H^\frac12 (\partial D)$, $\|u_g\|_{H^1(D)} \leq k_\gamma \|g\|_{H^\frac12 (\partial D)}$ for some $u_g \in H^1(D)$ such that $\gamma u_{g} = g$ with trace operator $\gamma$.


    \item Similar results hold for perturbed weak forms with $\tilde a$, $\tilde f$ and $\tilde u$ such that 
      \[\tilde a(\tilde u, v) = \tilde l (v) \text{ for all } v\in V.\]
      Moreover, 
      \[|u-\tilde u|_W \leq k_p \tilde a_\text{min}^{-1} \|f - \tilde f\|_{L^2(\Omega; L^2(D))} + \tilde a_\text{min} \|a-\tilde a\|_{L^\infty(\Omega; L^\infty(D))} |u|_W.\]
  \end{enumerate}
\end{bem}

\begin{bem}[Truncated KL exp.]
  \label{3.21}
  Assume r.v.\ of the KLE of $a(x,\cdot)$ and $f(x,\cdot)$ have the same distribution and
  \begin{equation}
    \tilde a(x,\cdot) := \mu_a(x) + \sum^P_{k=1} \sqrt{\nu^a_k} \phi^a_k(x) \xi_k(\omega)\\
    \tilde f (x,\cdot) := \mu_f(x) + \sum^P_{k=1} \sqrt{\nu^f_k} \phi^f_k(x) \xi_k(\omega)\\
    \label{eqn:20}
  \end{equation}
  Moreover, assume independence of $\xi_k$. Since $a(x,\cdot)$ should be \eqref{eqn:UB}, we assume uniformly bounded r.v. in the KLEs. 

  $\xi_k$ are independent and bounded with $\xi_k \in [-\gamma, \gamma]$ for some $\gamma \in \mathbb R^+$. This is not sufficient for \eqref{eqn:UB}
\end{bem}

\begin{beisp}
  \label{3.22}
  \label{ex:11}
  The stochastic process of example \ref{ex:10} is an KLE of the form \eqref{eqn:20} with 
  $\xi_k \in [-1,1]$ and
  \[v_k = \frac{\sigma^2}{k^4\pi^4}, \phi^w_k(x) = \cos(\pi k x),\]
  requires $\sigma < 6 \mu$.
\end{beisp}


We require that $\|a - \tilde a\|_{L^\infty(\Omega; L^\infty(D))} \to 0$ as $P \to \infty$, but Theorem \ref{thm:9} only states 
\[\sup_{x\in D} \mathbb E\left[|a(x) - \tilde a(x)|^2\right] \to 0,\]
as $P \to \infty$ for $C_a \in C(D \times D)$. Observe that 
\[\|a - \tilde a\|_{L^\infty (\Omega; L^\infty (D))} = \esssup_{(x,\omega) \in D\times \Omega} \left| \sum^\infty _{k=P+1} \sqrt{\nu^a_k} \phi_k^a (x) \xi_k(\omega) \right|\]
and with the boundedness of the $(\xi_k)$ we get 
\[\|a-\tilde a\|_{L^\infty(\Omega; L^\infty(D))} \leq \gamma \sum^\infty _{k=P+1} \sqrt{\nu^a_k} \|\phi_k^a\|_\infty.\]
In example \eqref{ex:11}, $\gamma = 1$ and $\sqrt{\nu^a_k} \|\phi^a_k\|_\infty \leq \sigma \pi^{-2} k^{-2}$. 

Hence $\|a- \tilde a\|_{L^\infty(\Omega; L^\infty (D)} \to 0$ as $P \to \infty$, and since $\|\phi^a_k \|_{L^2(D)} = 1$ in the KLE, $\|\phi^a_k\|_{L^\infty(D)}$ can be expected to grow as $K \to \infty$. This has to be compensated by the decay of the eigenvalues $(\nu^a_k)$.

It can be shown that for $\frac d2 < r < 2q$ and $C \in C^{2q} (D\times D)$, that
\begin{equation}
  \|\phi_k\|_{L^\infty (D)} < k \nu_k^{-\frac r{(2q-r)}}, \qquad \text{for } k = 1,\ldots
  \tag{22}
  \label{eqn:22}
\end{equation}

\begin{beisp}
  \label{3.23}
  The Whittle-Mat\'ern-Covariance is $2q$-times differentiable. Thus
  \[\|\phi^a_k\|_{L^\infty(D)} = \mathcal O\left( (\nu^a_k)^{- \frac{r}{2q-r}} \right), \qquad \text{for } r > \frac d2\]
  By Example \ref{ex:9},
  \[\nu^a_k = \mathcal O\left(k^{-\frac{2q +d }{d}} \right).\]
  Using $q \geq 2d$ gives $\nu^a_k = \mathcal O(k^{-5})$ and $\frac{r}{2q-r} \leq \frac17$, i.e.
  \[\sqrt{\nu^a_k} \|\phi^a_k\|_{L^\infty (D)} = \mathcal O\left(k^{-\frac52} k^{\frac{5}7}\right).\]
  This gives convergence, since $\frac{-5}2 +\frac{5}{7} < -1$. 

\end{beisp}


In general the eigenpairs of $C_a$ and $C_f$ are unknown, which makes numerical approximations neccessary. 

\begin{bem}[Variational formulation in $D \times \Gamma$]
  \label{3.24}
  Integrals $\int_\Omega \dx{\mathbb P}$ involve an abstract set $\Omega$ and $\mathbb P$-measure which is numerically inconvinient. Assume $\tilde a(x)$ and $\tilde f(x)$ to depend on $M < \infty$ many r.v.\ $\xi_k : \Omega \rightarrow \Gamma_k \subset \mathbb R$, $k \in \llbracket M\rrbracket$, e.g.\ via a KLE. Then one can equivalently formulate the problem on $D \times \Gamma$ with image/parameter domain $\Gamma := \Gamma_1 \times \ldots \times \Gamma_M \subset \mathbb R^M$.
\end{bem}

\begin{def2}
  \label{3.25}
  \label{def:17}
  A function $v \in L^2(\Omega; L^2(D))$ of the form $v(x,\xi(\omega))$, $(x,\omega) \in D\times \Omega$. The variable $\xi := (\xi_1,\ldots,\xi_M)^T : \Omega \rightarrow \Gamma \subset \mathbb R^M$ is called \textbf{finite-dimensional} or \textbf{$M$-dimensional noise}. (FDN)
\end{def2}

\begin{beisp}[Piecewise random functions]
  \label{3.26}
  \label{ex:13}
  Partition $D$ into $M$ non-overlapping $D_k$ such that $\overline D = \bigcup^M_{k=1} \overline{D_k}$ and let
  \[\log(a(x,\omega) - a_0) = \sum_{k=1}^M \xi_k(\omega) \mathbbm 1_{D_k}(x),\]
  with $a_0 > 0$ and $\xi_k \sim \mathcal N(\mu_k, \sigma_k^2)$. On each $D_k$, $a(x) = a_0 + e^{\xi_k}$ is a log-normal r.v. with 
  \[\mathbb E\left[a(x)\big|_{D_k}\right] = a_0 + e^{\mu_k + \frac{\sigma_k^2}2}, \qquad \mathbb V\left[a(x)\big|_{D_k}\right] = \left(e^{\sigma_k^2} - 1\right) e^{2\mu_k + \sigma^2_k}.\]

  If $v = v(x,\xi)$ is FDN, then with the joint density $p$ of $\xi$, e.g. 
  \[\|v\|^2_{L^2(\Omega; L^2(D))} = \mathbb E\left[\|v\|^2_{L^2(D)}\right] = \int_\Gamma p(y) \|v(\cdot,y)\|^2_{L^2(D)} \dx{y}.\]
  By $y_k := \xi_k(\omega)$ and $y = (y_1,\ldots,y_M)^T$ any FDN random function $v \in L^2(\Omega; L^2(D))$ can be associated with a function $v = v(x,y)$ in 
  \[L^2_p(\Gamma; L^2(D)) := \{v : D \times \Gamma \rightarrow \mathbb R | \int_\Gamma p(y) \|v(\cdot, y)\|_{L^2(D)} \dx{y} < \infty.\]
\end{beisp}

\begin{lem}
  \label{3.27}
  \label{lem:9}
  Let $\tilde a(x)$, $\tilde f(x)$ be FDN. Then $\tilde u \in V$ satisfying ($\tilde{WP_\Omega)}$) is also FDN.
\end{lem}

\begin{proof}
  Since $\tilde a(x,\cdot)$, $\tilde f(x,\cdot)$ are functions of $\xi : \Omega \rightarrow \Gamma$, they are $\sigma(\xi)$-measurable and by Theorem \ref{thm:19}, $\tilde u(x,\cdot)$ is $\sigma(\xi)$-measurable. 
  By Doob-Dynkin lemma, $\tilde u(x,\cdot)$ is a function of $\xi$.
\end{proof}

\begin{lem}[Doob-Dynkin]
  \label{3.28}
Let $H_1, H_2$ be separable Hilbert spaces and $Z, Y$ be $H_1$-valued (resp. $H_2$) r.v. If $Z$ is $\sigma(Y)$-measurable, then $Z = \phi(Y)$ for some Borel-measurable function $\phi: H_2 \rightarrow H_1$.
\end{lem}

\begin{bem}[Weak solution on $D\times \Gamma$]
  \label{3.29}
  Equivalent to ($\tilde{WP_\Omega}$), $\tilde u \in W$ with 
  \[W := L^2_p(\Gamma; H^1_g(D)) := \left\{v : D \times \Gamma \rightarrow \mathbb R \middle| \int_\Gamma p(y) \|v(\cdot, y)\|^2_{H^1(D)} \dx{y} < \infty, \gamma v = g\right\}\]
  and likewise $L^2_p(\Gamma; H^1_0 (D)) = V$. Then ($\tilde{WP_\Gamma}$) $\tilde a(\tilde u, v) = \tilde l(v)$ for all $v \in V$ with
  \[\tilde a(u,v) := \int_\Gamma p(p) \int_D \tilde a(x,y) \nabla u(x,y) \cdot \nabla v(x,y) \dx{x} \dx{y},\]
  \[\tilde l(v) := \int_\gamma p(y) \int_D \tilde f(x,y) v(x,y) \dx{x} \dx{y}\]
  with weight $p : \Gamma \rightarrow \mathbb R^+$ the joint density of $\bm \xi = (\xi_1,\ldots,\xi_M)^T$ with $M = \max\{P,N\}$ and $\bm \xi : \Omega \rightarrow \Gamma \subset \mathbb R^M$. Note that ($\tilde{WP_\Gamma}$) is an $(M+2)$-dimensional problem on $D\times \Gamma$ and we use $\Gamma$ instead of $\Omega$, e.g.
  \[\tilde a(x,y) = \mu_0(x) + \sum^P_{k=1} \sqrt{\nu^a_k} \phi^a_k(x) y_k, \qquad x\in D, y \in \Gamma.\]
  The stochastic probelm is transformed into a parametric problem. 

\end{bem}

\begin{bem}[Stochastic Galerkin FEM on $D \times \Gamma$]
  \label{3.30}
  We first distretise the physical space, i.e. $W^h \subset H^1_g(D)$ and $V^h \subset H^1_0(D)$ by FEM and $\tilde u_h \in L^2_p(\Gamma; W^h)$.
\end{bem}

Semi-discretization

\begin{def2}
  \label{3.32}
  \label{def:18}
  Semi-discrete solution on $D\times \Gamma$ to ($\tilde{WP_\Gamma}$) is a function $\tilde u_h \in L^2_p(\Gamma; W^h)$ such that $\tilde a_h(\tilde u_h, v) = \tilde l(v)$ for all $v \in L^2_p(\Gamma; V^h)$. Like before, the problem bas a unique solution. 

  We assume linear FEM but other discretizations are also possible. Since $L^2_p(\Gamma; V^h) \subset L^2_p(\Gamma, H^1_0(D)) = V$ and 
  \[\tilde a(\tilde u- \tilde u_h, v) = 0, \qquad \text{for all }  v\in L^2_p(\Gamma; V^h)\]
  and $\tilde u_h$ is the best approximation.
\end{def2}

\begin{satz}[Best approximation]
  \label{3.33}
  \label{thm:20}
  It holds 
  \[|\tilde u - \tilde u_h|_E = \inf_{v \in L^2_p(\Gamma; W^h)} |\tilde u-v|_E\]
  with
  \[|v|^2_E := \int_\Gamma p(y) \int_D \tilde a(x,y) |\nabla v(x,y)|^2 \dx{x}\dx{y}.\]
\end{satz}
Further we define
\[|v|^2_W := \int_\Gamma p(y) |v(\cdot,y)|_{H^1(D)}^2 \dx{x}\dx{y}.\]
We assume that there exists $k_2 > 0$ independent of $y \in \Gamma$ such that for $\tilde f \in L^2_p(\Gamma; L^2(D))$, $\tilde u \in L^2_p(\Gamma; H^2(D))$ and
\begin{equation}
  |\tilde u|_{L^2_p(\Gamma; H^2(D))} \leq k_2 \|\tilde f\|_{L^2_p(\Gamma; L^2(D)}
  \tag{23}
  \label{eqn:23}
\end{equation}
