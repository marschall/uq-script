\paragraph{Iterationstep resp. timestep}

Assume we have a $U^n\in\mathcal{M}_{\textbf{r}^n}$ and for example $\Delta^n=-h_n\nabla J(U^n)\in H_d$.
Then, an iteration step is given by 
\begin{equation}
  \label{iterstep}
  Y^n+1 = \underbrace{U^n}_{\in\mathcal\mathcal{M}_{\textbf{r}^n}} + \underbrace{\Delta^n}_{not \in\mathcal{M}_{\textbf{r}^n}} not \in \mathcal{M}_{\textbf{r}^n} \text{ in general.}
\end{equation}
Now, we are interested in a $U^{n+1}\in\mathcal{M}_{\textbf{r}^n}$, s.t. $U^{n+1}\approx Y^{n+1}$.
Using the Riemannian gradient we can use:
\begin{equation}
  Z^{n+1} 0 P_{\mathcal{T}_{U^n}}Y^{n+1} = U^n +P_{\mathcal{T}_{U^n}}\Delta^n = U^n+\zeta^n
\end{equation}
\begin{equation}
  U^{n+1} = \mathcal{R}(U^n,\zeta^n)\in\mathcal{M}_{\textbf{r}^n}
\end{equation}
where $\mathcal{R}$ is a retraction.

\begin{def2}
  We define an equivalence class
  \begin{equation}
    U^{n+1} tilde \tilde{U}^{n+1}
  \end{equation}
  if $\| U^{n+1} - \tilde{U}^{n+1}\| = \mathcal{O}(\|\zeta^n\|)$
\end{def2}
Let us define a tensor valued curve in $H_d$
\begin{def2}
  \begin{equation}
    X(t) 0 U^n+t\Delta^n \quad \textbf{, } X(0) = U^n \quad\textbf{, }X(1) = \Delta^n
  \end{equation}
\end{def2}
Then,
  \begin{equation}
    \frac{dX}{dt}(t) 0 \dot{X}(t)=\Delta^n \qquad \textbf{"Dirac Frenkl"}
  \end{equation}
  \begin{equation}
    \dot{V}(t) = P_{\mathcal{T}_{V(t)}}\underbrace{\Delta^n}_{P_\mathcal{T}}\quad\textbf{ , }V(0)=U^n
  \end{equation}
  The idea is now a Lie-Trotter Splitting approximation.
  \begin{equation}
    P_{U^n}\Delta^n = P_1\Delta^n + \dots + P_n\Delta^n
  \end{equation}
  \begin{Example}
    for $d=2$ we have $U=U^n=LSR^T=\tilde{L}R^T=L\tilde{R}^T$ for example with SVD and $R,L\in\mathcal{G}(r,n)$ where $\mathcal{G}$ is the orthogonal Grassman manifold, i.e. the columns of $L$ and $R$ are ONBs. Furthermore, $S\in\mathbb{R}^{n,r} $ is regular.
     Then we have 
     \begin{equation}
       V(t) = L(t)S(t)R(t) = \tilde{L}(t)R(t) = L(t)\tilde{R}(t).
     \end{equation}
     Differentiation yields
     \begin{equation}
       \dot{V} = \dot{L}SR + L\dot{S}R + LS\dot{R} = \dot{\tilde{L}}R+\tilde{L}\dot{R} = \dot{L}\tilde{R}+L\dot{\tilde{R}}
     \end{equation}
     This gives us the algorithm
     \begin{Algorithm}
       \begin{enumerate}
         \item $\tilde{V}(1) = \tilde{L}_1 R_0 = L_1S_1R_0$
         \item $S_1 -> \tilde{S}_0, \tilde{R}_0 = \tilde{S}_0R_0$
         \item $U_1=L_1\tilde{R}_1$ Lubich and Oseledets
       \end{enumerate}
     \end{Algorithm}
     Then,
     $U^n = \tilde{L}_0R_0$ yields $ \tilde{L}_1 = \tilde{L}_0 + \Delta^nR_0 = L_1\tilde{S_1}$ and
     $\tilde{S}_0 = \tilde{S}_1-L_1^T\Delta^nR_0$, $\tilde{R}_0=\tilde{S}_0R_0$, $\tilde{R}_1=\tilde{R}_0 + L_1^T\Delta^n$
     which is essentially the same as
     $\tilde{L}_1 = \tilde{L}_0+\Delta^nR_0$, $\tilde{L}_1 = L_1\tilde{S}_1$, $\tilde{R}_0=L_1^TU$. Then, $\tilde{R}_1=\tilde{R}_0+L_1^T\Delta^n$, $U_1=U^{n+1} = L_1\tilde{R}_1$.     
  \end{Example}
  
  As in our problem before we have $Y^{n+1} =U^n+\Delta^n$, $U^n=U_1^n\circ\dots\circ U_d^n$, $U_j^n:=U_1^{n}\circ\dots U_{j-1}^{n+1}\circ U_{j-1}\circ U_j^n\circ\dots\circ U_d^n$ and $U_{j+1}^{n+1} = U_1^{n+1}\circ\dots\circ \underbrace{(U_j^n+\zeta_j^n)}_{\tilde{U}_j{n+1}}\circ U_{j+1}^n\circ\dots$, where $\zeta_j^n = \tau^T(U_j^n)\Delta^n$
  
  Remember our notation
  \begin{align}
    \mathcal{T}_{U^n} &= P_1H\otimes\dots\otimes P_dH=T_1\otimes\dots\otimes T_n \\ &= span\{\mathcal{E}_1,\dots,\mathcal{E}_d\}
  \end{align}
  \begin{equation}
    \mathcal{E}_j=\{U_1\circ\dots\circ\delta U_j\circ\dots\circ U_d : \delta U_j\in\mathbb{R^{r_{j-1},n_j,r_j}} \text{Eichbedingung}\}\quad j=1,\dots,d, (j=d,\dots, 1)\textbf{ .}
  \end{equation}
  Then,
  \begin{equation}
    U_{j+1}^{n+1} = U_j^{n+1} + P_{\mathcal{E}_j}\Delta^n
  \end{equation}
  which is equivalent to the ALDI algorithm (alternating div. projections)(ADF)
  \begin{equation}
    U^{n+1} = ALS_{\frac{1}{2}}(U^n,U^n+\Delta^n)\approx argmin\{\|U^n+\Delta^n-V\|:V\in\mathcal{M}_{\textbf{r}}\}
  \end{equation}
  Denote that, the algorithm is faster than the ALS, even thought we need more iteration steps.
  
  \paragraph{Randomized SVD}
  The problem of the projection methods is, that we could be stuck in a stationary point, since then the projection onto the tangential space is always zero. Therefore, we try to find a posibility to avoid that.
  \begin{equation}
    U^{n+1} = U^n + HSVD\Delta^n
  \end{equation}
  but the HSVD is pretty expensive.
  
  Let $A=USV^T\in U_1\otimes U_2$ with $rank (A)=r$ and $Im(A) = Im(U) = span(\{u_j\}_j)$. Furthermore, take a uniform Matrix $Q=[q_1,\dots,q_r]$, where $q_i[k]\in\mathcal{N}(0,1)$. Then, $Im(AQ) = Im(A)$ almost surely. $Im(A) = U_1$ but $Im(Q) not \approx U_2$, BUT $Im(AQ)\approx U_1$.
  
  Let $T_1 = AQ=\tilde{U}_1R\in \mathbb{R}^{n,n}$, with
  \begin{equation}
    \|(I-\tilde{U}_1\tilde{U}_1^T)A\| \approx\leq C \|(I-U_1U_1^T=A\| (SVD)
  \end{equation}
  For further computation we can choose $T_1\in\mathbb{R}^{n,r+p}$. For simplicity choose $p=r$.
  The SVD can be obtained by $\hat{V}=\tilde{U}_1^TA=\tilde{V}R\approx U_2$. Then, $\tilde{S}=\tilde{U}_1A\tilde{V}_1$ and $A\approx \tilde{U}_1\tilde{S}\tilde{V}$.
  
   Then, we call this method: LIDL (hierarchical Lindenstrauss Transformation)
  \begin{equation}
  ALS_\frac{1}{2}(U^n+\Delta	^n, Q_1\circ\dots\circ Q_n, rang=2r)\approx HSVD_r(U^n+\Delta	^n)
  \end{equation}
  
