%Lecture 1 -- Thu Oct 15 
\section{Stochastic Processes}
\subsection{Random Walk and Brownian Motion}
\begin{example}[Random Walk]
  \label{1.1}
Let $(\Omega, \mathcal F, \mathbb P)$ be a probability space.  
We consider a sequence $(X_n)_{n\in \mathbb N_0}$ of random variables (r.v.) on ($\Omega, \mathcal F, \mathbb P)$ with values in $\mathbb Z$ defined by
\[\left\{\begin{array}{l}X_1 := 0, \\ X_{n+1} := X_n + \xi_{n}, \qquad n \in \mathbb N\end{array}\right.\]
where $(\xi_n)_{n\in \mathbb N}$ is in turn a sequence of i.i.d.\ r.v.\ with $\mathbb P(\xi_n = 1) = \mathbb P(\xi_n = -1) = \frac12$ for $n \in \mathbb N$. 

Then, for arbitrary $n \in \mathbb N$, we have $\mathbb{E}[X_n] = 0$, since also $\mathbb{E} [\xi_j] = 0$ for all $j \in \mathbb N$.
Moreover, $\mathbb E[\xi_i \xi_j] = \delta _{i,j}$ for arbitrary $i,j \in \mathbb N$, we have 
\begin{align*}
  \Cov [X_n, X_m] &:= \mathbb E[(X_n - \mathbb E[X_n]) (X_m - \mathbb E[X_m])] \\
                   &= \mathbb E[X_n X_m] = \mathbb E\left[\left( \sum^{n-1}_{j=1} \xi_{j} \right) \left(\sum^{m-1}_{k=1} \xi_{k}\right)\right] \\
                   &= \min \{n-1,m-1\}.  
\end{align*}

Now we define $X(t)$ to be the piecewise linear interpolation of $(X_n)_{n\in \mathbb N}$. 
This gives a set of random variables indexed by $t \in \mathbb R^+_0$. 
We will call $X(t)$ a \emph{stochastic process}.
\end{example}

\begin{definition}[Stochastic process]
  \label{1.2}
  Given a set $\mathcal T \subset \mathbb R$, a measurable space $(H,\mathcal H)$ and a probability space $(\Omega, \mathcal F, \mathbb P)$. A family $X = \{X(t) : t \in \mathcal T\}$ is called \emph{$H$-valued stochastic process} if for every $t \in \mathcal T$ the function $X(t)$ is $\mathcal F$-$\mathcal H$-measurable.
\end{definition}

\begin{definition}[Sample Path]
  \label{1.3}
  For a given $\omega \in \Omega$, the mapping defined by 
  \begin{equation}
   X(\cdot, \omega)\colon T \to H\text{, } t \mapsto X(t, \omega) 
  \end{equation} 
  is called \emph{sample path} of $X$. 
\end{definition}

\begin{remark}
  \label{1.4}
  Diffusion scaling of $X$ as in Example \ref{1.1} leads to a Brownian motion.
\end{remark}

\begin{example}[Distribution of a random walk]
  \label{1.5}
  With the notation from example \ref{1.1} let $p_{n,j} = \mathbb P(X_n = j)$, $j\in \mathbb Z$, $n\in \mathbb N_0$. 
  Then, we have for arbitrary $n \in \mathbb N$ and $j \in \mathbb Z$
  \[p_{n+1,j} = \frac12 (p_{n,j-1} + p_{n,j+1})\]
  as well as $p_{0,j} = \delta _{0,j}$. 

  Note that $p_{2n, 2k+1} = 0$ for arbitrary $n \in \mathbb N_0$ and $k \in \mathbb Z$.
\end{example}

\begin{lemma}
  \label{1.6}
  With the notation of examples \ref{1.1} and \ref{1.5} we have
  \begin{enumerate}[(i)]
    \item $\frac{1}{n} X_n \to 0$ $\mathbb P$-a.s.
    \item $\frac{1}{\sqrt{n}} X_n \xrightarrow{\text{Law}} \mathcal N(0,1)$.
  \end{enumerate}
\end{lemma}
\begin{proof}
  We have $X_n = \sum^{n-1}_{j=1} \xi_{j}$ with $\mathbb E[\xi_j] = 0$ and $\mathbb V[\xi_j] = 1$ for all $j \in \mathbb N$. 
  Since $(\xi_n)_{n\in \mathbb N}$ are i.i.d.\ Bernoulli RVs \ (centered around zero), the two statements are exactly the law of large numbers and the central limit theorem.
\end{proof}

The random walk is approximately Gaussian for $n$ large. 
We derive a sequence of stochastic procceses $Y_N = \{Y_N(t) : t \in \mathbb R^n\}$ for $N \in \mathbb N$ where $Y_N(t)$ is approximately Gaussian for large $N$.

Let again, $X$ be the linear interpolation of a random walk $(X_n)_{n\in \mathbb N}$. 
Now, we define \[Y_N (t) := \frac1{\sqrt{N}} X(tN) = \frac1{\sqrt{N}} X_{\lfloor tN \rfloor} + \frac{tN - \lfloor tN\rfloor}{\sqrt N} \xi_{\lfloor tN \rfloor}\text{.}\]
Then, $Y_N = \{Y_N(t) : t \in \mathbb R^+_0\}$ is a real-valued stochastic process for each $N\in \mathbb N$ with continuous sample paths.

\begin{lemma}
  \label{1.7}
  With $Y_N$ as above, we have for arbitrary $t \in \mathbb R^+_0$, $Y_N(t) \xrightarrow{\text{Law}} \mathcal N(0,t)$ as $N \to \infty$.
\end{lemma}
\begin{proof}
  The second term in the definition of $Y_N(t)$ converges to zero in probability, i.e.\ for every $\varepsilon > 0$
  \[\mathbb P\left( \left| \frac{tN - \lfloor tN\rfloor}{\sqrt{N}} \xi_{\lfloor tN \rfloor} \right| > \varepsilon\right) \xrightarrow{N \to \infty} 0.\]
  The first term in the definition converges to the desired distribution by Lemma \ref{1.6}. 
  Since convergence in probability implies convergence in distribution, we are already done.
\end{proof}

\begin{remark}
  \label{1.8}
  For every $t\in \mathbb R^+_0$ we have $Y_N(t) \in L^2(\Omega)$ for each $N \in \mathbb N$. However, the sequence $(Y_N(t))_{N\in \mathbb N}$ is not Cauchy, i.e. it has no limit in $L^2(\Omega)$.
\end{remark}

\begin{definition}[Finite-Dimensional Distributions]
  \label{1.9}
  Let $X = \{X(t) : t \in \mathcal T\}$ be a real-valued stochastic process. For $t_1,\ldots,t_M \in \mathcal T$ we define
  \[\mathbf X := (X(t_1),\ldots,X(t_M))^T.\]
  Then $\mathbf X$ is a $\mathbb R^M$-valued r.v.\ and the probability distribution $\mathbb P_{\mathbf X}$ on $(\mathbb R^M, \mathcal{B}(\mathbb R^M))$ is known as the \emph{finite-dimensional distribution} of $X$ at $t_1,\ldots,t_M$.
\end{definition}

\begin{lemma}
  \label{1.10}
  For the diffusion scaled random walk $Y_N$ as above and $\mathbf Y_N := (Y_N(t_1),\ldots, Y_N(t_M))^T$ a finite-dimensional distribution we have  
  $\mathbf Y_N \xrightarrow{\text{Law}} \mathcal N(0,C)$ as $N \to \infty$, where $C \in \mathbb R^{M\times M}$ with entries $c_{i,j} = \min\{t_i, t_j\}$. 
\end{lemma}

\begin{definition}[Second Order Process]
  \label{1.11}
  A stochastic process $X = \{X(t) : t\in \mathcal T\}$ is said to be of \emph{second order}, if $X(t) \in L^2(\Omega)$ for all $t \in \mathcal T$. 

  If $X$ is of second order, we define the \emph{mean function} 
  \begin{equation}
    \mu : t \mapsto \mathbb E[X(t)]
  \end{equation}
   and the \emph{covariance function} 
  \begin{equation}
     C : (s,t) \mapsto \Cov(X(s), X(t))
  \end{equation}     
  of $X$.

\end{definition}
\begin{definition}[Real-valued Gaussian Process]
  \label{1.12}
  A real-valued stochastic proccess $X$ is called \emph{Gaussian process}, if all its finite-dimensional distributions are multivariate Gaussian distributions.
\end{definition}

\begin{definition}[Brownsche Bewegung]
  \label{1.13}
  The processes $W = \{W(t) : t \in \mathbb R^+_0\}$ is called \emph{Brownian motion}, if it is a real-valued Gaussian process with continuous sample paths, mean function $\mu(t) = 0$ and covariance function $C(s,t) = \min\{s,t\}$. 
\end{definition}

\begin{remark}
  \label{1.14}
  \begin{itemize}
    \item The Browniam motion $W$ can be understood as limiting process of the rescaled linear interpolants $Y_N$.
    \item As with a random walk, the increments $W(t) - W(s)$ over disjoint intervals are independent. 
      In particular, for $p \leq r \leq s \leq t$ we find 
	\begin{equation}
	  \Cov[W(r) - W(p), W(t) - W(s)] = 0\text{.}
	\end{equation}	      
       Since $W$ is a Gaussian process, for which correlation zero  already  shows independence, we showed that increments of the Brownian motion are independent.

      We can further calculate $\mathbb V[W(t) - W(s)] = |t-s|$, which implies that $W(t) - W(s) \sim \mathcal N(0,|t-s|)$.
    \item An alternative definition of the Brownian motion is: $W = \{W(t) : t \in \mathbb R^+_0\}$ is a real valued stochastic procces with 
      \begin{enumerate}[(i)]
        \item $W(0) = 0$ a.s.
        \item $W(t) - W(s) \sim \mathcal N(0,|t-s|)$ for all $t ,s \in \mathbb R^+_0$,
        \item for $p,r,s,t \in \mathbb R$ with $p\leq r\leq s\leq t$ we have $W(r) - W(p)$ and $W(t)-W(s)$ are independent
        \item The paths of $W$ are almost surely continuous.
      \end{enumerate}
  \end{itemize}
\end{remark}

\begin{remark}[Relation of the random walk and the heat equation]
  \label{1.15}
  Rescale the random walk $(X_n)$ to $(Y_n) = \left(\frac1{\sqrt{N}} X_n\right)$ for some $N \in \mathbb N$. On the lattice $(t_n, x_n) = (n \Delta t_j , \Delta x)$, $j \in \mathbb Z$, $n \in \mathbb N_0$ with $\Delta t = \Delta x^2 = \frac1N$. 
  As above, we have 
  $q_{n,j} = \mathbb P(Y_n = j)$ satisfies
  \[q_{n+1,j} := \frac12 (q_{n,j-1} + q_{n,j+1})\]
  and thus
  \[\frac{q_{n+1,j} - q_{n,j}}{\Delta t} = \frac12 \frac{q_{n,j-1} - 2 q_{n,j} + q_{n,j+1}}{\Delta x^2}.\]
  This is equivalent to the finite difference approximation of the heat equation
  \begin{equation}
    \frac{\partial u}{\partial t} = \frac12 \frac{\partial ^2 u}{\partial x^2}, \qquad x\in \overline{\mathbb R}, t \in \mathbb R^+_0
    \label{eqn:diffusionProblem}
    \tag{5}
  \end{equation}
  for initial conditions $u(0,x) = \phi(x)$, $\phi : \mathbb R \rightarrow \mathbb R$. 

  We define 
  \[U_{n,j} := \mathbb E[\phi(x_j + Y_n)] = \sum_{k\in \mathbb Z} \phi(x_j + x_k) q_{n,k}\]
  By the above definition of $q_{n,j}$, we have
  \begin{multline*}
    U_{n+1,j} = \sum_{k\in \mathbb Z} \phi(x_j + x_k) q_{n+1, k} = \sum_{k\in\mathbb Z} \phi_{x_j + x_k} \cdot \frac12 (q_{n,k-1} + q_{n,k+1}) \\
    = \frac12 \sum_{k\in \mathbb Z} (\phi(x_j + x_{k+1}) + \phi(x_j + x_{k-1}) ) q_{n,k} = \frac12 (U_{n,j+1} + U_{n,j-1}) 
  \end{multline*}

  This shows
  \begin{equation}
    \frac{U_{n+1,j} - U_{n,j}}{\Delta t} = \frac12 \frac{U_{n,j-1} - 2 U_{n,j} + U_{n,j+1}}{\Delta x^2}
    \label{eqn:discreteDiffusion}
    \tag{6}
  \end{equation}
  with starting value $U_{0,j} = \phi(x_j)$.
  
  
%Lecture 2 -- Tue Oct 20

  The finite differences (FD) discretization of \eqref{eqn:diffusionProblem} with initial condition $u(0,x) = \phi(x)$ on the other hand is given by
  \begin{equation}
    \frac{u_{n+1,j} - u_{n,j}}{\Delta t} = \frac12 \frac{u_{n,j-1} - 2 u_{n,j} + u_{n,j+1}}{\Delta x^2}, \qquad u_{0,j} = \phi(x_j)
    \label{eqn:diffusionFDdiscretization}
    \tag{7}
  \end{equation}

  Since in \eqref{eqn:discreteDiffusion} and \eqref{eqn:diffusionFDdiscretization} update rules and initial conditions are the same, the FD approximation $u_{n,j}$ and the $U_{n,j}$ of the random walk are equivalent.
\end{remark}

\begin{theorem}
  \label{1.16}
  For continuous bounded initial data $\phi$, the FD approximation $u_{n,j}$ defined by \eqref{eqn:diffusionFDdiscretization} converges to the solution of \eqref{eqn:diffusionProblem} as $\Delta t = \Delta x^2 \to 0$. 
\end{theorem}
\begin{proof}
  Recall that $Y_n := Y_n(t_n)$, then $Y_n \xrightarrow{\text{Law}} \mathcal N(0,t_n)$ by Lemma \ref{1.10}. 
  Hence, $x_j + Y_n \xrightarrow{\text{Law}} \mathcal N(x_j, t_n)$ as $N\to \infty$ with $(t_n, x_j)$ fixed. 

  Equivalently, 
  \[U_{n,j} = \mathbb E[\phi(x_j + Y_n)] \to \int_\mathbb R \phi(x) \cdot \frac1{\sqrt{2\pi t_n}} e^{- \frac{(x-x_j)^2}{2t_n}} \dx{x}.\]
  It is well known, that the exact solution of \eqref{eqn:diffusionProblem} is given by
  \[u(t,x) = \frac1{\sqrt{2\pi t}} \int_\mathbb R e^{-\frac{(x-y)^2}{2t}} \phi(y) \dx{y}.\]
  Since, $U_{n,j} = u_{n,j}$, this yields $u_{n,j} \to u(t_n, x_j)$ for $N \to \infty$ and fixed $(t_n,x_j)$. 
\end{proof}

\begin{remark}
  \label{1.17}
  \begin{itemize}
    \item The distribution of a real-valued multivariate Gaussian r.v.\ is uniquely determined by its mean vector $\mu$ and covariance matrix $C$. 
    Here $C$ must be symmetric and non-negative definite.
    \item Similarly for a Gaussian process $X = \{X(t) : t \in \mathcal T\}$, the covariance function $C : \mathcal T \times \mathcal T \rightarrow \mathbb R$ must be symmetric and non-negative definite, i.e.\ for arbitrary $N \in \mathbb N$, $t_1,\ldots,t_N \in \mathcal T$ and $a_1,\ldots,a_N \in \mathbb R$ we always have 
      \[\sum^N_{j,k = 1} a_j C(t_j, t_k) a_k \geq 0.\]
    \item Let $\mathbb R^{\mathcal T}$ be the set of all functions $f : \mathcal T \rightarrow \mathbb R$ and $\mathcal{B}(\mathbb R^{\mathcal T})$ the smallest $\sigma$-field containing all open sets of $\mathbb R^{\mathcal T}$. For $t_1,\ldots,t_N \in \mathcal T$ and $F \in \mathcal{B}(\mathbb R^{\mathcal T})$, we define
      \[B := \{f \in \mathbb R^{\mathcal T} : (f(t_1), \ldots, f(t_N))^T \in F\}.\]
      Note that Definition \ref{1.3} implies that the sample paths $X(\cdot,\omega)$ of a real-valued stochastic process $X = \{X(t) : t\in \mathcal T\}$ belong to $\mathbb R^{\mathcal T}$. Moreover, the sample paths define a $\mathbb R^{\mathcal T}$-valued r.v.
  \end{itemize}
\end{remark}

%\begin{lemma}
%  \label{1.18}
%  Since $\omega \mapsto X(\cdot,\omega)$ is $(\Omega,\mathcal F)$-$(\mathbb R^\mathcal T, \mathcal{B}(\mathbb %R^\mathcal T))$-measurable, the sample paths
%\end{lemma}

\begin{definition}[Independet processes, sample paths]
  \label{1.19}
  \begin{enumerate}[(i)]
    \item Two real-valued stochastic processes $X = \{X(t) : t\in \mathcal T\}$ and $Y = \{Y(t) : t \in \mathcal T\}$ are called \emph{independent} if the associated $(\mathbb R^{\mathcal T}, \mathcal{B}(\mathbb R^\mathcal T))$-valued r.v.\ are independent, i.e.\
      \[\mathbf X = (X(t_1),\ldots,X(t_N))^T \quad \text{and} \quad \mathbf Y = (Y(s_1),\ldots,Y(s_N))^T\]
      are independent $\mathbb R^N$-valued r.v.\ for arbitrary $t_1,\ldots,t_N, s_1,\ldots,s_N \in \mathcal T$. 
    \item The functions $f : \mathcal T \rightarrow \mathbb R$, $i\in \{1,2\}$, are inpendent sample paths of a real-valued processes $X = \{X(t) : t \in \mathcal T\}$ if $f_i(t) = X_i(t,\omega)$ for some $\omega \in \Omega$, where $X_i$ are i.i.d.\ processes with the same distribution as $X$.
  \end{enumerate}
\end{definition}

\begin{theorem}[Daniel-Kolmogorov]
  \label{1.20}
  Let $\mathcal T \subset \mathbb R$. The following statements are equivalent.
  \begin{enumerate}[(i)]
    \item There exists a real-valued second order stochastic processes with mean function $\mu$ and Covariance function $C(s,t)$.
    \item $\mu : \mathcal T \rightarrow \mathbb R$, $C : \mathcal T \times \mathcal T \rightarrow \mathbb R$ with $C$ symmetric and non-negative definite.
  \end{enumerate}
\end{theorem}

\begin{corollary}
  \label{1.21}
  The probability distribution $\mathbb P_X$ on $(\mathbb R^\mathcal T, \mathcal{B}(\mathbb R^\mathcal T))$ of a real-valued Gaussian processes is uniquely determined by its mean function $\mu$ and covariance function $C$.
\end{corollary}

\begin{example}[Cosine Covariance]
  \label{1.22}
  Let $C(s,t) = \cos(s-t)$ and $\mu \equiv 0$. Obviously $C$ is symmetric and it is easy to show that for $t_1,\ldots,t_N \in \mathcal T = \mathbb R$, $a_1,\ldots,a_n \in \mathbb R$
  \[\sum^N_{j,k=1} a_j \cos(t_j - t_k) a_k = \left| \sum_{j=1}^N a_j \cos(t_j) \right|^2 + \left| \sum^N_{j=1} a_j \sin(t_j)\right|^2 \geq 0.\]
  In fact, $X(t) = \xi_i \cos(t) + \xi_2 \sin(t)$ with $\xi_1,\xi_2 \overset{\text{i.i.d.}}{\sim} \mathcal N(0,1)$ is the Gaussian processes defined by $\mu$ and $C$ above.
\end{example}

For the existance of a \emph{Brownian Motion} we need the following
\begin{lemma}
  \label{1.23}
  $C : \mathbb R^+_0 \times \mathbb R^+_0 \rightarrow \mathbb R, (s,t) \mapsto \min \{s,t\}$ is symmetric and non-negative definite.
\end{lemma}

\subsection{Conditional Expectation for Square Integrable $H$-valued r.v.}
\begin{definition}[$L^2(\Omega, \mathcal F, \mathbb P)$]
  \label{1.24}
  For a Hilbert space $(H,\left<\cdot,\cdot\right>_H, \|\cdot\|_H)$, the space of $\mathcal F$-measurable r.v. from $(\Omega, \mathcal F, \mathbb P)$ to $(H, \mathcal{B}(H))$ with $\mathbb E[\|X\|^2] < \infty$ is denoted by $L^2(\Omega, \mathcal F, \mathbb P; H) =: L^2(\Omega ; H)$ and forms a Hilbert space with the inner product
  \[\langle X,Y\rangle_{L^2(\Omega; H)} = \mathbb E\left[\langle X,Y\rangle_H\right]\]
  for $X, Y \in L^2(\Omega; H)$.
\end{definition}

\begin{definition}[Conditional Expectation Given a $\sigma$-field]
  \label{1.25}
  Let $X \in L^2(\Omega; H)$ and $\mathcal A$ a sub-$\sigma$-field of $\mathcal F$. Then, the \textbf{conditional expectation} of $X$ given $\mathcal A$, denoted by $\mathbb E[X |\mathcal A]$, is defined as orthogonal projection of $X$ onto the space $L^2(\Omega, \mathcal A; H) \subset L^2(\Omega, \mathcal F; H)$.
\end{definition}

Consider a Hilbert space $\Psi$ and a $\sigma$-field $\mathcal G$. The probability distribution $\mathbb P_X$ of a $\Psi$-valued r.v.\ $X$ is 
\[\mathbb P_X (G) = \mathbb P(\{\omega \in \Omega : X(\omega) \in G\}) = \mathbb E[\mathbbm 1_G(X)], \qquad \text{for }G \in \mathcal G.\]
Now we can use the definition of conditional expectations to define also conditional probability distributions. 

\begin{definition}[Conditional Probability]
  \label{1.26}
  For a $\Psi$-valued r.v.\ $X$ on $(\Omega, \mathcal F, \mathbb P)$ and $\mathcal A$ a sub-$\sigma$-field of $\mathcal F$ we define 
  \[\mathbb P_X [G | \mathcal A] := \mathbb E[ \mathbbm 1_G(X) | \mathcal A].\]
  Then $\mathbb P_X [G|\mathcal A]$ is a $[0,1]$-valued r.v.\ and $G \mapsto \mathbb P_X [G | \mathcal A]$ is a measure-valued r.v.
\end{definition}

For $H$-valued r.v.\ $Y$ and $y \in H$, define 
\[\mathbb P_X [G | Y = y] := \mathbb P[X \in G | Y = y] = \mathbb E[\mathbbm 1_G(X) | Y = y].\footnotemark\]
\footnotetext{More precisely, the definition is based on the following 
  \begin{lemma}
A r.v. $Y$ is $\sigma(X)$-measurable if and only if there exists a measurable function $f$ such that $Y = f(X)$. This function is $\mathbb P_X$-almost surely unique.
  \end{lemma}
Hence there exists a unique function $f$ such that $\mathbb E[Y | X] := \mathbb E[Y | \sigma(X)] = f(X)$ $\mathbb P$-a.s. Now we simply define symbolically $\mathbb E[Y | X = x] = f(x)$.}
The map $G \mapsto \mathbb P_X [G | Y = y]$ is a measure on $(\Psi, \mathcal G)$. Then $\mathbb P_X[G | Y = y]$ is called conditional probability that $X \in G$ given $Y = y$. 

\begin{example}[Brownian Bridge]
  \label{1.27}
  Let $\mathcal T = [0,T]$ for some fixed $T \in \mathbb R^+$. We want to define a new stochastic process, the Brownian bridge (\emph{Bb}), which behaves like a standard Brownian motion restricted to $\mathcal T$, except for the fact that we impose that its final point shall be equal to a beforehand fixed value $b \in \mathbb R$. 

  We realise this by conditioning $W$ to $W(0) = 0$ (as it is already defined) and $W(T) = b$. Hence the finite-dimensional distribution of a \emph{Bb} $B$ at $t_1,\ldots,t_N \in \mathcal T$ are given by
  \[\mathbb P\left[\left(\begin{array}{c}B(t_1)\\ \vdots \\ B(t_N)\end{array}\right) \in F\right] = \mathbb E\left[\mathbbm 1_F \left(\begin{array}{c}W(t_1)\\\vdots\\W(t_N)\end{array}\right) \middle| W(T) = b\right],\]
  i.e. for measurable $\phi : \mathbb R^N \rightarrow \mathbb R$ we have 
  \[\mathbb E[\phi(B(t_1),\ldots,B(t_N))] = \mathbb E[\phi(W(t_1),\ldots,W(t_n))| W(T) = b].\]

  From a more indepth study of conditional expression and the resulting disintegration theorem, one sees that a thusly defined process is again Gaussian and we can calculate the mean and covariance functions of the \emph{Bb} as
  $\mu(t) = \mathbb E[W(t) | W(T) = b]$ and $C(s,t) = \mathbb E[(W(s) - \mu(s)) (W(t) - \mu(t)|W(T) = b]$.
\end{example}

\begin{lemma}[Standard Brownian Bridge]
  \label{1.28}
  The stochastic process $B = \{B (t) : t \in [0,1]\}$ with $B(0) = 0 = B(1)$ is a Gaussian process 
  with mean function $\mu \equiv 0$ and $C : (s,t) \mapsto \min\{s,t\} - s\cdot t$.
\end{lemma}
%Lecture 3 -- Thu Oct 22 11:31:57 CEST 2015
\subsection{White and Coloured Noise}
\begin{remark}[White Noise]
  \label{1.29}
  Let $\mathcal T = [0,1]$ and $(\phi_n)_{n\in \mathbb N}$ be a orthonormal basis of $L^2(\mathcal T)$. For instance 
  \begin{equation}
    \phi_j(t) = \sqrt{2} \sin(j\pi t)\text{.}
    \tag{$*$}
    \label{eqn:basisFkt}
  \end{equation}
  Consider the stochastic process $\zeta$ (the \textbf{wite noise}) given by
  \begin{equation}
    \zeta(t) = \sum^\infty_{j=1} \xi_j \phi_j(t)
    \tag{$**$}
    \label{eqn:summingProcess}
  \end{equation}
  with $(\xi_j)_{j\in \mathbb N}$ a sequence of i.i.d.\ $\mathcal N(0,1)$-r.v.
  By definition, $\zeta$ has mean function $\mu \equiv 0$ and the covariace
  \[C(s,t) = \Cov[\zeta(s), \zeta(t)] = \sum^\infty _{j,k =1} \Cov[\zeta_j, \zeta_k] \phi_j(s) \phi_k(t) = \sum^\infty _{j,k=1}\phi_j(s) \phi_k(t).\]
  For the basis \eqref{eqn:basisFkt}, we get $C(s,t) = \delta(s-t)$. Hence, for $s \neq t$, the covariance vanishes, i.e. the process is self-uncorrelated and for $s=t$ we have
  \[C(t,t) = \mathbb V[\zeta(t)] = \mathbb E[\zeta(t)^2] =\delta (0) = \infty.\]
  In particular, the process $\zeta$ does not converge and is not in $L^2(\mathcal T)$.
\end{remark}

\begin{remark}[Coloured Noise]
  \label{1.30}
  With the notation from Remark \eqref{1.29} define a processes $X$ on $\mathcal T$ via
  \[X(t) = \sum^\infty _{j=1} \sqrt{\nu_j} \xi_j \phi_i(t).\]
  If $\nu_j$ vary with $j \in \mathbb N$, then $X(t)$ is called \textbf{coulered noise} and the random variables $X(t)$ and $X(s)$ are correlated.
\end{remark}

\subsection{Karhunen-Lo\`eve expansion}

\begin{remark}[Matrix Spectral Decomposition, Discrete KLE]
  \label{1.31}
  Let $X = \{X(t) : t \in \mathcal T\}$ a real-valued Gaussian processes with mean function $\mu$ and covariance function $C$. For $t_1,\ldots,t_N \in \mathcal T$ define 
  \begin{equation}
    \mathbf X = (X(t_1),\ldots,X(t_N))^T \sim \mathcal N(\mathbf{\mu}, C_N)
  \end{equation}
  with $\mathbf{\mu} = (\mu(t_1),\ldots,\mu(t_N))^T$ and $C_N \in \mathbb R^{N\times N}$ with entries $c_{i,j} = C(t_i,t_j)$. 
  Samples of $X$ can be generated with $C_N = V^TV$ (Cholesky decomposition of $C_N$) and 
  \begin{equation}
    X = \mathbf{\mu} + V^T \mathbf{\xi}
    \tag{8}
    \label{eqn:8}
  \end{equation}
  with $\mathbf{\xi} = (\xi_1,\ldots,\xi_N)^T \sim \mathcal N(0,I_N)$ which can be generated as $N$ i.i.d.\ normal r.v.\ (e.g.\ via Box-Miller).
\end{remark}

\begin{theorem}[Spectral Decomposition]
  \label{1.32}
  Every symmetric $A \in \mathbb R^{N\times N}$ can be decomposed as $A = U \Sigma U^T$ where $U$ is orthonormal with columns $u_j$, which are eigenvectors of $A$ and $\Sigma = \operatorname{diag}(\nu_1,\ldots,\nu_N)$ the associated eigenvalues of $A$.
\end{theorem}

\begin{remark}
  \label{1.33}
  \begin{itemize}
    \item The advantage of the spectral decomposition is, that one can use the singular value decomposition, which is more robust than the Cholesky decomposition.
    \item Since the covariance matrix $C_N$ in Remark \ref{1.31} is square, real-valued and symmetric we have $C_N = U \Sigma U^T$ and since $C_N$ is non-negative definite, we can order the eigenvalues of $C_N$ as $\nu_1 \geq \nu_2 \geq \ldots \geq \nu_N \geq 0$. Let now
      \[X = \mathbf{\mu} + \sum^N_{j=1} \sqrt{\nu_j} u_j \xi_j,\]
      with $\xi_j \overset{\text{i.i.d.}}{\sim} \mathcal N(0,1)$. 
    \item Efficiency gained by truncation of spectral decomposition: In the decomposition $C_N = \sum^N_{j=1} \nu_j u_j u_j^T$ the first terms (those with biggest eigenvalues) contribute most. For $n < N$ define $\Sigma_n := \operatorname{diag}(\nu_1,\ldots,\nu_n)$ and $U_n = (u_1,\ldots,u_n) \in \mathbb R^{n \times N}$. Then we define the truncated spectral decomposition
      \[C_{N,n} = U_n \Sigma_n U_n^T = \sum_{j=1}^n \nu_j u_j u_j^T.\]
      The approiximation error yields
      \[\| C_N - C_{N,n} \|_{L(\mathbb R^N)} = \sup_{x \neq 0} \frac{\|(C_N - C_{N,n})x \|_2}{\|x\|_2} = \left\| \sum^N_{j=n+1} \nu_j u_j u_j^T\right\|_{L(\mathbb R^N)} = \nu_{n+1}.\]
    \item The truncation of \eqref{eqn:8} gives 
      \[\hat X = \mu + U_n \Sigma_n^\frac12 \mathbf{\xi} = \mu + \sum^n_{j=1} \sqrt{\nu_j} u_j \xi_j\]
      with $\mathbf{\xi} = (\xi_1,\ldots,\xi_n) \sim \mathcal N(0,I_n)$, i.e. $\hat X \sim \mathcal N(\mathbf{\mu}, C_{N,n})$. If $X$ is defined by \eqref{eqn:8}, then we have
      \[\mathbb E\left[\left\|X - \hat X\right\|^2_2\right] = \mathbb E\left[\sum^N_{j=n+1} \sum^N_{k=n+1} \sqrt{\nu_j \nu _k} u_j^T u_k \xi_j \xi_k\right] = \sum^N_{j=n+1} \nu_j.\]
      The error in approximation averages with respect to test functions $\phi \in L^2(\mathbb R^N)$ satisfies
      \[\left|\mathbb E[\phi(X)] - \mathbb E\left[\phi\left(\hat X\right)\right]\right| \leq k \|\phi\|_{L^2} \|C_N - C_{N,n}\|_F^2 \leq k \|\phi\|_{L^2} \sum^N_{j=n+1} \nu_j^2.\]
    \item The decay of the eigenvalues of $C_N$ determines the approximability by few terms.
    \item Trunctation is a method for model order reduction: Approximating a $N$-dimensional Gaussian r.v.\ $X$ is approximated by an $n$ ($< N$) uncorrelated $\mathcal N(0,1)$-r.v.
  \end{itemize}
\end{remark}

\begin{remark}[Karhunen-Lo\`eve Expansion]
  \label{1.34}
  A generalization of \ref{1.31} to \ref{1.33} to stachastic processes $X = \{X(t) : t \in \mathcal T\}$ is called KLE.  Let $\mu(t) = \mathbb E[X(t)]$ and consider $Y(t) = X(t) - \mu(t)$ (the centering of $X$). 

  Our aim is to sample paths in an orthonormal basis $(\phi_j)_{j\in \mathbb N}$ of $L^2(\mathcal T)$, i.e.
  \[X(t,\omega) - \mu(t) = \sum^\infty _{j=1} \gamma_j(\omega) \phi_j(t)\]
  with r.v.\ $\gamma_j(\omega) := \left<X(t,\omega) - \mu(t), \phi_j(t)\right>_{L^2(\mathcal T)}$. In the KLE we chose $(\phi_j)_{j\in \mathbb N}$ as the eigenfunctions of the integral operator $\mathcal C$, which is defined by 
  \[(\mathcal Cf)(t) := \int_\mathcal T C(s,t) f(s) \dx{t}, \qquad \text{for } f \in L^2(\mathcal T).\]
\end{remark}

\begin{lemma}
  \label{1.35}
  Let $X \in L^2(\Omega; L^2(\mathcal T))$. Then, $\mu \in L^2(\mathcal T)$ and the sample paths $X(t,\omega) \in L^2(\mathcal T)$ a.s.\ .
\end{lemma}

\begin{proof}
  By assumption $\|X\|_{L^2(\Omega; L^2(\mathcal T))} = \mathbb E[\|X\|^2_{L^2(\mathcal T)}] < \infty$ and $\|X(t,\omega)\|_{L^2(\mathcal T)} < \infty$ a.s.\ $w \in \Omega$ and sample paths $X(\cdot,\omega) \in L^2(\mathcal T)$ a.s.
  Jensen's inequality gives $\mu(t) ^2 = \mathbb E[X(t)]^2 \leq \mathbb E[X(t)^2]$ and hence 
  \[\|\mu\|^2_{L^2(\mathcal T)} = \int_\mathcal T \mu(t) \dx{t} \leq \int_\mathcal T \mathbb E[X(t)^2] \dx{t} = \|X\|^2_{L^2(\Omega; L^2(\mathcal T))} < \infty.\] 
\end{proof}

\begin{theorem}[$L^2$-Convergence of the KLE]
  \label{1.36}
  Consider $X = \{X(t) : t \in \mathcal T\}$ and suppose $X \in L^2(\Omega; L^2(\mathcal T))$. Then
  \begin{equation}
    X(t,\omega) = \mu(t) + \sum^\infty_{j=1} \sqrt{\nu_j} \phi_j(t) \xi_j(\omega)
    \tag{10}
    \label{eqn:10}
  \end{equation}
  converges in $L^2(\Omega;L^2(\mathcal T))$, with 
  \[\xi_j (\omega) = \frac{1}{\sqrt{\nu_j}} \left<X(t,\omega) - \mu(t), \phi_j(t)\right>_{L^2(\mathcal T)},\]
  and $\{\nu_j, \phi_j\}_{j\in \mathbb N}$ denotes eigenpairs of the integraloperator $\mathcal C$ with kernel function $C$, the covariance function of $X$, with $\nu_i \geq \nu_{n+1} \geq 0$ for all $n \in \mathbb N$. The $\xi_n$ have mean zero, variance one and are pairwise uncorrelated. If $X$ is a Gaussian process, we have $\xi_j \sim \mathcal N(0,1)$.
\end{theorem}

\begin{theorem}[Uniform Convergence of the KLE]
  \label{1.37}
  Consider a real valued stochastic process $X \in L^2(\Omega; L^2(\mathcal T))$ and let $X_J (t,\omega)$ be defined as
  \[X_J (t,\omega) := \mu(t) + \sum^J_{j =1} \sqrt{\nu_j} \phi_j(t) \xi_j(\omega).\]
  If $\mathcal T\subset \mathbb R$ is a compact set and the covariance function $C$ of $X$ belons to $C(\mathcal T \times \mathcal T)$, then $\phi_j \in C(\mathcal T)$ and the expansion of $C$ converges uniformly, i.e.
  \[\sup_{s,t \in \mathcal T} |C(s,t) - C_J(s,t)| \leq \sup_{t\in \mathcal T} \sum^\infty _{j=J+1} \nu_j \phi_j(t) ^2 \to 0, \qquad \text{for } J \to \infty,\]
  where $C_J= \sum^J_{j=1} \nu_j \phi_j(t) \phi_j(s)$ is the covariance function of $X_J$ and 
  \[\mathbb E[|X(t) - X_J(t)|^2] \to 0, \qquad \text{for } J\to \infty.\]
\end{theorem}

\subsection{Regularity of Stochastic Processes}

\begin{definition}[Mean-Square Continuity]
  \label{1.38}
  A stochastic process $X = \{X(t) : t \in \mathcal T\}$ is \textbf{mean-square continuous}, if, for all $t\in \mathcal T$, 
  \[\mathbb E\left[(X(t+h) - X(t))^2\right] \to 0 \qquad \text{as } h \to 0.\]
\end{definition}

\begin{theorem}
  \label{1.39}
  Let $X = \{X(t) : t \in \mathcal T\}$ be a centered processes. The covariance function $C$ of $X$ is continuous at $(t,t) \in \mathcal T \times \mathcal T$ if and only if $X$ is mean-square continuous in $t$.
  In particular, if $C \in C(\mathcal T\times \mathcal T)$, then $X$ is mean-square continuous everywhere.
\end{theorem}

\begin{definition}[Mean-Square Derivative]
  \label{1.40}
  A stochastic process $X = \{X(t) : t \in \mathcal T\}$ is \textbf{mean-square differentiable} with mean-square derivative $\frac{\dx{X(t)}}{\dx{t}}$, for all $t \in \mathcal T$, if 
  \[ \mathbb E\left[\left| \frac{X(t+h) - X(t)}{h} - \frac{\dx{X(t)}}{\dx{t}} \right|^2 \right]^\frac12 \to 0, \qquad \text{as } h \to 0.\]
\end{definition}

\begin{theorem}
  \label{1.41}
  Given a centered stochastic-process $X$ with covariance function $C \in C^2(\mathcal T,\mathcal T)$. 
  Then, $X$ is mean-square differentiable in any $t \in \mathcal T$ and the derivative $\frac{\dx{X(t)}}{\dx{t}}$ has the covariance function $\frac{\partial ^2 C(s,t)}{\partial s \partial t}$.
\end{theorem}

\begin{remark}
  \label{1.42}
  Analogously, if it exist, the $r$-th mean-square derivative has covariance function $\frac{\partial ^{2r} C(s,t)}{\partial s^r \partial t^r}$.
\end{remark}

\begin{definition}[Stationary Process]
  \label{1.43}
  A stochastic processes $X$ is called \textbf{(weakly) stationary}, if it has constant mean function $\mu(t) \equiv \mu$ and its covariance function $C(s,t)$ depends only on the difference $s -t$, i.e. $C(s,t) = C(s-t,0) = c(s-t)$ for some so called stationary covariance function $c(t)$.
\end{definition}

\begin{remark}
  \label{1.44}
  \begin{itemize}
    \item The distribution of a stationary Gaussian process is invariant under translation in $t$.
    \item Examples are centered Gaussian processes with $C(s,t) = \delta (s-t)$, $\cos(s-t)$ or the exponential covariance function $e^{-|s-t|}$.
  \end{itemize}
\end{remark}

