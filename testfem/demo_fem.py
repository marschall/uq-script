from __future__ import division
import numpy as np
from functools import partial
from plothelper import PlotHelper
from testfield import TestField
from fem_utils import setup_FEM, evaluate_u
from dolfin import set_log_level, WARNING, Function

set_log_level(WARNING)

mean = 0                                            # mean value of the affine field
expfield = True                                     # switch to go to the log-normal field
skip_freq = 5
field = TestField(mean=mean, expfield=expfield, skip_freq=skip_freq)     # create field object
N = 100    	                                    # define mesh resolution
FEM = setup_FEM(N)                                  # setup variational formulation and boundary conditions
eval_u = partial(evaluate_u, FEM=FEM, field=field)  # create callable function object from FEM solution

#                                                   # evaluate sample solution
K, M = 5, 20  	                                    # number samples, KL modes

u_ref, a_ref = eval_u([[0] * M])                    # evaluate reference, i.e. u[x, 0, ... , 0]

ph = PlotHelper()                                   # create plothelper object for simple fenics function plots
U_ref = Function(FEM["V"])                          # create Function for reference solution
A_ref = Function(FEM["V"])                          # create Function for reference field
U_ref.vector()[:] = u_ref[0]                        # store coefficients for reference solution
A_ref.vector()[:] = a_ref[0]                        # store coefficients for reference field (should be constant 1)
# ph["u_ref"].plot(U_ref)                           # plot reference solution
# ph["a_ref"].plot(A_ref, interactive=True)         # plot reference field and wait for input

for i in range(K):                                  # loop through number of samples
    y = 2 * np.random.rand(M) - 1                   # uniform realization of y in [-1,1]
    uy, ay  = eval_u([y])                           # evaluate solution at y
    #                                               # create Fenics Function on the given FunctionSpace
    Uy, Uy_dev = Function(FEM["V"]), Function(FEM["V"])
    Ay = Function(FEM["V"])                         # create Fenics Function for field realisation
    Uy.vector()[:] = uy[0]                          # Store solution in Function
    Ay.vector()[:] = ay[0]                          # Store current field realisation in Function
    Uy_dev.vector()[:] = uy[0] - u_ref[0]           # store difference to u[x, 0, ..., 0]

    ph['u'].plot(Uy)                                # plot results
    ph['a'].plot(Ay)
    ph['u deviation'].plot(Uy_dev, interactive=True)

