from __future__ import division
import numpy as np
from dolfin import *

def setup_FEM(N, degree=1):
    mesh = UnitSquareMesh(N, N)
    V = FunctionSpace(mesh, 'CG', degree)

    # define Dirichlet boundary (x = 0 or x = 1)
    def boundary(x):
        #return near(x[0], 0, DOLFIN_EPS)
        return near(x[0], 0, DOLFIN_EPS) or near(x[1], 0, DOLFIN_EPS)\
            or near(x[0], 1, DOLFIN_EPS) or near(x[1], 1, DOLFIN_EPS)

    # define boundary condition
    u0 = Constant(0.0)
    bc = DirichletBC(V, u0, boundary)

    # define variational problem
    u = TrialFunction(V)
    v = TestFunction(V)
    f = Constant(1)
    L = f * v * dx
    return {"bc": bc, "V": V, "u": u, "v": v, "L": L}


def evaluate_u(y, FEM, field, identity=False):
    # type: (List[List[float]], Dict, TestField) -> List[List[float]], List[List[float]]
    """
      evaluate solution of the given FEM problem with the current field realisation at y
    :param y: List of samples in [-1,1]^M
    :param FEM: FEM dictionary as defined in setup_FEM
    :param field: TestField object
    :param identity: switch to sample identity
    :return: List of solution coefficients
    """
    #                                               # generate field realisations
    Fy = [field.realisation(yi, FEM["V"]) for yi in y]

    # from dolfin import plot
    # plot(Fy[0], interactive=True)
    # print [fy.vector().array() for fy in Fy]

    #                                               # evaluate FEM solutions
    Uy = []                                         # init list of solution coefficients
    Ay = []                                         # init list of field coefficients
    for yi, fy in zip(y, Fy):
        a = fy * inner(grad(FEM["u"]), grad(FEM["v"])) * dx
        uy = Function(FEM["V"])
        if not identity:
            solve(a == FEM["L"], uy, FEM["bc"])
            Uy.append(uy.vector().array()**2)
            Ay.append(fy.vector().array())
        else:
            ex = Expression('(1 - A0*x[0])*x[0] * (1-A1*x[1])* x[1]', A0=1, A1=1)
            #ex = Expression('1 - A0*A0 - 10*(A0-0.5)*(A0-0.5)*(A0-0.5) - A1*A1 ', A0=1, A1=1)
            ex.A0 = yi[0]
            ex.A1 = yi[1]
            Uy.append(interpolate(ex, FEM["V"]).vector().array())
#           Uy.append(np.ones(FEM["V"].dim()))
    return np.array(Uy), np.array(Ay)

