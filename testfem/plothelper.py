from dolfin import plot, interactive, UnitSquareMesh, Expression, FunctionSpace, interpolate, Function, Mesh, MeshFunctionSizet

class PlotProxy(object):
    def __init__(self, name):
        self.name = name
        self.handle = None
    def plot(self, f, *args, **kwargs):
        if self.handle is None:
            if not "title" in kwargs.keys():
                kwargs["title"] = self.name
            self.handle = plot(f, *args, **kwargs)
        else:
            self.handle.plot(f)

class PlotHelper(object):
    def __init__(self):
        self.handles = {}
    def __getitem__(self, name):
        try:
            ph = self.handles[name]
        except:
            ph = PlotProxy(name)
            self.handles[name] = ph
        return ph
    def interactive(self):
        interactive()

# taken from
# http://fenicsproject.org/qa/5795/plotting-dolfin-solutions-in-matplotlib

import matplotlib.pyplot as plt
import matplotlib.tri as tri
import matplotlib.colors as col

def mesh2triang(mesh):
    xy = mesh.coordinates()
    return tri.Triangulation(xy[:, 0], xy[:, 1], mesh.cells())

def plot_mpl(obj):
    ''' plot FEniCS object (use with plt.figure() ... plt.show()) '''
    plt.gca().set_aspect('equal')
    if isinstance(obj, Function):
        mesh = obj.function_space().mesh()
        if (mesh.geometry().dim() != 2):
            raise(AttributeError)
        if obj.vector().size() == mesh.num_cells():
            C = obj.vector().array()
            plt.tripcolor(mesh2triang(mesh), C)
        else:
            C = obj.compute_vertex_values(mesh)
            plt.tripcolor(mesh2triang(mesh), C, shading='gouraud')
    elif isinstance(obj, Mesh):
        if (obj.geometry().dim() != 2):
            raise(AttributeError)
        plt.triplot(mesh2triang(obj), color='k')
    elif isinstance(obj, MeshFunctionSizet):
        mesh, C = obj.mesh(), obj.array()
        vmin, vmax = min(C), max(C)
        plt.tripcolor(mesh2triang(mesh), facecolors=C, edgecolors=['none','k'][0], cmap=plt.cm.coolwarm, vmin=vmin, vmax=vmax)  # coolwarm, brg, seismic, winter, jet, hot, spectral, prism
    else:
        print "unsupported plotting type", type(obj)
        assert TypeError
