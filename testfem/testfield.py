from __future__ import division
from dolfin import interpolate, Expression, Function
import numpy as np

class TestField:
    """ artificial M-term KL (plane wave Fourier modes as in EGSZ). random variables are assumed uniform in [-1,1].
    """
    def __init__(self, mean=1, expfield=False, M=None, skip_freq=0):
        # type: (float, bool, int) -> None
        """
         initialise field with given mean.
         field is log-uniform for expfield=True.
         length M of expansion can optionally be fixed in advance.
         usually it is determined by the length of y when evaluating realisations.
        :param mean: mean value of the field
        :param expfield: Switch to go to lognormal field
        :param M: maximal number of terms in realisation
        :return:
        """
        #                                           # create a Fenics expression of the affine field
        self.a = Expression('C + sin(A*pi*F1*x[0]) * sin(A*pi*F2*x[1])', A=1, C=mean, F1=0, F2=0, degree=5)
        self.mean = mean
        self.expfield = expfield
        self.M = M
        self.skip_freq = skip_freq

    def realisation(self, y, V):
        # type: (List[float], FunctionSpace) -> Function
        """
          evaluate realisation of random field subject to samples y and return interpolation onto FEM space V.
        :param y: list of samples of the RV in [-1, 1]
        :param V: FunctionSpace
        :return: Fenics Function as field realisation
        """

        def indexer(i):
            m1 = np.floor(i/2)
            m2 = np.ceil(i/2)
            return m1, m2
        M = len(y)                                  # store length of the realisation vector
        a = self.a                                  # store affine field Expression

        a.C, a.F1, a.F2 = self.mean, 0, 0           # get mean function as starting point
        x = interpolate(a, V).vector().array()      # interpolate constant mean on FunctionSpace
        a.C = 0                                     # set mean back to zero. From now on look only at amp_func
        #                                           # add up stochastic modes
        if M > 0:
            #                                       # get mean-length ratio
            mean = self.mean if not self.expfield else np.exp(self.mean)
            CM = mean / M if self.M is None else self.mean / self.M
            assert self.M is None or M <= self.M    # check length of given sample and actual maximal field length
            for m, ym in enumerate(y):              # loop through sample items
                a.F1, a.F2 = indexer(m + 2 + self.skip_freq) # get running values in Expression
                #                                   # add current Expression value
                x += CM * ym * interpolate(a, V).vector().array()
        f = Function(V)                             # create empty function on FunctionSpace
        #                                           # set function coefficients to realisation coefficients
        f.vector()[:] = x if not self.expfield else np.exp(x)
        return f

